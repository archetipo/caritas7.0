function my_basewidget(instance, module){ //module is instance.point_of_sale

    // This is a base class for all Widgets in the POS. It exposes relevant data to the

    module.BBW = instance.web.Widget.extend({
        init:function(parent,ation){
            this._super(parent);
            ation = ation || {};
        },
        show: function(){
            this.$el.show();
        },
        hide: function(){
            this.$el.hide();

        },
        renderElement: function(){
            this._super();
            if(this.hidden){
                if(this.$el){
                    this.$el.hide();
                }
            }
        },         
        destroy: function () {
            this._super();
        }         
    });

}

function datas_Model(instance, module){ 
    //modulo da implementare con la chiamata ad una funzione.
    //come funziona
    //impostare la funzione di callback da eseguire alla fine dell'azione come es.
            // this.pool.ready.done(function(){
            //     self.renderElement(pnode); <--- funzione del modulo che lo usa
            // });
    //poi chiamare il metodo es get_data con i parametri || act_obj
    //quindi nel methodo di callback si potranno utilizzare i dati 
    //presenti in loaded_data

    module.Pool = instance.web.Class.extend({
        init:function(action){
            this.ready = $.Deferred();
            action = action || {};
            this.loaded_data=[];
            this.obj_model=null;
            var self=this;

        },
        get_data: function(model,fields,filter,ctx,order1,order2){
            var self=this;
            this.obj_model=new instance.web.Model(model);
            this.fields=fields;
            this.filter=filter;
            this.ctx=ctx
            this.order1=order1 || '';
            this.order2=order2 || '';
            $.when(this.load_data(self))
                .done(function(){
                    self.ready.resolve();
                  }); 

        }, 
        load_data: function(loc){
            var self = loc;
            self.load=self.obj_model.query(self.fields)
                 .filter(self.filter)
                 .order_by(self.order1,self.order2)
                 .all().then(function (datas) {
                    self.loaded_data=datas;
                 });  
            return self.load;          
        },                
    });

}

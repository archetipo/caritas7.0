# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Caritas Base',
    'version': '1.1',
    'category': 'Modulo di Base per Caritas',
    'description': """
     Questo modulo è la base per gestire i bisognosi e per la gestione della
     struttura Caritas in sostanza vengono inseriti i dati della persona
     richiedente, vengono caricati i motivi e le rischieste di aiuto per poi
     essere gestite e per pianificare la consegna spesa, casa, affitto""",
    'author': 'Caritas Interparrochiale Bra',
    'website': 'http://www.caritasbra.it',
    'depends': ['resource', 'board','Caritas_rubrica'],
    'init_xml': [],
    'update_xml': [
        'security/cbase_security.xml',
        'security/ir.model.access.csv',
        'cbase_view.xml',
        'cbase_view_config.xml',
        # 'cbase_data.xml',
        # 'cbase_data2.xml',
        'cbase_data_ricdef.xml',
        'cbase_scheduler.xml',


    ],
    'installable': True,
    'application': True,
    'active': False
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

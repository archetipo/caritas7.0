# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------------
#   Modulo Base gestione utenti Caritas e servizi sociali
#           utenti famiglia lavoro
#   Autore: Alessio Gerace
#   data  : Maggio 2011
#   update : Giugno 2011
#
#
#
#
#
##############################################################################

from openerp.osv import orm,  fields
from tools.translate import _
from datetime import datetime, timedelta, date
import decimal_precision as dp
import unittest
import sets
import codicefiscale
from codicefiscale import get_birthday, get_sex, control_code,ctrl_city_code,build



import resource
import tools
import time
import addons
import datetime
from lxml import etree
from xlrd import open_workbook
import os,csv
import string


STAT_RIC=[('new', 'In Valutazione'),
         ('pian', 'Accettata'),
         ('rif', 'Riufiutata'),
         ('evasa','Evasa')]

STAT_B=[('new', 'Nuovo'),
         ('proc', 'In circolazione'),
         ('rit', 'Ritornato'),
         ('scad', 'Scaduto')]

MESI=[('01', 'Gennaio'),
        ('02', 'Febbraio'),
        ('03', 'Marzo'),
        ('04', 'Aprile'),
        ('05', 'Maggio'),
        ('06', 'Giugno'),
        ('07', 'Luglio'),
        ('08', 'Agosto'),
        ('09', 'Settembre'),
        ('10', 'Ottobre'),
        ('11', 'Novembre'),
        ('12', 'Dicembre')]

class cbase_func(orm.Model):
    _name = "cbase.func"

    def decodeCf(self,data):
        t=data.split(' ')
        contv=0
        rs=[]
        for j in t:
            if j!='':
                rs.append(j)
            if j=='' and contv==0:
                contv+=1
                rs.append(j)
        t=rs
        conta=0
        nc=0
        nomcong=['nome','cognome']
        code=''
        sp=' '
        for x in t:
            if conta==0:
                cf=x[0:16]
                print "cod: %s lung %d" % (cf,len(x))
                nomcong[nc]=x[16:len(x)]
            elif conta>0:
                if x=='':
                    nc+=1
                    sp=''
                    nomcong[nc]=''
                else:
                    nomcong[nc]+=sp
                    for l in x:
                        if (l.isalpha()):nomcong[nc]+=l
                        if (l.isdigit()):code+=l
                    print conta
                    print len(t)
                    if (conta<len(t)-1) and sp=='':
                            nomcong[nc]+=' '
            conta+=1
        print nomcong
        return nomcong[1],nomcong[0],cf,code

    def calcdata(self,anno):
        ap=1900+int(anno)
        oggi=date.today()
        if (oggi.year-ap)>95:
            ap=2000+int(anno)
        return ap

    def get_nazione(self,cr,uid):
        partner_id = self.pool.get('res.users').browse(cr, uid, uid).partner_id.id
        nazione = self.pool.get('res.partner').browse(cr, uid, partner_id).country_id.id
        return nazione

    def testindi(self,cr,uid,ids):
        print ids
        ret=True
        message =u''
        indig=self.pool.get('cbase.indigente').browse(cr,uid,ids[0])
        if indig and indig.codfis=='' or indig.codfis=='n' or indig.codfis=='N' :
            ret=False
            message=u'Manca il codice Fiscale o risulta non compilato correttamente'
            self.log(cr, uid, ids[0], message)
        if indig and not indig.finanze:
            ret=False
            message=u'Non è stata compilata la sistuazione finanziaria in futuro questo dato sarà fondamentale per l\'accesso ai servizi'
            self.log(cr, uid, ids[0], message)

        return ret

    def get_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))

    def get_anno(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return int(date(int(a),int(m),int(g)).strftime('%Y'))

    def get_ini_anno(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),1,1)
cbase_func()

class cbase_abitativi(orm.Model):
    _name = "cbase.abitativi"
    _description = "Problemi abitativi"
    _columns = {
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'name': fields.char('Problema abitativo', size=128,required=True),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
    }
    _defaults = {
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_abitativi()

class cbase_economici(orm.Model):
    _name = "cbase.economici"
    _description = "Problemi economici"
    _columns = {
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'name': fields.char('Problema economico', size=128,required=True),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
    }
    _defaults = {
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_economici()

class cbase_patologie(orm.Model):
    _name = "cbase.patologie"
    _description = "Patologie"
    _columns = {
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'name': fields.char('Patologia', size=128,required=True),
        'description': fields.text('Descrizione'),
        'standard':fields.boolean('Utilizzare come standard'),
        'statcod': fields.char('Codice statistico', size=32),
    }
    _defaults = {
            'standard':False,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_patologie()

class cbase_scolar(orm.Model):
    _name = "cbase.scolar"
    _description = "Scolarizzazioni"
    _columns = {
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'name': fields.char('Titolo di Studio', size=128,required=True),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
    }
    _defaults = {
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_scolar()

class cbase_lingue(orm.Model):
    _name = "cbase.lingue"
    _description = "Idiomi"
    _columns = {
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'name': fields.char('Idiomi Lingue', size=128,required=True),
        'nazione': fields.many2one('res.country', 'Nazione al quale appartiene la Lingua',required=True),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
    }
    _defaults = {
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_lingue()

class cbase_vistips(orm.Model):
    _name = "cbase.vistips"
    _description = "Visti permessi tipo di sogiorno"
    _columns = {
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'name': fields.char('Visto/Permesso/Tipo di soggiorno', size=128,required=True),
        'scadenza': fields.boolean('Documento a scadenza '),
        'orig': fields.boolean('Mantiene Nazione origine'),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
    }
    _defaults = {
            'orig':True,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_vistips()

#~ conf richieste aiuti
class cbase_ricdef(orm.Model):
    _name = "cbase.definiz"
    _description = "definizioni richieste"

    def _val_ric(self, cr, uid, ids,name, args, context=None):
        res = {}
        valcfg=self.pool.get('cbase.definizval')
        for ric in self.browse(cr, uid, ids):
            valcfgval=0.0
            idsval=valcfg.search(cr, uid,[('name','=',ric.id),('comptenza','=','USER_COMPETENZA')])
            if idsval:
                valcfgval=valcfg.read(cr, uid,idsval,['valore'])[0]['valore']
            res[ric.id] = valcfgval
        return res
    def _costo_ric(self, cr, uid, ids,name, args, context=None):
        res = {}
        valcfg=self.pool.get('cbase.definizval')
        for ric in self.browse(cr, uid, ids):
            valcfgval=0.0
            idsval=valcfg.search(cr, uid,[('name','=',ric.id),('comptenza','=','USER_COMPETENZA')])
            if idsval:
                valcfgval=valcfg.read(cr, uid,idsval,['erog'])[0]['erog']
            res[ric.id] = valcfgval
        return res
    def _cod_contab(self, cr, uid, ids,name, args, context=None):
        res = {}
        valcfg=self.pool.get('cbase.definizval')
        for ric in self.browse(cr, uid, ids):
            valcfgval=''
            idsval=valcfg.search(cr, uid,[('name','=',ric.id),('comptenza','=','USER_COMPETENZA')])
            if idsval:
                valcfgval=valcfg.read(cr, uid,idsval,['cod'])[0]['cod']
            res[ric.id] = valcfgval
        return res
    def _mov_contab(self, cr, uid, ids,name, args, context=None):
        res = {}
        valcfg=self.pool.get('cbase.definizval')
        for ric in self.browse(cr, uid, ids):
            valcfgval=''
            idsval=valcfg.search(cr, uid,[('name','=',ric.id),('comptenza','=','USER_COMPETENZA')])
            if idsval:
                valcfgval=valcfg.read(cr, uid,idsval,['mov'])[0]['mov']
            res[ric.id] = valcfgval
        return res
    def _mov_racc(self, cr, uid, ids,name, args, context=None):
        res = {}
        valcfg=self.pool.get('cbase.definizval')
        for ric in self.browse(cr, uid, ids):
            valcfgval=False
            idsval=valcfg.search(cr, uid,[('name','=',ric.id),('comptenza','=','USER_COMPETENZA')])
            if idsval:
                valcfgval=valcfg.read(cr, uid,idsval,['acc'])[0]['acc']
            res[ric.id] = valcfgval
        return res
    _columns = {

        'name':fields.char('Richiesta', size=256),
        'statcod': fields.char('Codice statistico', size=32),
        'valore':fields.function(_val_ric,method=True,type='float',string='Valore richiesta €', digits=(14, 2)),
        'erog':fields.function(_costo_ric,method=True,type='float',string='Costo effettivo richiesta €', digits=(14, 2)),
        'cod':fields.function(_cod_contab,method=True,type='char',string='Codice Analitico'),#da definire relazione con tabella piano dei conti
        'mov':fields.function(_mov_contab,method=True,type='char',string='Conto analitico'), #da definire relazione con tabella piano dei conti
        'racc':fields.function(_mov_racc,method=True,type='boolean',string='La Richiesta è accettata automaticamente al momento dell\'inserimento'), #da definire relazione con tabella piano dei conti
        'tipo':fields.selection([('norm', 'Normale'),
                         ('viveri', 'Viveri'),
                         ('farmaci', 'Farmaci'),
                         ('vestiario', 'Vestiario'),
                         ('microcredito', 'Microcredito'),
                         ('mensa', 'Mensa'),
                         ('dormitorio','Pernottamento')], 'Categoria Aiuto'),
        'valmod':fields.boolean('Valore modificabile'),
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        }
    _defaults = {
            'valmod':True,
            'name':'Nessuna',
            'tipo':'norm',
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
    }
    _order = 'name asc'
cbase_ricdef()

#~ configura valore richiesta per ente richieste aiuti
class cbase_ricdefval(orm.Model):
    _name = "cbase.definizval"
    _description = "definizioni richieste"
    _columns = {
        'name':fields.many2one('cbase.definiz','Richiesta ', size=128),
        'valore':fields.float('Valore richiesta €',  digits=(14, 2)),
        'erog':fields.float('Costo effettivo richiesta €',  digits=(14, 2)),
        'cod':fields.char('Codice analitico ', size=32),#da definire relazione con tabella piano dei conti
        'mov':fields.char('Conto analitico', size=32), #da definire relazione con tabella piano dei conti
        'acc':fields.boolean('La Richiesta è accettata automaticamente al momento dell\'inserimento'),
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        }
    _defaults = {
            'valore':0.0,
            'erog':0.0,
            'acc':False,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
    }
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ print 'ovverride Search'
        #~ print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ print new_args
        return super(cbase_ricdefval, self).search(cr, uid, new_args)

    def read_group(self, cr, uid, domain,fields,groupby, offset=0, limit=None,order=None,context=None):
        #~ print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #~ print 'domain gruppo articoli %s' % new_domain
        return super(cbase_ricdefval, self).read_group(cr, uid, new_domain, fields , groupby, limit, context)
    _order = 'name asc'
cbase_ricdefval()

#~  fine model configurazioni

class cbase_probeco(orm.Model):
    _name = "cbase.probeco"
    _description = "Problemi economici"
    _columns = {
        'fin_id':fields.integer('id situazione finanziaria'),
        'ind_id':fields.integer('id indigente'),
        'name': fields.many2one('cbase.economici', 'Tipo problema',required=True),
        'nota': fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    _defaults = {
            'dtin':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_probeco()

class cbase_probabit(orm.Model):
    _name = "cbase.probabit"
    _description = "Problematiche abitative"
    _columns = {
        'ind_id':fields.integer('id indigente'),
        'name': fields.many2one('cbase.abitativi', 'Tipo problema',required=True),
        'nota': fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    _defaults = {
            'dtin':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_probabit()

#scuola e lingue
class cbase_linguepar(orm.Model):
    _name = "cbase.linguepar"
    _description = "Lingue parlate dall'utente"
    _columns = {
        'ind_id': fields.many2one('cbase.indigente','id indigente',size=128),
        'name': fields.many2one('cbase.lingue', 'Idioma',required=True),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    _defaults = {
            'dtin':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_linguepar()

#~  Salute e patologie
class cbase_salute(orm.Model):
    _name = "cbase.salute"
    _description = "Stato salute dell'utente"
    _columns = {
        'ind_id': fields.many2one('cbase.indigente','id indigente',size=128),
        'name': fields.many2one('cbase.patologie', 'Tipo patologia',required=True),
        'grad':fields.integer('Grado di invalidità % (solo numeri)'),
        'nota': fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    _defaults = {
            'dtin':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            }
    #~ def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False,submenu=False):
        #~ res=super(cbase_salute, self).fields_view_get(cr, uid, view_id, view_type, context=context, toolbar=toolbar,submenu=submenu)
        #~ root = etree.fromstring(res['arch'])
        #~ if context.has_key('nome'):
            #~ if context['nome']!=[] and context['nome'][0]:
                #~ root.attrib['string']+=' di '+self.pool.get('cbase.indigente').browse(cr,uid,context['nome'][0]).name
        #~ res['arch']=etree.tostring(root)
        #~ return res
    _order = 'name asc'
cbase_salute()

class cbase_statocivile(orm.Model):
    _name = "cbase.statocivile"
    _description = "Stoto civile della persona"
    _columns = {
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'name': fields.char('Stato Civile', size=128),
        'description': fields.text('Status Description'),
        'statcod': fields.char('Codice statistico', size=32),
    }
    _defaults = {
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_statocivile()

class cbase_parenti(orm.Model):
    _name = "cbase.parenti"
    _description = "Tipo di parentela"
    _columns = {
        'name': fields.char('Tipo parente', size=32,required=True),
        'description': fields.text('Status Description'),
    }
    _order = 'name asc'
cbase_parenti()

#~ famigliari e famiglia
class cbase_famiglia(orm.Model):
    _name = "cbase.famiglia"
    _description = "Nucleo famigliare indigente"
    # calcolo automatico anni e nazionalità e patologie e lavoro.. e controllarte.
    _columns = {
        'ind_id':fields.many2one('cbase.indigente', 'Capo famiglia'),
        'name':fields.many2one('cbase.indigente', 'Parente'),
        'nome': fields.related('name','nome',type='char',size=128, relation='cbase.indigente', string='Nome'),
        'cognome': fields.related('name','cognome',type='char',size=128, relation='cbase.indigente', string='cognome'),
        'codfis': fields.related('name','codfis',type='char',size=16, relation='cbase.indigente', string='codice Fiscale'),

        'minor':fields.boolean('Neonato', help="Inteso come minore di un anno"),
        'eta':fields.integer('Anni'),
        'grad':fields.many2one('cbase.parenti', 'Grado Parentela',required=True),
        'coniuge':fields.boolean('Coniuge', help="Selezionare nel caso sia coniuge"),
        'isee':fields.boolean('Presente ISEE', help="Se è presente nell'isee corrente"),
        'statfam':fields.boolean('Presente in Stato di Famiglia', help="Presente nello stato di famiglia"),
        'pborsa':fields.boolean('Prende la Borsa', help="Conteggiato nella borsa"),
        'conv':fields.boolean('Abita in casa', help="Conteggiato nella borsa"),
        'patol': fields.related('name','patologie',type='one2many', relation='cbase.salute', string='Patologia'),
        'lavoro': fields.related('name','lavori',type='one2many', relation='cbase.lavoro', string='Lavoro'),
        'redd':fields.float('Reddito',  digits=(14, 2)),
        'nota': fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    _defaults = {
        'coniuge': 0,
        'eta':1,
        'dtin':fields.date.context_today,
        'pborsa':False,
        'isee':False,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))


    def on_change_grad(self,cr,uid,ids,grad,name):
        gp=self.pool.get('cbase.parenti').browse(cr,uid,grad)
        res = {'value': {}}
        print 'Famiglia grad',gp.name
        if gp.name=='Coniuge':
            res['value']['coniuge']= True
        elif ids:
            res['value']['coniuge']= False
        return res

    def name_get(self,cr,uid,ids,context=None):
        res=[]
        objs=self.browse(cr,uid,ids)
        for obj in objs:
            res.append((obj.id,obj.name.name))
        return res

    def on_change_fam(self, cr, uid, ids, name):
        '''
        res = {'value': {
            'country_id': self.pool.get('rubrica.country').search(cr, uid, [('name','=','Italy')])[0],
            }}
        '''
        res = {'value':{}}
        if(name):
            ind = self.pool.get('cbase.indigente').browse(cr, uid, name)
            res = {'value': {

                'eta':int((date.today()-self.gen_data(ind.comple)).days/(365.25)),
                }}
        return res

    def test_indi(self,cr,uid,codfisc):
        oindi=self.pool.get('cbase.indigente')
        indi_id=oindi.search(cr,uid,[('codfis','=',codfisc)])
        if indi_id and len(indi_id)>0:
            return indi_id[0]
        else:
            return 0

    def on_change_codf(self,cr,uid,ids,nome,cognome,ccf,context=None):
        oindi=self.pool.get('cbase.indigente')
        print nome,cognome,ccf
        indiexist=0
        if nome and cognome and ccf:
            if ccf and len(ccf)==16:
                ccf=ccf.upper()
                indiexist=self.test_indi(cr,uid,ccf)
                func=oindi.pool.get('cbase.func')
                italia=func.get_nazione(cr,uid)
                res=oindi.load_cf(cr,uid,nome,cognome,ccf)
                if res['value']['codfis']==ccf:
                    if indiexist==0:
                        try:
                            res['value']['name']=self.create_indi(cr,uid,nome,cognome,ccf,res)
                            ind=oindi.browse(cr,uid,res['value']['name'])
                            res['value']['eta']=int((date.today()-self.gen_data(ind.comple)).days/(365.25))
                            return res
                        except:
                            raise orm.except_orm(_(u'Attenzione'), _(u'Controllare id dati inseriti'))
                            return False
                    else:
                        res['value']['name']=indiexist
                        ind=oindi.browse(cr,uid,res['value']['name'])
                        res['value']['eta']=int((date.today()-self.gen_data(ind.comple)).days/(365.25))
                        return res
                else:
                    raise orm.except_orm(_(u'Attenzione'), _(u'Il codice fiscale non è congruo con i dati inseriti, verificare nome e cognome o il codice'))
                    return False
            else:
                raise orm.except_orm(_(u'Attenzione'), _(u'Il codice fiscale non è valido mancano o eccedono caratteri'))
                return False
        else:
            return False

    def create_indi(self,cr,uid,nome,cognome,ccf,args):
        oindi=self.pool.get('cbase.indigente')
        return oindi.create(cr,uid,args['value'])


    def write(self, cr, uid, ids,vals, context=None):
        if type(ids)==int:
            fam=ids
        if type(ids)==long:
            fam=ids
        elif type( ids ) == list:
            fam=ids[0]
        prj_indi=self.pool.get('cbase.indigente')
        res={}
        con=False
        if 'coniuge' in vals:
            if  vals['coniuge']:
                con=True
        idr=super(cbase_famiglia,self).write(cr, uid,ids, vals, context)
        o=self.browse(cr,uid,fam)
        indi=prj_indi.browse(cr,uid,o.name.id)
        if indi:
            if con:
                res['coniug']=o.name.name
            prj_indi.write(cr,uid,o.name.id,res)
        return idr

    def aggiorna_scheda(self, cr, uid,ids,context=None):
        idds=self.search(cr,uid,[('id','>',0)])
        obs=self.browse(cr,uid,idds)
        for o in obs:
            print o.name
            if o.name.comple:
                print o.name
                eta=int((date.today()-self.gen_data(o.name.comple)).days/(365.25))
                try:
                    self.write(cr,uid,o.id,{'eta':eta})
                except:
                    pass
        return True

    def create(self, cr, uid, vals, context=None):
        if (vals['ind_id']==vals['name']):
            raise orm.except_orm(
                _(u'Attenzione!!'),
                _(u'Non si può aggiungere un utente parente di se stesso!!'))
            return False

        id=super(cbase_famiglia,self).create(cr, uid, vals, context)
        if 'coniuge' in vals and vals['coniuge']==True:
            self.pool.get('cbase.indigente').write(cr, uid, vals['ind_id'],{'coniug':vals['name']})
        return id

    _order = 'name asc'


class cbase_delegato(orm.Model):
    _name = "cbase.delegato"
    _description = "Delegato al Ritiro"
    _columns = {
        'ind_id':fields.integer('id indigente'),
        'name': fields.char('Nome', size=32,required=True),
        'doc': fields.char('Documento Identità No', size=32, help="Documento d'identita del delegato al ritiro",required=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'dtin':fields.date.context_today,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_delegato()

#~  Reddito
class cbase_tipiredd(orm.Model):
    _name = "cbase.tipiredd"
    _description = "Tipologie di reddito"
    _columns = {
        'name':fields.char('Tipo i reddito', size=128),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
        'tip': fields.selection([('sost', 'Sostitutivo'),
                                  ('agg', 'Aggiuntivo')], 'Tipo di azione nel conteggio'),
        'insieme':fields.boolean('Reddito d\'insieme anno precedente'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'insieme':False,
        'tip':'sost',
        'dtin':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_tipiredd()

class cbase_righeredd(orm.Model):
    _name = "cbase.righeredd"
    _description = "Reddito da"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','Indigente',size=128),
        'fin_id':fields.many2one('cbase.finanze','id situazione finanziaria'),
        'name': fields.many2one('cbase.tipiredd', 'Tipo reddito',required=True),
        'impcm':fields.float('Importo € mensile',  digits=(14, 2)),
        'dtal': fields.date("Al"),
        'dtld': fields.date("Dal",required=True),
        'anno':fields.integer('Anno'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'dtin':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }

    def on_change_dtld(self,cr,uid,ids,data):
        func=self.pool.get('cbase.func')
        res={'value':{}}
        if data:
            res['value']['anno']=func.get_anno(data)
        return res
    def on_change_dtal(self,cr,uid,ids,data):
        func=self.pool.get('cbase.func')
        res={'value':{}}
        if data:
            res['value']['anno']=func.get_anno(data)
        return res
    _order = 'name asc'
cbase_righeredd()

class cbase_finanze(orm.Model):
    def _Calc_Stato(self, cr, uid, ids,name, args, context=None):
        res = {}
        f=self.pool.get('cbase.func')
        for redd in self.browse(cr, uid, ids):
            res[redd.id] = 'att'
            if f.get_data(redd.dta)<date.today():
                res[redd.id] = 'scad'
        return res
    def _getanno(self,cr,uid,context=None):
        anno=int(datetime.date.today().strftime('%Y'))
        mindata=date(int(anno),3,1)
        if datetime.date.today()< mindata:
            return anno-1
        else:
            return anno

    _name = "cbase.finanze"
    _description = "Situazione Finanziaria Famigliare"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'anno':fields.integer('Anno',required=True),
        'righereddito':fields.one2many('cbase.righeredd','fin_id','Lista fonti di reddito mensile'),
        'isee':fields.float('Isee',  digits=(14, 2),required=True),
        'aggisee':fields.float('AGGIORNAMENTO ISEE anno in corso', digits=(14, 2),required=True),
        'dtda': fields.date("Valido dal ",required=True),
        'dta': fields.date(" al ",required=True),
        'totanno':fields.float('Reddito Famigliare anno indicato',  digits=(14, 2)),
        'probeco':fields.one2many('cbase.probeco','fin_id','Problemi economici',required=False),
        'nota': fields.text('Aggiungi un Nota'),
        'stato': fields.function(_Calc_Stato,method=True,string='Stato',type="selection",selection=[('att','Attivo'),('scad','Scaduto')]),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'anno':lambda self, cr, uid, c:self._getanno(cr,uid,c),
        'dtin':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
        }

    def get_dafascadisee(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a)+1,4,30).strftime('%Y-%m-%d')

    def testdatanno(self,anno):
        mindata=date(int(anno),3,1)
        if datetime.date.today()< mindata:
            return False,mindata
        else:
            return True,''

    def on_change_dtda(self,cr,uid,ids,data):
        res={'value':{}}
        if ids:
            oldval=self.read(cr,uid,ids[0])['dtda']
            if oldval and oldval!=data:
                data=oldval
        ret=self.get_dafascadisee(data)
        res['value']['dtda']=data
        res['value']['dta']=ret
        return res

    def on_change_isee(self,cr,uid,ids,isee):
        res={'value':{}}
        res['value']['isee']=isee
        if ids:
            oldval=self.read(cr,uid,ids[0])['isee']
            if oldval and oldval!=isee and oldval!=0:
                isee=oldval
                stringa='Il Campo Isee non può essere modificato per aggiornamenti usare il campo AGGIORNAMENTO ISEE '
                self.log(cr, uid, ids[0], stringa)
        res['value']['isee']=isee
        return res

    def create(self, cr, uid, vals, context=None):
        agg,val=self.testdatanno(vals['anno'])
        if agg:
            vals['dta']    =self.get_dafascadisee(vals['dtda'])
            return super(cbase_finanze,self).create(cr, uid, vals, context)
        else:
            stringa='l\'anno %d non può essere compilato prima del %s ' % (vals['anno'],val.strftime('%d-%m-%Y'))
            raise orm.except_orm(_(u'Info'), _(stringa))
            return False


    def write(self, cr, uid,ids,vals, context=None):
        if vals.has_key('dtda'):
            data=vals['dtda']
        else:
            data=self.browse(cr,uid,ids)[0].dtda
        vals['dta']=self.get_dafascadisee(data)
        return super(cbase_finanze,self).write(cr, uid,ids, vals, context)


    _rec_name='anno'
    _sql_constraints = [
        ('anno_ind_id_uniq', 'unique(anno, ind_id)', u'Attenzione!! L Anno che si vuole aggiungere esiste già, selezionarlo dalla lista e modificare quello.'),
    ]
    _order = 'anno desc'
cbase_finanze()

#~ Casa
class cbase_tipicasa(orm.Model):
    _name = "cbase.tipicasa"
    _description = "Tipologie di spesa casa"
    _columns = {
        'name':fields.char('Tipo', size=128),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'dtin':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_tipicasa()

class cbase_righetipicasa(orm.Model):
    _name = "cbase.righetipicasa"
    _description = "Spese Casa"
    _columns = {
        'casa_id':fields.integer('id situazione finanziaria'),
        'name': fields.many2one('cbase.tipicasa', 'Spesa casa per',required=True),
        'impcm':fields.float('Importo € Annuo',  digits=(14, 2)),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'dtin':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
cbase_righetipicasa()

class cbase_casa(orm.Model):
    _name = "cbase.casa"
    _description = "Codzione Alloggiativa"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'name':fields.date('Data',required=True),
        'casape': fields.boolean('Casa di proprietà Estero'),
        'righecasa':fields.many2one('cbase.tipicasa', 'Spesa casa per',required=True),
        'totanno':fields.float('Importo € Annuo',  digits=(14, 2),required=True),
        'probcasa':fields.many2one('cbase.abitativi', 'Tipo problema'),
        'nota': fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'name':fields.date.context_today,
        'dtin':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
        }

    _order = 'name asc'
cbase_casa()

#~  Veicoli
class cbase_veicoli(orm.Model):
    _name = "cbase.veicoli"
    _description = "Veicoli in possesso"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'name': fields.char('Nome Vettura', size=32),
        'targa': fields.char('Targa', size=32),
        'cil': fields.char('Cilindrata', size=32),
        'imm': fields.char('Anno Immatricolazione', size=32),
        'patenti': fields.char('Patenti', size=32),
        'nota': fields.text('Aggiungi un Nota'),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
        'dtin':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }


    _order = 'name asc'
cbase_veicoli()

#~ lavoro
class cbase_categlav(orm.Model):
    _name = "cbase.categlav"
    _description = "definizioni categorie di lavoro"
    _columns = {
        'lav_id':fields.integer('lavoro indigente'),
        'name':fields.char('Categoria lavoro', size=256,required=True),
        }
    _defaults = {
            'name':'Nessuna',
    }
    _order = 'name asc'
cbase_categlav()

class cbase_lavori(orm.Model):
    _name = "cbase.lavori"
    _description = "Lavori"
    _columns = {
        'opinsft':fields.char('Operatore', size=128,readonly=True),
        'name': fields.char('Condizione Lavorativa', size=128,required=True),
        'description': fields.text('Descrizione'),
        'statcod': fields.char('Codice statistico', size=32),
        'valore': fields.integer('Valore Statistico',required=True),
    }
    _defaults = {
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
    _order = 'name asc'
cbase_lavori()

class cbase_lavoro(orm.Model):
    _name = "cbase.lavoro"
    _description = "Situazione Lavorativa "
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'name':fields.many2one('cbase.lavori','Lavoro', size=128,required=True),
        'datlav':fields.char('Presso', size=128, translate=True),
        'dtld': fields.date("Fino Al / Dal"),
        'cimp':fields.boolean('Iscritto al centro per l impiego '),
        'inter':fields.boolean('Iscritto agenzia interinale '),
        'rif': fields.boolean('Ha riufiutato un un offerta di lavoro nell ultimo anno'),
        'nota': fields.text('Aggiungi un Nota'),
        'catlav':fields.many2many('cbase.categlav','cat_rel','ind_id','lav_id','A quali lavori si rende disponibile'),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
                 'rif': 0,
                 'dtin':fields.date.context_today,
                 'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
                 }
    #~ def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False,submenu=False):
        #~ res=super(cbase_lavoro, self).fields_view_get(cr, uid, view_id, view_type, context=context, toolbar=toolbar,submenu=submenu)
        #~ root = etree.fromstring(res['arch'])
        #~ if context.has_key('nome'):
            #~ if context['nome']!=[] and context['nome'][0]:
                #~ root.attrib['string']+=' di '+self.pool.get('cbase.indigente').browse(cr,uid,context['nome'][0]).name
        #~ res['arch']=etree.tostring(root)
        #~ return res
    _order = 'name asc'
cbase_lavoro()

#~ enti
class cbase_enti(orm.Model):
    _name = "cbase.enti"
    _description = "Enti Socio Assistenziali"
    _columns = {
        'assoc':fields.many2one('rubrica.rubrica', 'Associazione Riferimento',required=False),
        'name':fields.char('Nome Ente', size=128),
        'territorio': fields.many2one('res.city', string='Territorio',required=True),
        'territorioprog': fields.many2one('res.city', string='Territorio Progetti provinciali',required=True),
        'province': fields.many2one('res.city', string='Diocesi/Comune-Provincia',required=True),
        'viveri':fields.boolean('Gestione Distribuzione Viveri'),
        'vestiario':fields.boolean('Distribuzione Vestiario'),
        'microcredito':fields.boolean('Microcredito'),
        'farmaci':fields.boolean('Distribuzione Famaci'),
        'mensa':fields.boolean('Mensa'),
        'dormitorio':fields.boolean('Accoglienza / Dormitorio'),
        'cda':fields.boolean('Centro di ascolto'),
        'esterno':fields.boolean('Ente Esterno'),
        'minisee':fields.float('Isee minimo',  digits=(14, 2),required=True),
        'maxisee':fields.float('Isee massimo',  digits=(14, 2),required=True)
        }
    _defaults = {
                'minisee':1500.00,
                'maxisee':7000.00,
                'viveri': False,
                'vestiario': False,
                'microcredito': False,
                'farmaci': False,
                'mensa': False,
                'dormitorio': False,
                'cda': False,
                'esterno': True
                }


    def aggiorna(self, cr, uid,ids,context=None):
        idss=self.search(cr,uid,[('id','>',0)])
        for id in idss:
            ente=self.browse(cr,uid,id)
            dioc=ente.province.id
            self.write(cr,uid,id,{'territorioprog':dioc})
        return True
    _order = 'name asc'
cbase_enti()

class rubrica_rubrica(orm.Model):
    _inherit ='rubrica.rubrica'
    _description = 'Description'
    _columns = {
        # 'ente':fields.many2one('cbase.enti', 'Ente Riferimento',required=False),
        'ente':fields.one2many('cbase.enti', 'assoc', 'Ente Riferimento' ),
    }


#~ aiuti erogati
class cbase_erogenti(orm.Model):
    _name = "cbase.erogenti"
    _description = "Aiuti Erogati"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'ente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'rif':fields.char('Riferimento', size=256),
        'entealtro':fields.many2one('cbase.enti','Ente Esterno', size=128,select=True,required=True),
        'rifaltro':fields.char('Riferimento', size=256),
        'imp': fields.float('Importo Erogato',  digits=(14, 2)),
        'dur': fields.integer('Quantità'),
        'tmp': fields.selection([('volta', 'Volta/e'),
                                 ('nscad', 'Senza scadenza'),
                                 ('mese', 'giorno/i'),
                                 ('mese', 'Mese'),
                                 ('anno', 'Anno')], 'U.Mis.'),
        'nota': fields.text('Aggiungi un Nota'),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'inest':fields.boolean('Da ente esterno'),
        'opinsft':fields.char('Operatore', size=32),
    }
    _defaults = {
            'tmp':'volta',
            'dur':1,
            'dtin':fields.date.context_today,
            'ente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'entealtro':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'inest':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.esterno,
            'rif':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name
    }

    _order = 'ente asc'
cbase_erogenti()

#~ borse
class cbase_borse(orm.Model):
    _name = "cbase.borse"
    _description = "pianificazione Consegna Borsa"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'qt': fields.selection([('1', '1'),
                                ('2', '2')], 'Borse al mese'),
        'scad': fields.selection([('1', '1'),
                                  ('2', '2'),
                                  ('3', '3'),], 'Scade tra mese/i'),
        'st_att':fields.boolean('Attivo'),
        'st_proc':fields.boolean('Processato'),
        'st_scad':fields.boolean('Scaduto'),
        'primacons':fields.selection(MESI,'A partire da'),
        'occasionale':fields.boolean('Occasionale'),
        'nota': fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtini': fields.datetime("Data inserimento"),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        }


    _defaults = {
            'dtini':fields.date.context_today,
            'scad':'1',
            'st_att':1,
            'st_proc':0,
            'st_scad':0,
            'occasionale': 1,
            'qt':'1',
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'primacons':datetime.date.today().strftime('%m')
    }
    def cons_occasionale(self, cr, uid, ids,name,args, context=None):
            for borsa in self.browse(cr, uid, ids):
                if borsa.occasionale:
                    borsa.st_att=True
                    borsa.st_proc=True
                    borsa.st_scad=True
                    borsa.qt=1
                    borsa.scad=1
            return True

    def write(self, cr, uid, ids,vals, context=None):
        if type(ids)==int:
            id_borsa=ids
        if type(ids)==long:
            id_borsa=ids
        elif type( ids ) == list:
            id_borsa=ids[0]
        obj_prenot=self.browse(cr,uid,id_borsa)
        chkindi=self.pool.get('cbase.indigente')
        num=chkindi.check_for_borse(cr,uid,obj_prenot.ind_id.id)
        if num==0 :
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'L\'Isee supera il valore soglia impossibile '))
            return False
        elif num == 1:
            if 'scad' in vals:
                vals['scad']='1'
            if 'qt' in vals:
                vals['qt']='1'
        return super(cbase_borse,self).write(cr, uid,ids, vals, context)

    def create(self, cr, uid, vals, context=None):
        r_id=super(cbase_borse,self).create(cr, uid, vals, context)
        obj_prenot=self.browse(cr,uid,r_id)
        chkindi=self.pool.get('cbase.indigente')
        if obj_prenot.ind_id:
            num=chkindi.check_for_borse(cr,uid,obj_prenot.ind_id.id)
            if num==0 :
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'L\'Isee supera il valore soglia impossibile '))
                return False
            elif num == -1 :
                obj_r=chkindi.browse(cr,uid,obj_prenot.ind_id.id)
                if obj_r.borse and len(obj_r.borse)>1:
                    raise orm.except_orm(
                        _(u'Attenzione'),
                        _(u'Manca un isee valido impossibile '))
                    return False
                else:
                    return self.write(cr,uid,r_id,{'scad':'1','qt':'1'})
            elif num == 1:
                return self.write(cr,uid,r_id,{'scad':'1','qt':'1'})
        else:
            return r_id


    _order = 'dtini desc'


#~ richieste aiuti
#~ todo gestire codice richieste come unico per ricerca ecc..
#~ ciaooo
class cbase_richieste(orm.Model):
    _name = "cbase.richieste"
    _description = "Richieste dell' utente"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'defin':fields.many2one('cbase.definiz', 'Tipo richiesta',required=True),
        'cod_ric': fields.char('codric', size=32),
        'dtda': fields.date("Del",required=True),
        'valmod':fields.boolean('Valore modificabile'),
        'valore':fields.float('Con Valore richiesto di €',  digits=(14, 2)),
        'stato':fields.selection(STAT_RIC, 'Stato'),
        'buono':fields.selection(STAT_B, 'Stato Buono'),
        'erog':fields.float('Valore erogato di €',  digits=(14, 2)),# visualizza solo Admin
        'dterog': fields.date("il"),
        'progetto':fields.integer('id progetto'),
        'codiceb':fields.char('Codice', size=64),
        'nota':fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
    }
    _defaults = {
            'erog':0.0,
            'defin':0,
            'stato':'new',
            'buono':'new',
            'valmod':True,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    #~ TODO da inserire il controllo progetti quando sarà attivo
    def On_change_ric(self,cr,uid,ids,ric,context=None):
        res={'value': {}}
        if ric:
            self.ecce=False
            str=u''
            tric=self.pool.get('cbase.definiz').browse(cr,uid,ric)
            propente=self.pool.get('res.users').browse(cr, uid, uid).id_ente
            #~ =self.pool.get('res.users').browse(cr, uid, uid
            if (tric.tipo=='viveri') and propente.viveri:
                self.ecce=True
                str=u'Usare il sistema di gestione Borse per modificare le date della pianificazione'
            if self.ecce:
                raise orm.except_orm(_(u'Info'), _(str))
                res={'value': {'valmod':False}}
            else:
                valore_modif=tric.valmod
                valore=tric.valore
                erog=tric.erog
                accett=tric.racc
                res={'value': {'valmod':valore_modif,
                               'valore':valore,
                               'erog':erog}}
        return res

    def create(self, cr, uid, vals, context=None):
        return super(cbase_richieste,self).create(cr, uid, vals, context)

    def write(self, cr, uid,ids,vals, context=None):
        find=False
        if type(ids)==list:
            id=ids[0]
        else:
            id=ids
        tric=self.pool.get('cbase.definiz')
        code=self.browse(cr,uid,id).codiceb
        stato=self.browse(cr,uid,id).stato

        return super(cbase_richieste,self).write(cr, uid,ids, vals, context)

    def accetta(self, cr, uid, ids, context=None):
        print'Richiesta accettata'
        self.write(cr,uid,ids[0],{'stato':'pian'})
        return {}

    def rifiuta(self, cr, uid, ids, context=None):
        self.write(cr,uid,ids[0],{'stato':'rif','buono':'scad'})
        return {}

    def chiudi(self, cr, uid, ids, context=None):
        val=self.read(cr,uid,ids[0],['valore','erog','dterog'])
        print 'accettato',val
        if val['dterog']==False:
            raise orm.except_orm(_(u'Info'), _(u'Manca la data di erogazione, assicurarsi di essere in modalità di modifica'))
            return False
        self.write(cr,uid,ids[0],{'stato':'evasa','buono':'proc'})
        return {}

    def name_get(self,cr,uid,ids, context=None):
        records = self.browse(cr, uid, ids, context=context)
        result = {}
        for r in records:
            visit_name = r.defin.name
            print "defin name = %s" % visit_name
            result[r.id] = visit_name
        return result

    _rec_name='defin'
    _order = 'dtda asc'
cbase_richieste()

#~ diario
class cbase_diario(orm.Model):
    def _Calc_Totric(self, cr, uid, ids,name, args, context=None):
        res = {}
        for diario in self.browse(cr, uid, ids):
              res[diario.id] = 0.0
              for line in diario.richieste:
                  if line.dtda==diario.data :
                    res[diario.id]+=line.valore
        return res
    def _Calc_TotricEv(self, cr, uid, ids,name, args, context=None):
        res = {}
        for diario in self.browse(cr, uid, ids):
              res[diario.id] = 0.0
              for line in diario.richieste:
                  if (line.stato=='evasa' or line.stato=='pian') and line.dterog==diario.data :
                      res[diario.id]+=line.erog
        return res
    _name = "cbase.diario"
    _description = "Diario"
    _columns = {
            'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
            'data':fields.date("Data nota diario",required=True),
            'name':fields.char('oggetto', size=1024,required=False),
            'diario': fields.text('Aggiungi una nota nel Diario'),
            'nvalore':fields.float('€ Richieste Extra',  digits=(4,2)),
            'valore':fields.function(_Calc_Totric,method=True,string='€ Richiesti',digits=(4,2)),
            'erog':fields.function(_Calc_TotricEv,method=True,string='€ Erogati',digits=(4,2)),
            'viveri':fields.boolean('Borse'),
            'vestiario':fields.boolean('Vestiario'),
            'microcredito':fields.boolean('Microcredito'),
            'farmaci':fields.boolean('Farmaci'),
            'mensa':fields.boolean('Mensa'),
            'dormitorio':fields.boolean('Pernottamento'),
            'altro':fields.boolean('Richieste avanzate'),
            'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
            'richieste':fields.related('ind_id','richieste',type='one2many', relation='cbase.richieste', string='Richieste'),
            'dtini': fields.datetime("Data inserimento",readonly=True),
            'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
            'altro':False,
            'viveri': False,
            'vestiario': False,
            'microcredito': False,
            'farmaci': False,
            'mensa': False,
            'dormitorio': False,
            'valore':0.0,
            'data':fields.date.context_today,
            'dtini':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            }

    def onchangeval(self,cr,uid,ids,val,tipo,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        res={'value':{}}
        if ids:
            oldval=self.read(cr,uid,ids[0])[tipo]
            if oldval and oldval!=val:
                res['value'][tipo]=oldval
        ecce=False
        if tipo=='dormitorio' and val :
            if not user.id_ente.dormitorio:
                str=u'Sembra che il servisio %s non sia gestito in questo CDA'  % tipo
                ecce=True
        if tipo=='microcredito' and val:
            if not user.id_ente.dormitorio:
                str=u'Sembra che il servisio %s non sia gestito in questo CDA'  % tipo
                ecce=True
        if ecce:
            raise orm.except_orm(_(u'Info'), _(str))

        return res

    def get_name_def_ric(self,cr,uid,def_id):
        tric=self.pool.get('cbase.definiz')
        return tric.browse(cr,uid,def_id)

    def get_id_def_ric(self,cr,uid,def_id):
        tric=self.pool.get('cbase.definiz')
        return tric.browse(cr,uid,def_id)

    def get_stator_readable(self,str_stat):
        for i in STAT_RIC:
            if i[0]==str_stat:
                return i[1]

    def create(self, cr, uid, vals, context=None):
        print 'Create Valori diario'
        if vals.has_key('richieste'): print 'Richietse',vals['richieste']
        print ''
        print 'Tutti',vals
        #~ dret=self.search(cr,uid,[('data','=',item[2]['data'])])
        return super(cbase_diario,self).create(cr, uid, vals, context)

    def write(self, cr, uid,ids,vals, context=None):
        print 'Write Valori diario'
        if vals.has_key('richieste'): print 'Richietse',vals['richieste']
        print ''
        print 'Tutti',vals
        return super(cbase_diario,self).write(cr, uid,ids, vals, context)



    _order = 'data asc'
    _sql_constraints = [
        ('data_ind_id_uniq', 'unique(data,ind_id)', u'Attenzione!! La data che si vuole aggiungere esiste già, selezionare quella data dalla lista e modificare le note.'),]
cbase_diario()

class cbase_straniero(orm.Model):
    _name = "cbase.straniero"
    _description = "Persone Straniere"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente','id indigente',size=128),
        'name':fields.char('Numero Visto/Permesso/Carta Soggiorno', size=64),
        'tipo':fields.many2one('cbase.vistips','Tipo', size=128,select=True),
        'dtinip':fields.date("Data inizio permanenza Italia"),
        'dtscadp':fields.date("Data scadenza permesso"),
        'nota': fields.text('Aggiungi un Nota'),
        'idente':fields.many2one('cbase.enti','Ente', size=128,select=True,readonly=True),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
    }
    _defaults = {
            'dtinip':fields.date.context_today,
            'dtini':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            }

    _order = 'dtini desc'
cbase_straniero()


class cbase_indigente(orm.Model):
    _name = 'cbase.indigente'
    _description = 'Tabella base indigente'

    def _Calc_Totric(self, cr, uid, ids,field_name, args, context=None):
        res = {}
        for indi in self.browse(cr, uid, ids):
            res[indi.id]={
                            'totric':0.0,'totricevas':0.0,
                            'totaiuti':0.0,'capofam':True
                            }
            if (indi.nucleof is False) or (indi.famiglia != []):
                res[indi.id]['capofam']=True
            for line in indi.richieste:
                res[indi.id]['totric']+= line.valore
                if line.stato=='evasa':
                    res[indi.id]['totricevas']+= line.erog
            for line in indi.aiutenti:
                res[indi.id]['totaiuti']+= line.imp
        return res

    def _Calc_Tot(self, cr, uid, ids,name, args, context=None):
        res = {}
        for indi in self.browse(cr, uid, ids):
              res[indi.id] = 0.0
              for line in indi.famiglia:
                  res[indi.id]+= line.redd
        return res

    def _getrule(self, cr, uid, ids,field_name, args, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        res = {}
        if ids:
            objsindi=self.browse(cr, uid, ids)
            for indi in objsindi:
                res[indi.id]={'ruolo':'user','userente':False}
                if user.id==1 or indi.diocesi.id == user.id_ente.province.id:
                    res[indi.id]['ruolo']=user.ruolo
                if indi.comptenza.esterno:
                    if indi.comptenza.territorio.id == user.id_ente.territorio.id :
                        res[indi.id]['userente']=True
                else:
                    if indi.comptenza.id == user.id_ente.id:
                        res[indi.id]['userente']=True
        else:
            res=True
        return res

    _columns = {
        'name': fields.char('Cognome Nome', size=128, required=False),
        'cognome': fields.char('Cognome', size=128, required=True),
        'nome': fields.char('Nome', size=128, required=True),
        'indig_id': fields.char('Identification No', size=32),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128),
        'diocesi':fields.related('comptenza','province',type='many2one', relation='res.city', string='Diocesi', readonly=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'coniug': fields.char('Coniuge', size=32),
        'nazionalita': fields.many2one('res.country', 'Paese di Nascita',required=True),
        'nazionalitar': fields.many2one('res.country', 'Nazionalita',required=True),
        'luogonasc': fields.many2one('res.city', string='Città di nascita'),
        'comple': fields.date("Giorno di Nascita",required=True),
        'codfis': fields.char('Codice Fiscale', size=16, help="Codice fiscale"),
        'carcodfis':fields.char('Carica Tessera Sanitaria', size=200, help="Carica un  utente tramite Tessera Sanitaria"),
        'nci': fields.char('Carta Identità', size=32, help="Carta d'identita",required=True),
        'passaporto':fields.char('Passaporto', size=64,required=True),
        'permsog':fields.char('Permesso di Sogg.', size=64,required=True),
        'permvissog':fields.one2many('cbase.straniero','ind_id','',required=False),
        'sesso': fields.selection([('M', 'Maschio'),('F', 'Femmina')], 'Sesso', required=True),
        'stciv': fields.many2one('cbase.statocivile', 'Stato Civile', required=True),
        'risidente': fields.char('Frazione / Luogo',size=32),
        'cittares':fields.many2one('res.city', string='Residente a:'),
        'strada': fields.char('Indirizzo', size=128),
        'civico': fields.char('num Civico', size=12),
        'domiciliato':fields.many2one('res.city', string='Domiciliato a:'),
        'domfraz': fields.char('Frazione / Luogo',size=32),
        'domstrada': fields.char('Indirizzo', size=128),
        'domcivico': fields.char('num Civico', size=12),
        'tel': fields.char('Telefono', size=32, readonly=False),
        'cell': fields.char('Cellulare', size=32, readonly=False),
        'email': fields.char('E-mail', size=240),
        'foto': fields.binary('foto'),
        'active': fields.boolean('Attivo'),
        'occa': fields.boolean('Occasionale'),
        'righereddito':fields.one2many('cbase.righeredd','ind_id','Lista fonti di reddito mensile'),
        'straniero': fields.boolean('Persona Straniera'),
        'natoestero': fields.boolean('Nato all estero'),
        'patologie':fields.one2many('cbase.salute','ind_id','Patologie Utente',required=False),
        'famiglia':fields.one2many('cbase.famiglia','ind_id','Famigliari',required=False),
        'famigliaa':fields.one2many('cbase.famiglia','name','Parenti del capofamiglia'),
        'nucleof':fields.related('famigliaa','ind_id',type='many2one', relation='cbase.indigente', string='Nucleo Famigliare'),
        'totreddfam':fields.function(
            _Calc_Tot,method=True,type='float',
            string='Reddito Famigliare €'),
        'capofam':fields.function(
            _Calc_Totric,method=True,type='boolean',
            string='Reddito Famigliare €',store=False,multi='aiuti'),
        'finanze':fields.one2many('cbase.finanze','ind_id','Finanze',required=False),
        'casa':fields.one2many('cbase.casa','ind_id','Situazione Abitativa',required=False),
        'lavori':fields.one2many('cbase.lavoro','ind_id','Lavori',required=False),
        'veicoli':fields.one2many('cbase.veicoli','ind_id','Veicoli',required=False),
        'aiutenti':fields.one2many('cbase.erogenti','ind_id','Aiuti Erogati',required=False),
        'richieste':fields.one2many('cbase.richieste','ind_id','Aiuti Erogati',required=False),
        'totric':fields.function(_Calc_Totric,method=True,store=False,type='float',string='Tot. Richieste €',multi='aiuti'),
        'totricevas':fields.function(_Calc_Totric,store=False,method=True,type='float',string='Tot. Evaso €',multi='aiuti'),
        'totaiuti':fields.function(_Calc_Totric,method=True,store=False, type='float', string='Tot.Aiuti Enti ESTERNI Erogati €',multi='aiuti'),
        'borse':fields.one2many('cbase.borse','ind_id','Borse Spesa'),
        'borsecal':fields.one2many('cborse.calend','ind_id','Calendario e stato ritiri',readonly=True),
        'borsecons':fields.one2many('cborse.borsa','ind_id','Borse Consegnate',readonly=True),
        'deleg':fields.char('Delegato', size=128),
        'doc_del':fields.char('Documento', size=64),
        'linguep':fields.one2many('cbase.linguepar','ind_id','Lingue Utente',required=False),
        'scolariz':fields.many2one('cbase.scolar','Scolarizzazione', size=128),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'diario':fields.one2many('cbase.diario','ind_id','Diario'),
        'note':fields.text('Note Generali'),
        'refer':fields.many2one('rubrica.rubrica', 'Referenza',required=False),
        'userente':fields.function(_getrule,method=True, store=False,string='attiva',type='boolean',multi='utenti'),
        'ruolo':fields.function(_getrule,method=True, store=False,string='ruolo',type='char' , size=32,multi='utenti'),
    }
    _defaults = {
                'occa': False,
                'active': True,
                'straniero': False,
                'natoestero': False,
                'sesso':'m',
                'permsog':'N',
                'userente':True,
                'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
                'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
                'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
                }

    def check_for_borse(self,cr,uid,ids,context=None):
        if type(ids)==int:
            indi=ids
        if type(ids)==long:
            indi=ids
        elif type( ids ) == list:
            indi=ids[0]
        user=self.pool.get('res.users').browse(cr,uid,uid)
        obj_indi=self.browse(cr,uid,indi)
        if user.id_ente.viveri:
            if obj_indi.capofam == True:
                isee=self.get_currIsee(cr,uid,indi)
                if isee == -1:
                    return -1
                elif isee < user.id_ente.minisee:
                    return 1
                elif isee > user.id_ente.maxisee:
                    return 0
                elif (isee >=user.id_ente.minisee) and (isee <=user.id_ente.maxisee):
                    return 2
            else:
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'Questo utente non è un capofamiglia'))
                return False
        else:
            return 2

    def get_currIsee(self,cr,uid,ids,context=None):
        print ids

        if type(ids)==int:
            indi=ids
        if type(ids)==long:
            indi=ids
        elif type( ids ) == list:
            indi=ids[0]
        obj_indi=self.browse(cr,uid,indi)
        print 'oggetto ',obj_indi
        if obj_indi.finanze is None:
            return -1
        else:
            for f in obj_indi.finanze:
                if f.stato == 'att':
                    if f.aggisee != 0.0:
                        return f.aggisee
                    else:
                        return f.isee
        return -1



    def get_cognome_nome(self,nomecompl,offset=1):
        currname=nomecompl.split(' ')
        cnome=currname[-offset:]
        ccognome=currname[:-offset]
        nome=' '.join(cnome)
        cognome=' '.join(ccognome)
        return cognome,nome,offset

    def calcola_valida_cf(self, cr, uid,ids,context=None):
        res={}
        res['value']={}
        idd=ids
        indi=self.browse(cr,uid,idd)
        func=self.pool.get('cbase.func')
        naz=self.pool.get('res.country')
        nazcode=self.pool.get('cbase.codenazioni')
        citycode=self.pool.get('res.city')
        idcitta=self.read(cr,uid,idd,['luogonasc'])['luogonasc']
        italia=func.get_nazione(cr,uid)

        datan=func.get_data(indi.comple)
        sesso=indi.sesso
        testcode=indi.codfis
        nazione=indi.nazionalita.id
        offsetmax=len(indi.name.split(' '))
        offset=1
        cfok=''

        strerr=''

        cfexist=(len(indi.codfis)==16)

        if indi.nome=='' or indi.cognome=='':
            cognome,nome,offset=self.get_cognome_nome(indi.name)
            res['value']['cognome']=cognome
            res['value']['nome']=nome
            if  cfexist==False:
                res['value']['codfis']='N'+str(indi.id)
            else:
                res['value']['codfis']=indi.codfis
        print 'res nome cogn' , res
        if nazione==italia:
            if idcitta:
                res['value']['luogonasc']=idcitta[0]
                sitecode=indi.luogonasc.cadaster_code
            elif idcitta==False and cfexist:
                sitecode=indi.codfis[11:][:4]
                ret=citycode.search(cr,uid,[('cadaster_code','=',sitecode)])
                if ret!=[]:
                    idcitta=citycode.search(cr,uid,[('cadaster_code','=',sitecode)])
                    res['value']['luogonasc']=idcitta[0]
                else:
                    strerr='Città non codificata o inesistente'
                    return False,strerr,res
            elif idcitta==False and cfexist==False:
                strerr='Inserire la città di Nascita'
                return False,strerr,res
        else:
            print 'istat esteri',indi.nazionalita
            if indi.nazionalita.istat!=[]:
                sitecode=indi.nazionalita.istat[0].code1
            else:
                res['value']['codfis']='N'+str(indi.id)
                strerr='Spiacenti il codice nazione non ha codici catastali, informare amministratore di sistema. Grazie.'
                return False,strerr,res
        try:
            cognome,nome,offset=self.get_cognome_nome(indi.name,offset=offset)
        except:
            offset+=1
        prova=True
        if cfexist:
            while prova:
                if offset>offsetmax:
                    #~ strerr='Il codice fiscale non valido o dati inseriti in modo errato'
                    return True,strerr,res
                try:
                    cognome, nome,offset=self.get_cognome_nome(indi.name,offset=offset)
                    cfok=build(cognome, nome, datan, sesso, sitecode)
                    print cfok
                    if cfok ==testcode:
                        prova=False
                    else:
                        offset+=1
                except:
                    offset+=1
        else:
            try:
                cfok=build(cognome, nome, datan, sesso, sitecode)
            except:
                cfok='N'+str(indi.id)

        res['value']['codfis']=cfok


        return True,strerr,res

    def calcolacf(self,cr,uid,vals,context=None):
        func=self.pool.get('cbase.func')
        naz=self.pool.get('res.country')
        nazcode=self.pool.get('cbase.codenazioni')
        citycode=self.pool.get('res.city')
        italia=func.get_nazione(cr,uid)
        if 'comple' in vals:
            datan=func.get_data(vals['comple'])
        if 'luogonasc' in vals:
            idcitta=vals['luogonasc']
        if 'sesso' in vals:
            sesso=vals['sesso']
        if 'nazionalita' in vals:
            nazione=vals['nazionalita']
        if 'nome' in vals:
            nome=vals['nome']
        if 'cognome' in vals:
            cognome=vals['cognome']
        if nazione==italia:
            if idcitta:
                sitecode=citycode.browse(cr,uid,idcitta).cadaster_code
            elif idcitta==False:
                strerr='Inserire la città di Nascita'
                return False,strerr,vals
        else:
            test=nazcode.search(cr,uid,[('name','=',nazione)])
            if len(test)>0:
                sitecode=naz.browse(cr,uid,nazione).istat[0].code1
            else:
                strerr='Spiacenti il codice nazione non ha codici catastali, informare amministratore di sistema. Grazie.'
                return False,strerr,vals
        try:
            cfok=build(cognome, nome, datan, sesso, sitecode)
            vals['codfis']=cfok
            return True,'',vals
        except:
            return False,'impossibile calcolare il CF',vals

    def valida_dati_cf(self, cr, uid,id,vals,context=None):
        #~ build("rossi", "mario", datetime.datetime(1991, 1, 25), "M", "G693")
        res={}
        res['value']={}
        indi=self.browse(cr,uid,id)
        func=self.pool.get('cbase.func')
        naz=self.pool.get('res.country')
        nazcode=self.pool.get('cbase.codenazioni')
        citycode=self.pool.get('res.city')
        cfexist=False
        idcitta=False
        print indi.luogonasc
        if 'luogonasc' in vals:
            idcitta=vals['luogonasc']
        else:
            if indi.luogonasc:
                idcitta=indi.luogonasc.id

        italia=func.get_nazione(cr,uid)
        if 'comple' in vals:
            datan=func.get_data(vals['comple'])
        else:
            datan=func.get_data(indi.comple)
        if 'sesso' in vals:
            sesso=vals['sesso']
        else:
            sesso=indi.sesso
        if 'codfis' in vals:
            testcode=vals['codfis']
            if testcode:
                cfexist=(len(testcode)==16)
            else:
                cfexist=False
        else:
            testcode=indi.codfis

        if 'nazionalita' in vals:
            nazione=vals['nazionalita']
        else:
            nazione=indi.nazionalita.id

        offset=1
        cfok=''

        strerr=''

        if 'nome' in vals:
            nome=vals['nome']
        else:
            nome=indi.nome

        if 'cognome' in vals:
            cognome=vals['cognome']
        else:
            cognome=indi.cognome

        res['value']['cognome']=cognome
        res['value']['nome']=nome
        res['value']['codfis']=testcode
        if nazione==italia:
            if idcitta:
                res['value']['luogonasc']=idcitta
                sitecode=citycode.browse(cr,uid,idcitta).cadaster_code
            elif idcitta==False:
                strerr=u'Inserire la città di Nascita all utente %s %s ' % (cognome,nome)
                return False,strerr,res
        else:
            test=nazcode.search(cr,uid,[('name','=',nazione)])
            if len(test)>0:
                sitecode=naz.browse(cr,uid,nazione).istat[0].code1
            else:
                res['value']['codfis']='N'+str(id)
                strerr='Spiacenti il codice nazione non ha codici catastali, informare amministratore di sistema. Grazie.'
                return False,strerr,res
        if not cfexist:
            try:
                cfok=build(cognome, nome, datan, sesso, sitecode)
                res['value']['codfis']=cfok

            except:
                res['value']['codfis']='N'+str(id)




        return True,strerr,res

    def calcola_cf(self, cr, uid,ids,context=None):
        #~ build("rossi", "mario", datetime.datetime(1991, 1, 25), "M", "G693")
        print 'start CF'
        res={}
        return True

    def export(self,cr,uid,ids,context=None):
        print 'inizio aggiornamento'
        func=self.pool.get('cbase.func')
        italia=func.get_nazione(cr,uid)
        idss=self.search(cr,uid,[('id','>',0)])
        f = open(os.getcwd()+'/data/dati.csv',"wt")
        writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
        for id in idss:
            indi=self.browse(cr,uid,id)
            cognome=''
            nome=''
            if indi.cognome: cognome=indi.cognome
            if indi.nome: nome=indi.nome
            r='%s,%s,%s,%s' % (id,indi.codfis,indi.cognome,indi.nome)
            writer.writerow( r.split(',') )
        f.close()
        print 'fine aggiornamento'

    def importd(self,cr,uid,ids,context=None):
        print 'inizio aggiornamento'
        f = open(os.getcwd()+'/data/dati.csv',"rb")
        reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
        list=[]
        for row in reader:
            print row, '--->'
            ret=self.search(cr,uid,[('id','=',row[0])])
            if len(ret)>0:
                list.append(row[0])
                self.write(cr,uid,int(row[0]),{'codfis':row[1],'cognome':row[2],'nome':row[3]})
            print 'ok'
        f.close()
        idss=self.search(cr,uid,[('id','>',0)])
        for id in idss:
            stat,strerr,res=self.calcola_valida_cf(cr,uid,id)
            self.write(cr,uid,id,res['value'])

        print 'fine aggiornamento'

    def load_cf(self,cr,uid,nome,cognome,cf,context={}):
        city=self.pool.get('res.city')
        nazcode=self.pool.get('cbase.codenazioni')
        func=self.pool.get('cbase.func')
        italia=func.get_nazione(cr,uid)
        istat=cf[11:15]
        dtnas=get_birthday(cf)
        g,m,a=dtnas.split('-')
        a=str(func.calcdata(a))
        dtnasc=datetime.datetime(int(a),int(m),int(g))
        sesso=get_sex(cf)
        nom='%s %s' % (cognome.upper(),nome.upper())
        cfok=build(cognome, nome, dtnasc, sesso, istat)
        res={'value': {'name':nom,
                       'cognome':cognome.upper(),
                       'nome':nome.upper(),
                       'entec':self.pool.get('res.users').browse(cr, uid, uid, context).id_ente.name,
                       'comple':dtnasc.strftime('%Y-%m-%d'),
                       'sesso':sesso,
                       'codfis':cfok.upper(),
                       'nci':'n',
                       'passaporto':'n',
                       'permsog':'n',
                       'stciv':11 }
             }
        offsetmax=len(nom.split(' '))
        offset=1
        citycode=cf[11:][:4]

        sret=city.search(cr,uid,[('cadaster_code','=',ctrl_city_code(citycode))])
        if len(sret)>0:
            res['value']['nazionalita']=italia
            res['value']['nazionalitar']=italia
            res['value']['luogonasc']=sret[0]
        else:
            snret=nazcode.search(cr,uid,[('code1','=',ctrl_city_code(citycode))])
            if len(snret)>0:
                nazc=nazcode.browse(cr,uid,snret[0])
                res['value']['nazionalita']=nazc.name.id
                res['value']['nazionalitar']=nazc.name.id
        return res

    def on_change_codf(self,cr,uid,ids,nome,cognome,ccf,context=None):
        if nome!='' and cognome!='' and ids!=[]:
            if ccf and len(ccf)==16 and len(ccf)>0:
                ccf=ccf.upper()
                func=self.pool.get('cbase.func')
                italia=func.get_nazione(cr,uid)
                res=self.load_cf(cr,uid,nome,cognome,ccf)
                print res['value']['codfis']
                print ccf
                if res['value']['codfis']==ccf:
                    return res
                else:
                    raise orm.except_orm(_(u'Attenzione'), _(u'Il codice fiscale non è congruo con i dati inseriti, verificare nome e cognome o il codice'))
                    return False
            else:
                raise orm.except_orm(_(u'Attenzione'), _(u'La Tessera sanitaria non è valida mancano o eccedono caratteri'))
                return False
        else:
            raise orm.except_orm(_(u'Attenzione'), _(u'inserire nome e cognome'))
            return False

    def on_change_ccf(self,cr,uid,ids,ccf,context=None):
        if ccf and len(ccf)>30:
            t=ccf.split(' ')
            if t.count('')>=1:
                ccf=ccf.upper()
                func=self.pool.get('cbase.func')
                nome,cognome,cf,code= func.decodeCf(ccf)
                italia=func.get_nazione(cr,uid)
                return self.load_cf(cr,uid,nome,cognome,cf)
                #isvalid, get_birthday, get_sex, control_code, build
            else:
                raise orm.except_orm(_(u'Attenzione'), _(u'La Tessera sanitaria non è valida o è scaduta'))
                return False
        else:
            raise orm.except_orm(_(u'Attenzione'), _(u'Strisciare la tessera sanitaria nell\'apposito lettore'))
            return False

    def on_change_comptenza(self,cr,uid,ids,comptenza,context=None):
        fam=self.pool.get('cbase.famiglia')
        if comptenza and ids:
            o=self.browse(cr,uid,ids[0])
            if len(o.famiglia)>0:
                for f in o.famiglia:
                    self.write(cr,uid,f.name.id,{'comptenza':comptenza})

        else:
            return False

    def esistecf(self,cr,uid,cf):
        if cf:
            trov=self.search(cr,uid,[('codfis','=',cf)])
            if len(trov)>0:
                return True
            else:
                return False
        else:
            return False

    def on_change_naz(self,cr,uid,ids,naz,nazr,context=None):
        res = {'value':{}}
        basenaz=self.pool.get('cbase.func').get_nazione(cr,uid)
        indi=self.browse(cr,uid,ids)
        if not nazr:
            if naz:
                if naz!=basenaz:
                    res['value']['straniero']=True
                    res['value']['natoestero']=True
                else:
                    res['value']['straniero']=False
                    res['value']['natoestero']=False
            #print indi[0].permvissog

        else:
            if nazr!=basenaz:
                res['value']['straniero']=True
            else:
                res['value']['straniero']=False
            if naz!=basenaz:
                res['value']['natoestero']=True
                res['value']['luogonasc']=False
            else:
                res['value']['natoestero']=False
            #self.write(cr, uid, ids[0], res)
        return res

    def on_change_borse(self,cr,uid,ids,context=None):
        print ids
        res = {'value':{}}

        return res

    def _get_photo(self, cr, uid, context=None):
        photo_path = addons.get_module_resource('Caritas_base','images','photo.png')
        return open(photo_path, 'rb').read().encode('base64')

    def get_cod_id(self,cr,uid,ids,context=None):
        if type(ids)==list:
            cid=ids[0]
        else:
            cid=ids
        res=""
        indi=self.browse(cr, uid, cid)
        print 'trovato ',indi.indig_id
        if indi.indig_id==False or indi.indig_id=='':
            res="%dI%dA%sC%d" % (indi.id,indi.nazionalita,indi.sesso,indi.stciv)
            print 'codice',res
            return res
        else:
            return 0

    def Genera_codice_id(self,cr,uid,ids,context=None):
        """
            @param self: The object pointer
            @param cr: the current row, from the database cursor,
            @param uid: the current user’s ID for security checks,
            @param data: Get Data of CRM Meetings
            @param data_id: calendar's Id
            @param context: A standard dictionary for contextual values
        """
        cid=ids[0]
        res=""
        indi=self.browse(cr, uid, cid)
        if indi.indig_id==False or indi.indig_id=='':
            code={'indig_id': self.get_cod_id(cr,uid,ids,context)}
            self.write(cr, uid, cid, code)

    def Valida_Famiglia(self, cr, uid,ids,context=None):
        idd=self.pool.get('cbase.famiglia').search(cr,uid,[('ind_id','=',ids[0])])
        nomeconiuge=""
        print idd
        if idd != []:
            for famiglia in self.pool.get('cbase.famiglia').browse(cr, uid, idd):
                #~ print "famiglia.name ",famiglia.name
                #~ print "famiglia.ind_id ",famiglia.ind_id
                self.write(cr,uid,famiglia.name.id,{'nucleof':ids[0]})
                if famiglia.coniuge:
                    nomeconiuge=famiglia.name.name
                    self.write(cr, uid, ids[0], {'coniug': nomeconiuge})
        self.Genera_codice_id(cr, uid, ids, None)
        return {}

    def Valida_Aiuti(self, cr, uid,ids,context=None):
        self._Calc_Totaiuti(cr, uid, ids, None, None, context)
        self.Genera_codice_id(cr, uid, ids, None)
        return True

    def Valida_proposte(self, cr, uid,ids,context=None):
        self._Calc_Totric(cr, uid, ids, None, None, context)
        self.Genera_codice_id(cr, uid, ids, None)
        return True

    def genera(self,cr,uid,context=None):
        basenaz=self.pool.get('cbase.func').get_nazione(cr,uid)
        ids=self.search(cr,uid,[('nazionalita','!=',basenaz)])
        objs=self.browse(cr,uid,ids)
        print ids
        self.write(cr,uid,ids,{'straniero':True})
        for obj in objs:
            if obj.permsog!='':
                print {'name':obj.permsog,'tipo':1,'ind_id':obj.id}
                idr=self.pool.get('cbase.straniero').create(cr,uid,{'name':obj.permsog,'tipo':1,'ind_id':obj.id})
        return {}
    #~ (0, 0,  { values })    link to a new record that needs to be created with the given values dictionary
    #~ (1, ID, { values })    update the linked record with id = ID (write *values* on it)
    #~ (2, ID)                remove and delete the linked record with id = ID (calls unlink on ID, that will delete the object completely, and the link to it as well)
    #~ (3, ID)                cut the link to the linked record with id = ID (delete the relationship between the two objects but does not delete the target object itself)
    #~ (4, ID)                link to existing record with id = ID (adds a relationship)
    #~ (5)                    unlink all (like using (3,ID) for all linked records)
    #~ (6, 0, [IDs])          replace the list of linked IDs (like using (5) then (4,ID) for each ID in the list of IDs)

    def write(self, cr, uid, ids,vals, context=None):
        print 'write Indigente' , ids
        from copy import copy
        gric=False
        dia=False
        bors=False
        datadiario=''
        databorsa=''
        dataric=''
        indigente=-1
        righediario={}
        righediario={}
        if type(ids)==int:
            indigente=ids
        if type(ids)==long:
            indigente=ids
        elif type( ids ) == list:
            indigente=ids[0]

        newline=False
        tric=self.pool.get('cbase.definiz')
        oric=self.pool.get('cbase.richieste')
        ondiario=self.pool.get('cbase.diario')
        propente=self.pool.get('res.users').browse(cr, uid, uid).id_ente
        if vals.has_key('diario'):dia=True
        if vals.has_key('borse'):bors=True
        if vals.has_key('richieste') and vals['richieste']!=[]:
            gric=True


        diaaddme=True
        print vals
        retval={}
        retval['diario']=[]
        retvalric={}
        retvalric['richieste']=[]
        retvalbors={}
        retvalbors['borse']=[]
        #gestire le date del diario per  gli update
        if dia:
            for item in vals['diario']:
                if item[0]==0:
                    datadiario=item[2]['data']
                    if not righediario.has_key(datadiario):
                        righediario[datadiario]=item
                    if 'richieste' in item[2] and len(item[2]['richieste'])>0:
                        if not gric:
                            vals['richieste']=item[2]['richieste']
                            del(item[2]['richieste'])
                            gric=True
                        else:
                            vals['richieste'].append(item[2]['richieste'])
                elif item[0]==1:
                    loaddiario=ondiario.read(cr,uid,item[1],['data','diario','valore','erog'])
                    print ''
                    print '-----diario letto--  ',item[1]
                    print loaddiario
                    print ''
                    print ''
                    if item[2].has_key('data'):
                        datadiario=item[2]['data']
                    else:
                        datadiario=loaddiario['data']
                    if not righediario.has_key(datadiario):
                        righediario[datadiario]=item
                        print     'aggiunta diario? ',righediario[datadiario]
                    if not righediario[datadiario][2].has_key('diario'):
                        if loaddiario['diario']:
                            righediario[datadiario][2]['diario']=loaddiario['diario']
                    if item[2].has_key('richieste'):
                        if not gric:
                            vals['richieste']=item[2]['richieste']
                            del(item[2]['richieste'])
                            gric=True
                        else:
                            vals['richieste'].append(item[2]['richieste'])
                            del(item[2]['richieste'])
                else:
                    retval['diario'].append(item)
                cdia=item[2]
                if cdia:
                    if cdia.has_key('viveri'):
                        if cdia['viveri']:
                            diaaddme=True
                    if cdia.has_key('farmaci'):
                        if cdia['farmaci']:
                            diaaddme=True
                    if cdia.has_key('mensa'):
                        if cdia['mensa']:
                            diaaddme=True
                    if cdia.has_key('vestiario'):
                        if cdia['vestiario']:
                            diaaddme=True
                    if cdia.has_key('microcredito'):
                        if cdia['microcredito']:
                            diaaddme=True

        if bors:
            for item in vals['borse']:
                if item[0]==0:
                    databorsa=item[2]['dtini']
                    rigad=[0, False, {'diario': False,
                                       'data': databorsa}]
                    if not righediario.has_key(databorsa):
                        righediario[databorsa]=rigad

                    righediario[databorsa][2]['viveri']=True
                    strd=u'\nAggiunta pianificazione consgna viveri'# %s volta/e per %s mese/i a partire da %s' % (item[2]['qt'],item[2]['scad'],MESI[int(item[2]['primacons']-1)][1])
                    if righediario[databorsa][2]['diario']:
                        righediario[databorsa][2]['diario']+=strd
                    else:
                        righediario[databorsa][2]['diario']=strd
        if not bors or diaaddme:
            if dia:
                for itemrd in righediario:
                    if righediario[itemrd][2].has_key('viveri'):
                        if not bors:
                            vals['borse']=[]
                            bors=True
                        if righediario[itemrd][2]['viveri']:
                            ingiente=ids[0]
                            itemb=[0, False, {'primacons': datetime.date.today().strftime('%m'),
                                                      'nota': False, 'qt': '1', 'scad': '1','st_proc':0}]
                            vals['borse'].append(itemb)


        if gric:
            diaaddme=False
            for item in vals['richieste']:
                if item[0]==0:
                    ricdef=tric.browse(cr,uid,item[2]['defin'])
                    dataric=item[2]['dtda']
                    strd=''
                    rigad=[0, False, { 'diario': False,
                                       'data': dataric, }]
                    rigad[2][ricdef.tipo]=True

                    if not righediario.has_key(dataric):
                        righediario[dataric]=rigad

                    if ricdef.tipo!='norm':
                        righediario[dataric][2][ricdef.tipo]=True
                    nomestric=''
                    if ricdef.racc:
                        item[2]['stato']='evasa'
                        item[2]['erog']=ricdef.erog
                        item[2]['dterog']=dataric
                    for i in STAT_RIC:
                        if i[0]==item[2]['stato']:
                            nomestric=i[1]
                    if item[2].has_key('nota'):
                        notes=item[2]['nota']
                    else:
                        notes=''
                    strd+=u'\nAggiunta %s richiesta di %s da approvare' % (nomestric,ricdef.name)
                    if righediario[dataric][2].has_key('diario') and righediario[dataric][2]['diario']:
                        righediario[dataric][2]['diario']+=strd
                    else:
                        righediario[dataric][2]['diario']=strd

                if item[0]==1: #TODO gestire i pulsanti delle richieste
                    strd=''
                    oricr=oric.browse(cr,uid,item[1])
                    ricdef=tric.browse(cr,uid,oricr.defin.id)
                    dataric=oricr.dtda
                    inota=''
                    erog=0.0

                    rigad=[0, False, { 'diario': False,
                                       'data': dataric, }]
                    if item[2].has_key('nota'):
                        inota=item[2]['nota']

                    if item[2].has_key('dterog'):
                        dataric=item[2]['dterog']
                        rigad[2]['data']=dataric
                    if not righediario.has_key(dataric):
                        righediario[dataric]=rigad
                        newline=True
                    nomestric=''
                    if item[2].has_key('erog'):
                        for i in STAT_RIC:
                            if i[0]==oricr.stato:
                                nomestric=i[1]
                        if oricr.stato=='pian' or oricr.stato=='rif':
                            strd=u'\n %s richiesta di %s' % (nomestric,ricdef.name)
                    if righediario[dataric][2]['diario']:
                        righediario[dataric][2]['diario']+=strd
                    else:
                        righediario[dataric][2]['diario']=strd

        if diaaddme and dia:
            for itemrd in righediario:
                cdia=righediario[itemrd][2]
                if cdia.has_key('viveri') or cdia.has_key('farmaci') or cdia.has_key('mensa')  or cdia.has_key('vestiario') or cdia.has_key('microcredito'):
                    if not gric:
                        vals['richieste']=[]
                defin=0
                if righediario[itemrd][2].has_key('viveri'):
                    if righediario[itemrd][2]['viveri']:
                        rigaricv=[ 0, False, {'dtda': itemrd,'defin': defin, 'valore': 0, 'stato': 'new'}]
                        idricdef=tric.search(cr,uid,[('tipo','=','viveri')])
                        rigaricv[2]['defin']=idricdef[0]
                        loadtric=tric.read(cr,uid,idricdef)[0]
                        rigaricv[2]['valore']=loadtric['valore']
                        ricdef=tric.browse(cr,uid,idricdef[0])
                        if ricdef.racc:
                            rigaricv[2]['stato']='evasa'
                            rigaricv[2]['erog']=ricdef.erog
                        vals['richieste'].append(rigaricv)
                if righediario[itemrd][2].has_key('farmaci'):
                    if righediario[itemrd][2]['farmaci']:
                        rigaricf=[ 0, False, {'dtda': itemrd,'defin': defin, 'valore': 0, 'stato': 'new'}]
                        idricdef=tric.search(cr,uid,[('tipo','=','farmaci')])
                        ingiente=ids[0]
                        rigaricf[2]['defin']=idricdef[0]
                        loadtric=tric.read(cr,uid,idricdef)[0]
                        rigaricf[2]['valore']=loadtric['valore']
                        ricdef=tric.browse(cr,uid,idricdef[0])
                        if ricdef.racc:
                            rigaricf[2]['stato']='evasa'
                            rigaricf[2]['erog']=ricdef.erog
                            rigaricf[2]['buono']='proc'
                        vals['richieste'].append(rigaricf)
                if righediario[itemrd][2].has_key('mensa'):
                    if righediario[itemrd][2]['mensa']:
                        rigaricm=[ 0, False, {'dtda': itemrd,'defin': defin, 'valore': 0, 'stato': 'new'}]
                        idricdef=tric.search(cr,uid,[('tipo','=','mensa')])
                        ingiente=ids[0]
                        rigaricm[2]['defin']=idricdef[0]
                        loadtric=tric.read(cr,uid,idricdef)[0]
                        rigaricm[2]['valore']=loadtric['valore']
                        ricdef=tric.browse(cr,uid,idricdef[0])
                        if ricdef.racc:
                            rigaricm[2]['stato']='evasa'
                            rigaricm[2]['erog']=ricdef.erog
                            rigaricm[2]['buono']='proc'
                        vals['richieste'].append(rigaricm)
                if righediario[itemrd][2].has_key('microcredito'):
                    if righediario[itemrd][2]['microcredito']:
                        rigaricmi=[ 0, False, {'dtda': itemrd,'defin': defin, 'valore': 0, 'stato': 'new'}]
                        idricdef=tric.search(cr,uid,[('tipo','=','microcredito')])
                        ingiente=ids[0]
                        rigaricmi[2]['defin']=idricdef[0]
                        loadtric=tric.read(cr,uid,idricdef)[0]
                        rigaricmi[2]['valore']=loadtric['valore']
                        ricdef=tric.browse(cr,uid,idricdef[0])
                        if ricdef.racc:
                            rigaricmi[2]['stato']='evasa'
                            rigaricmi[2]['erog']=ricdef.erog
                        vals['richieste'].append(rigaricmi)
                if righediario[itemrd][2].has_key('dormitorio'):
                    if righediario[itemrd][2]['dormitorio']:
                        rigaricd=[ 0, False, {'dtda': itemrd,'defin': defin, 'valore': 0, 'stato': 'new'}]
                        idricdef=tric.search(cr,uid,[('tipo','=','dormitorio')])
                        ingiente=ids[0]
                        rigaricd[2]['defin']=idricdef[0]
                        loadtric=tric.read(cr,uid,idricdef)[0]
                        rigaricd[2]['valore']=loadtric['valore']
                        ricdef=tric.browse(cr,uid,idricdef[0])
                        if ricdef.racc:
                            rigaricd[2]['stato']='evasa'
                            rigaricd[2]['erog']=ricdef.erog
                        vals['richieste'].append(rigaricd)
                if righediario[itemrd][2].has_key('vestiario'):
                    if righediario[itemrd][2]['vestiario']:
                        rigaricve=[ 0, False, {'dtda': itemrd,'defin': defin, 'valore': 0, 'stato': 'new'}]
                        idricdef=tric.search(cr,uid,[('tipo','=','vestiario')])
                        ingiente=ids[0]
                        rigaricve[2]['defin']=idricdef[0]
                        loadtric=tric.read(cr,uid,idricdef)[0]
                        rigaricve[2]['valore']=loadtric['valore']
                        ricdef=tric.browse(cr,uid,idricdef[0])
                        if ricdef.racc:
                            rigaricve[2]['stato']='evasa'
                            rigaricve[2]['erog']=ricdef.erog
                        vals['richieste'].append(rigaricve)


        for itemd in righediario:
            print 'itemd ',itemd
            dret=ondiario.search(cr,uid,[('data','=',itemd),('ind_id','=',ids[0])])
            if dret!=[] and righediario[itemd][0]==0:
                print dret
                objd=ondiario.browse(cr,uid,dret[0])
                res=ondiario.read(cr,uid,dret,righediario[itemd][2].keys())
                righediario[itemd][0]=1
                righediario[itemd][1]=dret[0]
                if righediario[itemd][2].has_key('data'): del righediario[itemd][2]['data']
                for r in righediario[itemd][2]:
                    if r != 'ind_id' and r !='data' and res[0].has_key(r) and res[0][r]:
                        righediario[itemd][2][r]=(righediario[itemd][2][r]+res[0][r])

            retval['diario'].append(righediario[itemd])


        vals['diario']=retval['diario']

        if vals.has_key('nome') and vals.has_key('cognome'):
            vals['name']='%s %s' % (vals['cognome'],vals['nome'])
        elif 'nome' in vals:
            vals['cognome']=self.browse(cr,uid,indigente).cognome
            vals['name']='%s %s' % (vals['cognome'],vals['nome'])
        elif 'cognome' in vals:
            vals['nome']=self.browse(cr,uid,indigente).nome
            vals['name']='%s %s' % (vals['cognome'],vals['nome'])

        stat=True
        try:
            stat,strerr,res=self.valida_dati_cf(cr,uid,indigente,vals)
            print stat ,strerr,res
            if stat:
                vals.update(res['value'])
            else:
                raise orm.except_orm(_(u'Attenzione'), _(strerr))
                return stat
        except TypeError:
            pass



        if type( ids ) == list and  type( ids[0] ) == dict and len(ids[0].keys())==0:#ids==[{}]:
            self.create(cr,uid,vals,context)
            self.Genera_codice_id(cr,uid,ids,context)
            return True
        else:
            if 'indig_id' not in vals:
                code=self.get_cod_id(cr,uid,ids,context)
                if code!=0:
                    vals['indig_id']=code
            return super(cbase_indigente,self).write(cr, uid,ids, vals, context)

    def create(self, cr, uid, vals, context=None):
        res={}
        print 'Create Indigente'
        print ''
        print vals
        print ''
        vals['name']=''
        vals['name']='%s %s' % (vals['cognome'],vals['nome'])
        if 'codfis' in vals and vals['codfis']==False:
            succ,strerr,vals=self.calcolacf(cr,uid,vals)
            if succ==False:
                raise orm.except_orm(_(u'Attenzione'), _(strerr))
        if 'codfis' in vals and vals['codfis']!=False:
            if self.esistecf(cr,uid,vals['codfis']):
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'Il codice Fiscale %s \n che si sta tentando di caricare esiste già ') % vals['codfis'])

        a=super(cbase_indigente,self).create(cr, uid, vals, context)
        self.Genera_codice_id(cr,uid,[a],context)
        return a

    def s_salute(self, cr, uid, ids, context=None):
        res={'value': {'cview':'sal'}}
        print res
        return res

    _sql_constraints = [
            ('_cf_uniq', 'unique(codfis)', u'Attenzione!! CODICE FISCALE GIA\' PRESENTE NELL ARCHIVIO'),
        ]
    _order = 'id desc'
    _log_access='True'
cbase_indigente()

class cbase_stasts(orm.Model):
    _name = 'cbase.stats'
    _description = 'Tabella base statistiche'
    def _getrule(self, cr, uid, ids,field_name, args, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        res = {}
        if ids:
            objsindi=self.browse(cr, uid, ids)
            for indi in objsindi:
                res[indi.id]={'ruolo':'user','userente':False}
                if user.id==1 or indi.diocesi.id == user.id_ente.province.id:
                    res[indi.id]['ruolo']=user.ruolo
                if indi.comptenza.esterno:
                    if indi.comptenza.territorio.id == user.id_ente.territorio.id :
                        res[indi.id]['userente']=True
                else:
                    if indi.comptenza.id == user.id_ente.id:
                        res[indi.id]['userente']=True
        else:
            res=True
        return res

    _columns = {
        'name': fields.char('Nome Report', size=128, required=False),
        'anno':fields.integer('Anno',required=True),
        'italiani': fields.integer(' Famiglie di Italiani I Semestre'),
        'stranieri': fields.integer(' Famiglie di stranieri I Semestre'),
        'italiani_2_sem': fields.integer(' Famiglie di Italiani II Semestre'),
        'stranieri_2_sem': fields.integer(' Famiglie di stranieri II Semestre'),
        'numero_famigliari':fields.float('Media componenti Famigliari I Semestre',digits=(3, 2)),
        'numero_normale':fields.integer('di cui Famiglie di fatto I Semestre'),
        'numero_monoparentali':fields.integer(' e Famiglie monoparentali I Semestre'),
        'numero_piccoli':fields.integer('di cui con Figli da 0 a 3 anni I Semestre'),
        'numero_minorenni':fields.integer('di cui con Figli da 3 a 18 anni I Semestre'),
        'numero_anziani':fields.integer('Famiglie Anziani (>64) I Semestre'),
        'numero_2_famigliari':fields.float('Media componenti Famigliari II Semestre',digits=(3, 2)),
        'numero_2_normale':fields.integer('di cui Famiglie di fatto II Semestre'),
        'numero_2_monoparentali':fields.integer(' e Famiglie monoparentali II Semestre'),
        'numero_2_piccoli':fields.integer('di cui con Figli da 0 a 3 anni II Semestre'),
        'numero_2_minorenni':fields.integer('di cui con Figli da 3 a 18 anni II Semestre'),
        'numero_2_anziani':fields.integer('Famiglie Anziani (>64) II Semestre'),
        'media_borse_1':fields.float('Media Borse I semeste',digits=(3, 2)),
        'media_borse_2':fields.float('Media Borse II sem',digits=(3, 2)),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128),
        'diocesi':fields.related('comptenza','province',type='many2one', relation='res.city', string='Diocesi', readonly=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'note':fields.text('Note Generali'),
        'userente':fields.function(_getrule,method=True, store=False,string='attiva',type='boolean',multi='utenti'),
        'ruolo':fields.function(_getrule,method=True, store=False,string='ruolo',type='char' , size=32,multi='utenti'),
    }
    _defaults = {
                'userente':True,
                'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
                'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
                'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
                }
    def get_dat(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))

    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return datetime.datetime(int(a),int(m),int(g))

    def get_anni(self,data):
        return int((date.today()-self.get_dat(data)).days/(365.25))


    def calcola(self, cr, uid,ids,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        ric=self.browse(cr, uid, ids[0])
        anno=ric.anno
        print 'anno ricerca %d' % anno
        i='%d-1-1' % anno
        meta='%d-6-1' % anno
        fin='%d-12-31' % anno
        Isemestre=self.gen_data(i)
        IIsemestre=self.gen_data(meta)
        fine=self.gen_data(fin)
        nazcode=self.pool.get('cbase.codenazioni')
        func=self.pool.get('cbase.func')
        italia=func.get_nazione(cr,uid)
        tb_ind=self.pool.get('cbase.indigente')
        borse_cons=self.pool.get('cborse.borsa')
        unic_id_1_sem=[]
        unic_id_2_sem=[]
        italiani=0
        numero_fam=0.0
        stranieri=0
        fam_normale=0
        fam_conciventi=0
        fam_monopar=0
        fam_con_minor=0
        fam_con_minorenni=0
        fam_anziani=0
        singoli=0
        italiani_2=0
        numero_fam_2=0.0
        stranieri_2=0
        fam_normale_2=0
        fam_conciventi_2=0
        fam_monopar_2=0
        fam_con_minor_2=0
        fam_con_minorenni_2=0
        fam_anziani_2=0
        singoli_2=0

        numero_borse_I_sem=0.0
        numero_borse_II_sem=0.0

        ric_1_sem=[('comptenza','=',user.id_ente.id),
                    ('dtini','>=',Isemestre.strftime('%d-%m-%Y %H:%M:%S')),
                    ('dtini','<',IIsemestre.strftime('%d-%m-%Y %H:%M:%S')),
                    ('state','in',['compl'])]
        ric_2_sem=[('comptenza','=',user.id_ente.id),
                    ('dtini','>=',IIsemestre.strftime('%d-%m-%Y %H:%M:%S')),
                    ('dtini','<=',fine.strftime('%d-%m-%Y %H:%M:%S')),
                    ('state','in',['compl'])]
        ids_1_sem=borse_cons.search(cr,uid,ric_1_sem)
        lista_1_sem=borse_cons.browse(cr,uid,ids_1_sem)
        ids_2_sem=borse_cons.search(cr,uid,ric_2_sem)
        lista_2_sem=borse_cons.browse(cr,uid,ids_2_sem)
        for b in lista_1_sem:
            if b.ind_id.id not in unic_id_1_sem:
                unic_id_1_sem.append(b.ind_id.id)
                if b.ind_id.nazionalita.id!=italia:
                    stranieri+=1
                else:
                    italiani+=1
                if self.get_anni(b.ind_id.comple) > 64:
                    fam_anziani+=1

                numero_fam+=len(b.fam_id)+1

                coniuge=False
                minori=False
                minorenni=False
                if len(b.fam_id)>0:
                    for f in b.fam_id:
                        if f.grad.name=='Coniuge':
                            coniuge=True
                        if f.eta<=3:
                            minori=True
                        if f.eta>3 and f.eta<18:
                            minorenni=True
                else:
                    singoli+=1
                if minori:fam_con_minor+=1
                if minorenni:fam_con_minorenni+=1
                if coniuge:
                    fam_normale+=1
                else:
                    fam_monopar+=1


        for b in lista_2_sem:
            if b.ind_id.id not in unic_id_2_sem:
                unic_id_2_sem.append(b.ind_id.id)
                if b.ind_id.nazionalita.id!=italia:
                    stranieri_2+=1
                else:
                    italiani_2+=1
                if self.get_anni(b.ind_id.comple) > 64:
                    fam_anziani_2+=1

                numero_fam_2+=len(b.fam_id)+1

                coniuge=False
                minori=False
                minorenni=False
                if len(b.fam_id)>0:
                    for f in b.fam_id:
                        if f.grad.name=='Coniuge':
                            coniuge=True
                        if f.eta<=3:
                            minori=True
                        if f.eta>3 and f.eta<18:
                            minorenni=True
                else:
                    singoli_2+=1
                if minori:fam_con_minor_2+=1
                if minorenni:fam_con_minorenni_2+=1
                if coniuge:
                    fam_normale_2+=1
                else:
                    fam_monopar_2+=1



        numero_borse_I_sem=len(lista_1_sem)
        numero_borse_II_sem=len(lista_2_sem)
        print 'borse primo semestre %d' % numero_borse_I_sem
        print 'id unici primo semestre %d' % len(unic_id_1_sem)
        print 'borse secondo semestre %d' % numero_borse_II_sem
        print 'id unici secondo semestre %d' % len(unic_id_2_sem)
        media_borse_I_Sem=float(numero_borse_I_sem/len(unic_id_1_sem))
        media_borse_II_Sem=float(numero_borse_II_sem/len(unic_id_2_sem))

        res={
            'italiani':italiani,
            'stranieri':stranieri,
            'numero_famigliari':float(numero_fam/(italiani+stranieri)),
            'numero_normale':fam_normale+fam_conciventi,
            'numero_monoparentali':fam_monopar,
            'numero_piccoli':fam_con_minor,
            'numero_minorenni':fam_con_minorenni,
            'numero_anziani':fam_anziani,

            'italiani_2_sem':italiani_2,
            'stranieri_2_sem':stranieri_2,
            'numero_2_famigliari':float(numero_fam_2/(italiani_2+stranieri_2)),
            'numero_2_normale':fam_normale+fam_conciventi,
            'numero_2_monoparentali':fam_monopar_2,
            'numero_2_piccoli':fam_con_minor_2,
            'numero_2_minorenni':fam_con_minorenni_2,
            'numero_2_anziani':fam_anziani_2,
            'media_borse_1':media_borse_I_Sem,
            'media_borse_2':media_borse_II_Sem,
             }
        print res
        self.write(cr,uid,ids,res)
        return True


class res_users(orm.Model):
    _inherit = 'res.users'
    _description = 'User'

    _columns = {
        'nome':fields.char('Cognome Nome', size=64, required=False),
        'id_ente': fields.many2one('cbase.enti', 'Ente',required=True),
        'ruolo': fields.selection([('conf', 'Tutti i privilegi'),
                                  ('dioc', 'Responsabile Diocesano'),
                                  ('user', 'Utente')], 'Ruolo Caritas', required=True),
    }
    _defaults = {
                'ruolo':'user',
                'id_ente':1
                }
    def aggiorna_schede(self, cr, uid,ids,context=None):
        print 'inizio aggiornamento'
        partner=self.pool.get('res.partner')
        nazcode=self.pool.get('cbase.codenazioni')
        func=self.pool.get('cbase.func')
        idss=self.pool.get('res.users').search(cr,uid,[('id','>',1)])
        basen=self.pool.get('cbase.func').get_nazione(cr,uid)
        if idss:
            objsindi=self.pool.get('res.users').browse(cr, uid, idss)
            for user in objsindi:
                vals={'name':'','email':'','country_id':basen,'customer':False}
                print user.nome
                vals['name']=user.nome
                vals['email']=user.email
                ret_id=partner.create(cr,uid,vals)
                self.write(cr,uid,[user.id],{'partner_id':ret_id})
        print 'fine aggiornamento'

res_users()

class res_country(orm.Model):
    _inherit = 'res.country'
    _description = 'res.country'

    _columns = {
                'code': fields.char('Country Code', size=4,
                        help='The ISO country code in two chars.\n'
                             'You can use this field for quick search.', required=True),
                'istat':fields.one2many('cbase.codenazioni','name','Codici'),

    }
    _order = 'name asc'
res_country()


class cbase_codenazioni(orm.Model):
    _name = "cbase.codenazioni"
    _description = "codici nazioni estere"
    _columns = {
        'name':fields.many2one('res.country','Nazione',size=128),
        'code1':fields.char('codice castastale 1', size=4,required=True),
        'dal':fields.date("Data"),
        'code2':fields.char('codice castastale 2', size=4),
        'dal2':fields.date("ultima"),
    }
    def convertdata(self,value):
        val=value.split('-')
        if len(val)>1:
            g,m,a=val[:3]
            md=date(int(a),int(m),int(g))
            return md.strftime('%Y-%m-%d')
        else:
            return False

    def importdata(self, cr, uid,context=None):
        #~ nomatch
        print 'inizio importazione'
        import re
        naz=self.pool.get('res.country')
        istat=self.pool.get('cbase.codenazioni')
        idnm=naz.search(cr,uid,[('name','=','nomatch')])
        wb = open_workbook(os.getcwd()+'/data/ESTERI.xls')
        values = []
        nomacth=[]
        rowmatch=[]
        context={}
        s=wb.sheets()[0]
        tosearch=naz.search(cr,uid,[('name','!=','')])
        context['lang']='it_IT'
        dati=naz.read(cr,uid,tosearch,['id','name'],context=context)
        for dat in dati:
            dizio={'name':'','ids':[],'valori':[]}
            tf=dat['name']
            currid=dat['id']
            pattern=re.compile(tf.lower(),re.IGNORECASE)
            print 'importing--> ',tf
            tf=tf.strip
            for row in range(s.nrows):
                tom=s.cell(row,1).value.strip()
                if bool(re.match(pattern,tom)):
                    rowmatch.append(row)
                    vals={  'name':currid,
                            'code1':s.cell(row,2).value,
                            'dal':self.convertdata(s.cell(row,3).value),
                            'code2':s.cell(row,4).value,
                            'dal2':self.convertdata(s.cell(row,5).value)}
                    istat.create(cr,uid,vals)

        print 'Importazione Nazioni not macthing'
        cnt=0
        for row in range(s.nrows):
            if row not in rowmatch:
                code=str(cnt).zfill(4)
                ctrl=naz.search(cr,uid,[('name','=',s.cell(row,1).value)],context=context)
                print 'control -> ', ctrl
                if len(ctrl)==0:
                    cid=naz.create(cr,uid,{'name':s.cell(row,1).value,'code':code})
                else:
                    cid=ctrl[0]
                vals={  'name':cid,
                        'code1':s.cell(row,2).value,
                        'dal':self.convertdata(s.cell(row,3).value),
                        'code2':s.cell(row,4).value,
                        'dal2':self.convertdata(s.cell(row,5).value)}
                istat.create(cr,uid,vals)
                cnt+=1

        print 'fine importazione'

    _order = 'dal desc'
cbase_codenazioni()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

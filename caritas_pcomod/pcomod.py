# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------------
#   Modulo Gestione  Fondo Case
#   Autore: Alessio Gerace
#   data  : gennaio 2012
#   update : febbraio 2012
#   
#
#
#
#
##############################################################################

from osv import osv,fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp
import string
import resource 
import tools
import addons
import datetime
import calendar
import operator
import math

def roundf(f, p):
     from decimal import *
     return float(Decimal(str(f)).quantize(Decimal(str(pow(10,-p)))))
     
class pcomod_prodotti(osv.osv):
	_name = "pcomod.prodotti"
	_description = "Servizi e unità misura da dare in comodato"
	_columns = {
		'name':fields.char('Nome attrezzatura o servizio', size=128,required=True),
		'descrizione':fields.char('Descrizione', size=64),
		'um':fields.selection([('pezzi', 'Pezzi'),
								('km', 'Kilometri'),
								('corpo', 'Totale')],'Unita di misura'),
		'val':fields.float('Valore unitario €',digits=(6,2))
		}
	_defaults = {
		'um':'pezzi',
		'val':1.00
		}
pcomod_prodotti()

class pcomod_main(osv.osv):
	_name = "pcomod.main"
	_description = "Gestione Furgone Lisa AdD"
	_columns = {
		'name':fields.many2one('res.partner', 'Utente',required=True, select=True),
		'nome':fields.char('None Beneficiario', size=128),
		'indirizzo':fields.char('indirizzo', size=128),
		'zip': fields.char('Cap', size=5),
		'citta': fields.char('Città', size=128),
		'provincia': fields.char('Provincia', size=128),
		'tel':fields.char('Telefono', size=32),
		'fax':fields.char('Fax', size=64),
		'mobile':fields.char('Cellulare', size=64),
		'destinazione':fields.char('Destinazione', size=128),
		'utilizzo':fields.char('Utilizzo a cura di', size=128),
		'servizio':fields.many2one('pcomod.prodotti','Servizio',required=True,select=True),
		'qt':fields.float('Quantità', required=True, digits=(10,2)),
		'um':fields.char('Unità', size=64),
		'val':fields.float('Valore unitario €',digits=(6,2)),
		'dataini':fields.datetime("Dal",required=True),
		'datafine':fields.datetime("al",required=True),
		'periodo':fields.float('Periodo Utilizzo ore',digits=(6,2),readonly=True),
		'donazione':fields.float('Donazione €', required=True, digits=(6,2)),
		'dirtcode':fields.integer('Approvato direttivo '),
		'dirtdata':fields.date('del '),
		'costo':fields.float('Costo Servizio €',digits=(6,2)),
		'stato':fields.selection([('new', 'Nuova'),
                                 ('pian', 'Da confermare'),
                                 ('conf', 'Confermato'),
                                 ('annul','Annullato'),
                                 ('scad', 'Scaduto'),], 'Stato'),
		'note':fields.text('Note di questo evento'),	
		'dtin': fields.datetime("Data inserimento",readonly=True),
		'opinsft':fields.char('Operatore', size=32)
		}
	_defaults = {
		'stato':'new',
		'qt':1.00,
		'donazione':0.00,
		'dataini':lambda *a: datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
		'dtin':fields.date.context_today,
		'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
		}

	def on_change_name(self, cr, uid, ids, user):
		print ids
		res = {'value':{}}
		if(user):
			utente = self.pool.get('res.partner').browse(cr, uid, user)
			res = {'value': {
				'nome':utente.name,
				'indirizzo':utente.street,
				'zip':utente.zip,
				'citta':utente.city,
				'provincia':utente.province.name,
				'tel':utente.phone,
				'fax':utente.fax,
				'mobile':utente.mobile,
				}}
		return res
		
	def on_change_servio(self, cr, uid, ids, servizio):
		res = {'value':{}}
		if(servizio):
			serv = self.pool.get('pcomod.prodotti').browse(cr, uid, servizio)
			res = {'value': {
				'um':serv.um,
				}}
		return res
		
	def on_change_dtfine(self, cr, uid, ids, dataf,datai):
		res = {'value':{}}
		datafine = datetime.datetime.strptime(dataf, "%Y-%m-%d %H:%M:%S")
		datainizio = datetime.datetime.strptime(datai, "%Y-%m-%d %H:%M:%S")
		if datainizio>=datafine:
			stri='La data di inizio deve essere minore della data di fine'
			raise osv.except_osv(_('Attenzione'), _(stri))


	def Salva(self,cr,uid,ids,context=None):
		pianif=self.browse(cr, uid, ids[0])
		if pianif.stato=='new' or pianif.stato=='pian':
			print pianif.qt
			print pianif.val
			print pianif.periodo
			print pianif.costo
			print pianif.dataini
			print pianif.datafine
			print pianif.um
			print pianif.servizio.um
			
			datafine = datetime.datetime.strptime(pianif.datafine, "%Y-%m-%d %H:%M:%S")
			datainizio = datetime.datetime.strptime(pianif.dataini, "%Y-%m-%d %H:%M:%S")
			delta=(datafine-datainizio)
			periodo=float((delta.days*24)+(delta.seconds/3600))
			costo=float(pianif.qt*pianif.servizio.val)
			inicomp=self.search(cr,uid,[('dataini','<=',pianif.dataini),('datafine','>=',pianif.dataini),('servizio','=',pianif.servizio.id),('stato','<>','annul')])
			finecomp=self.search(cr,uid,[('dataini','<=',pianif.datafine),('datafine','>=',pianif.datafine),('servizio','=',pianif.servizio.id),('stato','<>','annul')])
			prenosup=self.search(cr,uid,[('dataini','>=',pianif.dataini),('datafine','<=',pianif.datafine),('servizio','=',pianif.servizio.id),('stato','<>','annul')])
			if inicomp.__contains__(pianif.id):
				inicomp.remove(pianif.id)
			if finecomp.__contains__(pianif.id):
				finecomp.remove(pianif.id)
			if prenosup.__contains__(pianif.id):
				prenosup.remove(pianif.id)
			self.write(cr,uid,ids[0],{'um':pianif.servizio.um,
									  'val':roundf(pianif.servizio.val,2),
									  'periodo':roundf(periodo,2),
									  'costo':roundf(costo,2)})
			if finecomp==[] and inicomp==[] and prenosup==[]:
				self.write(cr,uid,ids[0],{'stato':'pian'})
			else:
				if finecomp<>[] and inicomp<>[]:
					nomeutuso=self.browse(cr, uid, finecomp[0]).nome
					str=u'Nelle date scelte il %s è occupato da %s ' % (pianif.servizio.name,nomeutuso)
				elif inicomp<>[]:
					nomeutuso=self.browse(cr, uid, inicomp[0]).nome
					str=u'La data di inizio conicide con la prenotazione  del %s di %s Modificare data e/o ora' % (pianif.servizio.name,nomeutuso)
				elif finecomp<>[]:
					nomeutuso=self.browse(cr, uid, finecomp[0]).nome
					str=u'La data di fine conicide con la prenotazione  del %s di %s Modificare data e/o ora' % (pianif.servizio.name,nomeutuso)
				elif prenosup<>[]:
					str=u'Impossibile pianificare il %s si sovrappone a %d prenotazioni Modificare le date' % (pianif.servizio.name,len(prenosup))
				raise osv.except_osv(_('Info'), _(str))
		else:
			return True
		
	def Conferma(self,cr,uid,ids,context=None):
		pianif=self.browse(cr, uid, ids[0])
		if pianif.stato=='pian':
			self.write(cr,uid,ids[0],{'stato':'conf'})
		return True	
		
	def Annulla(self,cr,uid,ids,context=None):
		return self.write(cr,uid,ids[0],{'stato':'annul'})
		
			
	_order = 'dtin desc'
pcomod_main()


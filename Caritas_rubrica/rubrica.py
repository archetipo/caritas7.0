# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------------
#   Modulo Gestione Rubrica
#   Autore: Alessio Gerace
#   data  : Maggio 2011
#   update : Giugno 2011
#
#
#
#
#
##############################################################################
from openerp.tools.translate import _
from openerp.osv import orm, fields
from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource
import addons
import datetime
import calendar
import operator
import math



class rubrica_rubrica(orm.Model):
	_name = "rubrica.rubrica"
	_description = "Rubrica"
	_columns = {
		'name':fields.char('Nome o Ragione Sociale', size=64,select=True,required=True),
		'ind':fields.char('indirizzo', size=64,required=True),
		'ind2':fields.char('indirizzo', size=64),
		'zip': fields.char('CAP', size=24),
		'municipality': fields.many2one('res.city', 'Citta'),
		'province': fields.many2one('res.province', string='Provincia'),
		'region': fields.many2one('res.region', string='Regione'),
		'tel':fields.char('Telefono', size=32),
		'fax':fields.char('Fax', size=32),
		'mail':fields.char('Mail', size=32),
		'web':fields.char('Web', size=32),
		'legrap':fields.char('Legale Rappresentante', size=32),
		'nasc':fields.char('Nato a', size=32),
		'il':fields.char('il', size=32),
		'codente':fields.char('Codice Agea Ente', size=32),
		'nota': fields.text('Aggiungi un Note'),
		'opinsft':fields.char('Operatore', size=32),
		'opinsf':fields.char('Operatore', size=128,readonly=True),
		'dtin': fields.datetime("Data inserimento",readonly=True),
			}
	_defaults = {
			'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
			'dtin':fields.date.context_today,
			}

	def on_change_municipality(self, cr, uid, ids, city):
		res = {'value':{}}
		if(city):
			city_obj = self.pool.get('res.city').browse(cr, uid, city)
			res = {'value': {
				'province':city_obj.province_id.id,
				'region':city_obj.region.id,
				'zip': city_obj.zip,
				'municipality': city,
				}}
		return res
rubrica_rubrica()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


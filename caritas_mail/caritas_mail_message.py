# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-2011 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

import ast
import base64
import datetime
import dateutil.parser
import email
import logging
import pytz
import re
import time
import addons

import tools
from osv import osv
from osv import fields
from tools.translate import _
from openerp import SUPERUSER_ID


class caritas_mail_message(osv.osv):
	_inherit = 'mail.message'
	_description = 'Email Message'
	_order = 'date desc'

	def schedule_with_attach(self, cr, uid, email_from, email_to, subject, body, model=False, email_cc=None,
							 email_bcc=None, reply_to=False, attachments=None, message_id=False, references=False,
							 res_id=False, subtype='plain', headers=None, mail_server_id=False, auto_delete=False,
							 context=None):
		if context is None:
			context = {}
		if attachments is None:
			attachments = {}
		attachment_obj = self.pool.get('ir.attachment')
		for param in (email_to, email_cc, email_bcc):
			if param and not isinstance(param, list):
				param = [param]
		msg_vals = {
				'subject': subject,
				'date': time.strftime('%Y-%m-%d %H:%M:%S'),
				'user_id': uid,
				'model': model,
				'res_id': res_id,
				'body_text': body if subtype != 'html' else False,
				'body_html': body if subtype == 'html' else False,
				'email_from': email_from,
				'email_to': email_to and ','.join(email_to) or '',
				'email_cc': email_cc and ','.join(email_cc) or '',
				'email_bcc': email_bcc and ','.join(email_bcc) or '',
				'reply_to': reply_to,
				'message_id': message_id,
				'references': references,
				'subtype': subtype,
				'headers': headers, # serialize the dict on the fly
				'mail_server_id': mail_server_id,
				'state': 'outgoing',
				'auto_delete': auto_delete
			}
		email_msg_id = self.create(cr, uid, msg_vals, context)
		attachment_ids = []
		for fname, fcontent in attachments.iteritems():
			exist=attachment_obj.search(cr,uid,[('datas_fname','=',fname)])
			print ''
			print 'mail debug'
			print exist
			print ''
			if context.has_key('default_type'):
				del context['default_type']			
			if exist!=[]:
				print 'esiste il file e lo aggiorno!!'
				attachment_data = {
								   'res_model': self._name,
								   'res_id': email_msg_id,}						
				if model=='lavoro.rubrica':
					print 'oggetto'
					print model,type(model)
					attc=self.pool.get('lavoro.gestattach')
					attid=attc.search(cr,1,[('id','>',0)])
					if attid:
						pid=attc.browse(cr,1,attid[0]).name.id
						attachment_data['parent_id']=pid
				attachment_obj.write(cr,uid,exist,attachment_data,context)
				attachment_ids.append(exist[0])
			else:
				attachment_data = {
						'name': fname,
						'datas_fname': fname,
						'datas': fcontent and fcontent.encode('base64'),
						'res_model': self._name,
						'res_id': email_msg_id,
						}

				attachment_ids.append(attachment_obj.create(cr, uid, attachment_data, context))
		if attachment_ids:
			self.write(cr, uid, email_msg_id, { 'attachment_ids': [(6, 0, attachment_ids)]}, context=context)
		return email_msg_id



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

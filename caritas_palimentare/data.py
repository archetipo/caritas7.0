# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    
#-----------------------------------------------------------------------------------
#   Modulo Base gestione utenti Caritas e servizi sociali
#           utenti famiglia lavoro 
#   Autore: Alessio Gerace
#   data  : Dicembre 2012
#   update : Gennaio 2013
#   
#
#
#
#
##############################################################################
#campi selection


famiglia=[
          ('norm',  'Famiglia di Fatto'),
          ('monop', 'Richiedente nucleo monoparentale'),
          ('monon', 'Richiedente nucleo mononucleare composto da pensionato')]

          
abitaz=[('a1', 'Abitazione in locazione con esclusione edilizia residenziale pubblica'),
		('a2', 'Presa in carico da parte Servizio Socio Sanitari con esclusione di erogazione di benefici economici')]
			

      
#tabella dei punti
dat={		'monop':3.0, #mono parentale
			'monon':3.0,
			'norm':0.0, #mono reddito
            'fm':2.0,  #figlio minorenne
            'Pp':3.0, #persona con patologia
            'a1':3.0, #persona con patologia
            'a2':1.0, #persona con patologia
            }
            
#resituisce ill punteggio in base al valore isee
def calcisee(val):
    retv=0.0
    val=int(val)
    if val<=1250:
        return 8.0
    elif val <=2500:
        return 6.0
    elif val <=3750:
        return 4.0
    elif val <=5000:
        return 2.0
    else:
        return 0.0
        

        

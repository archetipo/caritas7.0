# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------
#   Modulo Gestione  Fondo Case
#   Autore: Alessio Gerace
#   data  : gennaio 2012
#   update : febbraio 2012
#
#
#
#
#
##############################################################################

from openerp.osv import orm,  fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp
import string
import resource
import tools
import addons
import datetime
import calendar
import operator
import math
import data as data

class palim_config(orm.Model):
	_name = "palim.config"
	_description = "Configurazione"
	_columns = {
		'name':fields.many2one('caritas.progetti','Progetto',required=True),
		'anno':fields.integer('Anno',required=True),
		'annodati':fields.integer('I dei criteri sono relativi all\' anno',required=True),
		'maxcandidati':fields.one2many('palim.confvalente','name','Configurazione Numero candidati per Comune',required=False),
		'dtin': fields.datetime("Data inserimento"),
		'opinsft':fields.char('Operatore', size=32),
		}
	_default={
			'anno':lambda *a:int(datetime.date.today().strftime('%Y')),
			'dtin':fields.date.context_today,
			'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
			}
	_sql_constraints = [('name_anno_uniq', 'unique(name,anno)',
	u'Attenzione!! Questo nome esiste già, selezionarlo dalla lista e modificare quello.')]
palim_config()

class palim_confvalente(orm.Model):
	_name = "palim.confvalente"
	_description = "Rubrica Locatore Conduttore punti"
	def _getdiff(self, cr, uid, ids,field_name, args, context=None):
		res = {}
		emrr=self.pool.get('palim.emerrubrica')
		if ids:
			obje=self.browse(cr, uid, ids)
			for ent in obje:
				res[ent.id]={'erogc':0.0,'diffe':0.0}
				idss=emrr.search(cr,uid,[('territorio','=',ent.territorio.id)])
				domande=emrr.browse(cr,uid,idss)
				totdom=0.0
				for domanda in domande:
					if domanda.evaso!='no' and domanda.stato in ['accettata','chiuso']:
						totdom+=float(domanda.evaso)

				res[ent.id]['erogc']=(totdom)
				res[ent.id]['diffe']=(ent.numc - totdom)
		else:
			res={}
		return res
	_columns = {
		'name':fields.many2one('palim.config','Configurazione',size=128),
		'numc':fields.float('Importo Massimo',digits=(14, 2)),
		'erogc':fields.function(_getdiff,method=True,store=False,string='Tot Importo Erogato',digits=(14, 2),type='float',multi='utente'),
		'diffe':fields.function(_getdiff,method=True, store=False,string='Da erogare',type='float' ,digits=(14, 2),multi='utente'),
		'comptenza':fields.many2one('cbase.enti','Ente Riferimento',select=True,required=True),
		'territorio':fields.related('comptenza','territorioprog',type='many2one', relation='res.city', string='Territorio', readonly=True),
		'dtin': fields.datetime("Data inserimento"),
		'opinsft':fields.char('Operatore', size=32),
		}
	_default={
			'dtin':fields.date.context_today,
			'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
			}
palim_confvalente()

class palim_lavori(orm.Model):
	_name = "palim.lavori"
	_description = "Lavori"
	_columns = {
		'opinsft':fields.char('Operatore', size=128,readonly=True),
		'name': fields.char('Condizione Lavorativa', size=128,required=True),
		'relat':fields.many2one('cbase.lavori','Lavoro', size=128),
		'statcod': fields.char('Codice statistico', size=32),
	}
	_defaults = {
			'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
	}

palim_lavori()


class palim_famiglia(orm.Model):
    _inherit = 'cbase.famiglia'
    _name = 'palim.famiglia'
    _description = 'Progetti al quale ha partecipato'

    _columns = {
        'palim_id':fields.many2one('palim.emerrubrica', 'Capo famiglia'),
        'anno':fields.integer('Anno'),
        'annodati':fields.integer('Anno'),
        'lavoro':fields.many2one('palim.lavori','Lavoro', size=128),
        'invalidita':fields.boolean('Invalidità Superiore 66%'),
        'incarico':fields.boolean('A carico'),
        'redditop':fields.boolean('Percettore Reddito'),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorioprog',type='many2one', relation='res.city', string='Territorio Progetti', readonly=True),
    }
    _defaults={
        'anno':lambda *a:int(datetime.date.today().strftime('%Y')),
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
    }


    def add_aggfam(self,cr,uid,id):
        indi_fam=self.pool.get('cbase.famiglia')
        indig=self.pool.get('cbase.famiglia')
        obj=self.browse(cr,uid,id)
        find_parente=indi_fam.search(cr,uid,[('name','=',obj.name.id)])
        find_capfam=indi_fam.search(cr,uid,[('ind_id','=',obj.name.id)])
        #aggiorna la scheda generale famiglari e le singole schede TODO

        if len(find_parente)>0:
            idf=find_parente[0]
        elif len(find_capfam)>0:
            idf=find_capfam[0]
        else:
            vals={'ind_id':obj.ind_id.id,'name':obj.name.id,
                   'eta':obj.eta,'grad':obj.grad.id,'isee':True,
                   'statfam':True,'conv':True,'opinsft':obj.opinsft}
            idf=indi_fam.create(cr,1,vals)


        return True

    def write(self, cr, uid, ids,vals, context=None):
        for fam in ids:
            if 'name' in vals:
                id_trov=self.search(cr,uid,[('palim_id','=',vals['name'])])
                if id_trov and len(id_trov)>0:
                    raise orm.except_orm(_('Attenzione!!'), _('Questo utente è già un capofamiglia'))
                    return False
            obj=self.browse(cr,uid,fam)
            if obj.ind_id!=obj.palim_id.name.id:
                vals['ind_id']=obj.palim_id.name.id
            idr=super(palim_famiglia,self).write(cr, uid,ids, vals, context)
            self.add_aggfam(cr,uid,fam)
        return idr



    def create(self, cr, uid, vals, context=None):
        if vals['nome']==False or vals['cognome']==False or vals['codfis']==False:
            raise orm.except_orm(_('Attenzione!!'), _('Nome Cognome e Codice fiscale sono obbligatori'))
            return False
        p_rubr=self.pool.get('palim.emerrubrica')
        p_indi=self.pool.get('cbase.indigente')
        id_trov=p_rubr.search(cr,uid,[('name','=',vals['name'])])
        if id_trov and len(id_trov)>0:
            raise orm.except_orm(_('Attenzione!!'), _('%s Questo utente è già caricato come intestatario di una scheda.') % vals['nome'])
            return False
        o_rubr=p_rubr.browse(cr,uid,vals['palim_id'])
        indid_trov=p_indi.search(cr,uid,[('codfis','=',o_rubr.name.codfis)])
        if len(indid_trov)>0 :
            o_indi=p_indi.browse(cr,uid,indid_trov[0])
            vals['ind_id']=o_indi.id
        else:
            vals['ind_id']=o_rubr.name.id
        id=super(palim_famiglia,self).create(cr, uid, vals, context)
        self.add_aggfam(cr,uid,id)
        return id

    _sql_constraints = [
            ('name_parente_uniq', 'unique(name)', u'Attenzione!! questo utente è già presente come famigliare'),
        ]


class palim_emerrubrica(orm.Model):
    _name = "palim.emerrubrica"
    _description = "Rubrica Persone povertà alimentare"
    def _getrule(self, cr, uid, ids,field_name, args, context=None):
        ogg_confe=self.pool.get('palim.confvalente')
        res = {}
        if ids:
            objssk=self.browse(cr, uid, ids)
            for ric in objssk:
                user,prj_rule,uprj_id,user_territorio,conf=self.get_user_prj_rule_anno(cr,uid,ric.anno)
                res[ric.id]={'userruolo':prj_rule,'userente':False,'userterritorio':user_territorio,'numc':0.0,'erogc':0.0,'diffe':0.0}
                if user.id==1 or ric.territorio.id == user.id_ente.territorioprog.id:
                    res[ric.id]['userente']=True
                for c in conf.maxcandidati:
                    if c.territorio.id==ric.territorio.id:
                        res[ric.id]['numc']=c.numc
                        res[ric.id]['erogc']=c.erogc
                        res[ric.id]['diffe']=c.diffe
        else:
            res={'userruolo':'user','userente':True}
        return res

    _columns = {
        'numprat':fields.integer('Numero pratica (solo numeri)'),
        'pdate':fields.date("Data pratica"),
        'name':fields.many2one('cbase.indigente', 'Persona'),
        'nome': fields.char('Nome', size=128, required=True),
        'cognome': fields.char('Cognome', size=128, required=True),
        'codfis': fields.char('Codice Fiscale', size=32, help="Codice fiscale",required=True),

        'strada': fields.char('Indirizzo', size=128),
        'civico': fields.char('num Civico', size=12 ),
        'risidente': fields.char('Frazione / Luogo',size=128),
        'cittares':fields.many2one('res.city', string='Residente a:'),
        'comple': fields.date("Giorno di Nascita",required=True),
        'natoestero': fields.boolean('Nato all estero'),
        'nazionalita': fields.many2one('res.country', 'Paese di Nascita'),
        'luogonasc': fields.many2one('res.city', string='Città di nascita'),
        'provn':fields.related('luogonasc','province_id',type='many2one', relation='res.province', string='Provincia'),
        'entext':fields.many2one('cbase.enti','Gia seguito Da', size=128),

        'cstrada': fields.char('Indirizzo Contatto', size=128),
        'cfrazl': fields.char('Frazione / Luogo contatto',size=128),
        'ccivico': fields.char('num Civico contatto', size=12 ),
        'tel': fields.char('Telefono', size=32),
        'cell': fields.char('Cellulare', size=32),
        'cmail': fields.char('Cellulare', size=32,),
        'invalidita':fields.boolean('Richiedente con Invalidità Superiore 66%'),

        'famigliari':fields.one2many('palim.famiglia','palim_id','Famigliari'),

        #~ capofamiglia e nucleo famigliare
        'nucleof':fields.related('name','nucleof',type='many2one', relation='cbase.indigente', string='Nucleo Famigliare'),
        'parentinucleo':fields.related('nucleof','famiglia',type='one2many', relation='cbase.famiglia', string='Nucleo Famigliare'),
        #~ parenti trovati
        'parenti':fields.related('name','famiglia',type='one2many', relation='cbase.famiglia', string='Nucleo Famigliare'),

        'isee':fields.float('ISEE € ',digits=(14, 2),required=True),
        'figlimin':fields.integer('Numero Figli minorenni'),
        'perspat':fields.integer('Numero persone con Patologia-Invalidita sup. 66%',required=True),
        'abitaz':fields.selection(data.abitaz, 'Condizione Alloggiativa'),
        'famtip':fields.selection(data.famiglia, 'Nucleo Famigliare',required=True),
        'punti':fields.float('Punteggio totale',digits=(14, 1),select=True),


        'evaso':fields.selection([('no', 'No'),
                                  ('200', 'Ticket')], 'Contributo erogato ',select=True),

        'candidato':fields.selection([('si','Candidato'),('no','Escluso')], 'Candidato'),
        'caritas':fields.selection([('si','SI'),('no','NO'),('inc','Controllare')], 'In Caritas'),
        'note':fields.text('Note Generali'),
        'opinsft':fields.char('Operatore', size=32,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorioprog',type='many2one', relation='res.city', string='Territorio',select=True),
        'provincia':fields.related('comptenza','province',type='many2one', relation='res.city', string='Provincia', readonly=True),
        'proj_id':fields.many2one('palim.config', 'Progetto emergenza casa al quale si riferisce'),
        'numc':fields.function(_getrule,method=True, store=False,type='float',  string='Totale Disponibilità', readonly=True,multi='utente'),
        'erogc':fields.function(_getrule,method=True, store=False,type='float', string='Totale Già Approvato', readonly=True,multi='utente'),
        'diffe':fields.function(_getrule,method=True, store=False,type='float',  string='Disponibilità Residua', readonly=True,multi='utente'),
        'anno':fields.integer('Anno'),
        'userente':fields.function(_getrule,method=True, store=False,string='attiva',type='boolean', multi='utente'),
        'userruolo':fields.function(_getrule,method=True, store=False,string='ruolo',type='char' , size=32,multi='utente'),
        'carcodfis':fields.char('Carica Tessera Sanitaria', size=200, help="Carica un  utente tramite Tessera Sanitaria"),
        'inizia':fields.boolean('continua caricamento'),
        'stato':fields.selection([('bozza', 'In Attesa di Restituzione'),
                                  ('nuova', 'In caricamento'),
                                  ('valutaz', 'Completato'),    #criterio 5 + valore
                                  ('accettata', 'Assegnato'), #ok propietario
                                  ('escluso', 'Non Erogabile'),
                                  ('chiuso', 'Erogato')], 'Stato',select=True),

        }
    _defaults = {
            'anno':lambda *a:int(datetime.date.today().strftime('%Y')),
            'dtin':fields.date.context_today,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'figlimin':0,
            'numprat':0,
            'famtip':'norm',
            'perspat':0,
            'isee':0.0,
            'punti':0.0,
            'candidato':'no',
            'caritas':'no',
            'evaso':'no',
            'userente':True,
            'userruolo':'user',
            'inizia':False,
            'natoestero':False,
            'stato':'bozza',
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'territorio':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.territorioprog.id,
            }


    def inserisci_dati(self, cr, uid,ids,context=None):
        #~ build("rossi", "mario", datetime.datetime(1991, 1, 25), "M", "G693")
        res={'value':{}}
        datains=datetime.date.today().isoformat()
        idtrov=self.search(cr, uid,[('inizia','=',True)])
        nprat=0
        if len(idtrov)==0:
            nprat=1
        else:
            nprat=len(idtrov)+1
        self.write(cr,uid,ids,{'inizia':True,'stato':'nuova','pdate':datains,'numprat':nprat})
        ids=ids[0]
        obj=self.browse(cr,uid,ids)
        indio=self.pool.get('cbase.indigente')
        emer_fam=self.pool.get('palim.famiglia')
        if obj.nucleof:
            for pn in obj.parentinucleo:
                d={'name':False,'nome':'','cognome':'','codfis':'','grad':False}
                if pn.name.id==obj.name.id:
                    d['name']=obj.nucleof.id
                    d['nome']=obj.nucleof.nome
                    d['cognome']=obj.nucleof.cognome
                    d['codfis']=obj.nucleof.codfis
                    d['grad']=pn.grad.id
                    d['palim_id']=ids
                else:
                    d['name']=pn.name.id
                    d['nome']=pn.nome
                    d['cognome']=pn.cognome
                    d['codfis']=pn.codfis
                    d['grad']=pn.grad.id
                    d['palim_id']=ids
                emer_fam.create(cr,uid,d)
        elif obj.parenti:
            for pn in obj.parenti:
                d={'name':False,'nome':'','cognome':'','codfis':'','grad':False}
                d['name']=pn.name.id
                d['nome']=pn.nome
                d['cognome']=pn.cognome
                d['codfis']=pn.codfis
                d['grad']=pn.grad.id
                d['palim_id']=ids
                emer_fam.create(cr,uid,d)
        return res

    def get_projectid_confid_from_year(self,cr,uid,anno):
        conf_id=self.pool.get('palim.config').search(cr, uid,[('anno','=',anno)])
        prj_id=self.pool.get('palim.config').browse(cr,uid,conf_id[0]).name.id
        return prj_id,conf_id[0]

    def aggiorna_erogaenti(self,cr,uid,progetto,importo,indi,anno):
        prj_indio=self.pool.get('cbase.erogenti')
        idriga=prj_indio.search(cr,uid,[('ind_id','=',indi),('proj_id','=',progetto)])
        if len(idriga)>0:
            val={'imp':importo}
            return prj_indio.write(cr,uid,idriga[0],val)
        else:
            data={'proj_id':progetto,
                  'ind_id':indi,
                  'anno':anno,
                  'imp':importo}
            return prj_indio.create(cr,uid,data)

    def accetta_domanda(self, cr, uid,ids,context=None):
        self.calcola_punti(cr,uid,ids,context)
        obj=self.browse(cr,uid,ids[0])
        prj_id,conf_id=self.get_projectid_confid_from_year(cr,uid,obj.anno)
        if obj.punti<=0:
            raise orm.except_orm(_('Attenzione'), _('Calcolare punteggio prima di procedere con l\'accettazione'))
            return False
        if self.aggiorna_erogaenti(cr,uid,prj_id,obj.evaso,obj.name.id,obj.anno):
            self.write(cr,uid,ids,{'stato':'accettata'})
            return True
        else:
            return False

    def on_change_entext(self,cr,uid,ids,entext,context=None):
        if entext:
            self.write(cr, uid,ids,{'inizia':False})
            self.escludi_domanda(cr,uid,ids,context)

    def escludi_domanda(self, cr, uid,ids,context=None):
        obj=self.browse(cr,uid,ids[0])
        prj_id,conf_id=self.get_projectid_confid_from_year(cr,uid,obj.anno)
        if self.aggiorna_erogaenti(cr,uid,prj_id,0.0,obj.name.id,obj.anno):
            self.write(cr,uid,ids,{'stato':'escluso'})
            return True
        else:
            return False

    def reimposta_domanda(self, cr, uid,ids,context=None):
        self.calcola_punti(cr,uid,ids,context)
        obj=self.browse(cr,uid,ids[0])
        prj_id,conf_id=self.get_projectid_confid_from_year(cr,uid,obj.anno)
        if self.aggiorna_erogaenti(cr,uid,prj_id,0.0,obj.name.id,obj.anno):
            self.write(cr,uid,ids,{'stato':'valutaz'})
            return True
        else:
            return False

    def imposta_erogata(self, cr, uid,ids,context=None):
        prj_indio=self.pool.get('cbase.erogenti')
        id=ids[0]
        if self.browse(cr,uid,id).evaso=='no':
            raise orm.except_orm(_('Attenzione'), _('Selezionare il valore da erogare e poi procedere'))
        else:
            self.write(cr,uid,ids,{'stato':'chiuso'})
        return True

    def get_user_prj_rule_anno(self,cr,uid,anno):
        print 'User Function'
        palim_conf=self.pool.get('palim.config')
        prj_conf=self.pool.get('caritas.progettiutenti')
        idc = palim_conf.search(cr, uid,[('anno','=',anno)])
        conf=palim_conf.browse(cr,uid,idc[0])
        statoproj=conf.name.stato # da inserire gestione controllo stato progetto
        usert = prj_conf.search(cr, uid,[('name','=',conf.name.id),('user_id','=',uid)])
        if statoproj == 'aperto':
            if usert and len(usert)>0:
                prj_rule=prj_conf.browse(cr,uid,usert[0]).prj_rule
                user_territorio=0
                prj_id=conf.name.id
                user = self.pool.get('res.users').browse(cr, uid, uid, context={})
                return user,prj_rule,prj_id,user_territorio,conf
            else:
                raise orm.except_orm(
                    _('Info'),
                    _('Utente non abilitato a questo progetto, contattare amministrazione'))
                return False
        elif statoproj == 'chiuso':
                raise orm.except_orm(
                    _('Attenzione'),
                    _(u'Questo progetto è chiuso per fine o verifica'))
                return False
        elif statoproj == 'nuovo':
                raise orm.except_orm(
                    _('Attenzione'),
                    _(u'Questo progetto non è ancora attivo'))
                return False

    def get_user_prj_rule(self,cr,uid):
        anno=datetime.date.today().strftime('%Y')
        return self.get_user_prj_rule_anno(cr,uid,anno)

    def test_indi(self,cr,uid,codfisc):
        oindi=self.pool.get('cbase.indigente')
        indi_id=oindi.search(cr,uid,[('codfis','=',codfisc)])
        if indi_id and len(indi_id)>0:
            return indi_id[0]
        else:
            return 0

    def test_indi_borse(self,cr,uid,ind_id):
        dtsc=(datetime.date(2013,12,20) - datetime.timedelta(2*365/12)).isoformat()
        oindi=self.pool.get('cbase.borse')
        indi_r=oindi.search(cr,uid,[('ind_id','=',ind_id),('dtini','>=',dtsc) ])
        if indi_r and len(indi_r)>0:
            return True
        else:
            return False

    def on_change_codf(self,cr,uid,ids,nome,cognome,ccf,context=None):
        oindi=self.pool.get('cbase.indigente')
        print nome,cognome,ccf
        indiexist=0
        if nome and cognome and ccf:
            if ccf and len(ccf)==16:
                ccf=ccf.upper()
                indiexist=self.test_indi(cr,uid,ccf)
                func=oindi.pool.get('cbase.func')
                italia=func.get_nazione(cr,uid)
                res=oindi.load_cf(cr,uid,nome,cognome,ccf)
                if res['value']['codfis']==ccf:
                    if indiexist==0:
                        del res['value']['name']
                        if res['value']['nazionalita']==italia:
                            res['value']['natoestero']=False
                        else:
                            res['value']['natoestero']=True
                        return res
                    else:
                        if self.test_indi_borse(cr,1,indiexist):
                            raise orm.except_orm(_('Attenzione'), _('Questo Utente Già presente nel sistema,\n ed ha ricevuto aiuti alimentari negli ultimi 6 mesi'))
                            return False
                        else:
                            res['value']['name']=indiexist
                            notaauto='La famiglia è presente nel sistema Caritas \n Per maggiorni informazioni ricercare il nominativo del conduttore in gestione anagrafiche \n e contattare il centro d\'ascolto di zona.\n '
                            res['value']['note']=notaauto
                            return res
                else:
                    raise orm.except_orm(_('Attenzione'), _('Il codice fiscale non è congruo con i dati inseriti, verificare nome e cognome o il codice'))
                    return False
            else:
                raise orm.except_orm(_('Attenzione'), _('Il codice fiscale non è valido mancano o eccedono caratteri'))
                return False
        else:
            return False

    def on_change_ccf(self,cr,uid,ids,ccf,context=None):
        oindi=self.pool.get('cbase.indigente')
        if ccf:
            t=ccf.split(' ')
            if t.count('')>=1:
                ccf=ccf.upper()
                func=self.pool.get('cbase.func')
                nome,cognome,cf,code= func.decodeCf(ccf)
                indiexist=self.test_indi(cr,uid,cf)
                italia=func.get_nazione(cr,uid)
                res=oindi.load_cf(cr,uid,nome,cognome,cf)
                print 'indi esiste ',indiexist
                if indiexist==0:
                    del res['value']['name']
                    if res['value']['nazionalita']==italia:
                        res['value']['natoestero']=False
                    else:
                        res['value']['natoestero']=True
                    return res
                else:
                    if self.test_indi_borse(cr,1,indiexist):
                        raise orm.except_orm(_('Attenzione'), _('Questo Utente Già presente nel sistema,\n ed ha ricevuto aiuti alimentari negli ultimi 6 mesi'))
                        return False
                    else:
                        res['value']['name']=indiexist
                        notaauto='La famiglia è presente nel sistema Caritas \n Per maggiorni informazioni ricercare il nominativo del conduttore in gestione anagrafiche \n e contattare il centro d\'ascolto di zona.\n '
                        res['value']['note']=notaauto
                        if res['value']['nazionalita']==italia:
                            res['value']['natoestero']=False
                        else:
                            res['value']['natoestero']=True
                        print res
                        return res
            else:
                raise orm.except_orm(_('Attenzione'), _('La Tessera sanitaria non è valida o è scaduta'))
                return False
        else:
            raise orm.except_orm(_('Attenzione'), _('Strisciare la tessera sanitaria nell\'apposito lettore'))
            return False

    def on_change_name(self,cr,uid,ids,name,context=None):
        print name
        res = {'value':{}}
        if name:
            indi=self.pool.get('cbase.indigente').browse(cr,uid,name)
            if indi:
                res['value']={
                            'nome': indi.nome,
                            'cognome':indi.cognome,
                            'codfis': indi.codfis,
                            'strada': indi.strada,
                            'risidente':indi.risidente,
                            'cittares':indi.cittares.id,
                            'civico': indi.civico,
                            'tel':indi.tel,
                            'cell':indi.cell,
                            'nazionalita': indi.nazionalita,
                            'nazionalitar': indi.nazionalitar,
                            'luogonasc': indi.luogonasc,
                            'comple': indi.comple,
                            }

        return res

    def test_city(self,city,territorio):
        print 'test', city,territorio
        if city!=territorio:
            raise orm.except_orm(_('Attenzione'), _('La città di residenza non puà essere quella inserita'))
            return False
        else:
            return True

    def on_change_city(self, cr, uid, ids, city,territorio):
        res = {'value':{}}
        if self.test_city(city,territorio):
            return res
        else:
            return False

    def calcola_punti(self, cr, uid, ids, context=None):
        famo=self.pool.get('palim.famigliari')
        cid=ids[0]
        punti=0.0
        pool=self.browse(cr, uid, cid)
        pini=0.0
        if pool:

            lavfam=[]
            redditf=0
            disab=0
            figlimin=0
            fammoccst=False
            punti=0
            if pool.invalidita:
                disab+=1

            for f in pool.famigliari:
                if f.eta<18:
                    figlimin+=1
                if f.invalidita:
                    disab+=1
            punti+=data.calcisee(pool.isee)
            punti+=data.dat[pool.abitaz]
            punti+=data.dat[pool.famtip] #uno - +percettori di reddito
            punti+=(data.dat['fm'] * figlimin)
            punti+=(data.dat['Pp'] * disab)
            self.write(cr, uid, cid, {'punti':punti,'stato':'valutaz','figlimin':figlimin,'perspat':disab})

        return {'value':{}}



    def get_evaso(self,val):
        if val=='no':
            return 0.0
        if val=='200':
            return 200.0



    def create_indi(self,cr,uid,nome,cognome,ccf,args):
        oindi=self.pool.get('cbase.indigente')
        res=oindi.load_cf(cr,uid,nome,cognome,ccf)
        res['value']['strada']=args['strada']
        res['value']['risidente']=args['risidente']
        res['value']['cittares']=args['cittares']
        res['value']['civico']=args['civico']
        res['value']['tel']=args['tel']
        res['value']['cell']=args['cell']
        indi_id=oindi.create(cr,uid,res['value'])
        indi=oindi.browse(cr,uid,indi_id)
        res={}
        res['nazionalitar']=indi.nazionalita.id
        oindi.write(cr,uid,indi_id,res)
        return indi_id


    def create(self, cr, uid, vals, context=None):
        if vals['name']==0:
            vals['name']=self.create_indi(cr,uid,vals['nome'],vals['cognome'],vals['codfis'],vals)
        elif self.test_indi_borse(cr,1,vals['name']):
            raise orm.except_orm(_('Attenzione'), _('Questo Utente Già presente nel sistema,\n ed ha ricevuto aiuti alimentari negli ultimi 6 mesi'))
            return False
        prj_indio=self.pool.get('cbase.erogenti')
        if 'anno' in vals and vals['anno']:
            anno=vals['anno']
            idc=self.pool.get('palim.config').search(cr, uid,[('anno','=',vals['anno'])])
            if idc and len(idc)>0:
                prj_id=self.pool.get('palim.config').browse(cr,uid,idc[0]).name.id
                ind_id=vals['name']
                evaso=self.get_evaso(vals['evaso'])
                vals['proj_id']=idc[0]
        idr=super(palim_emerrubrica,self).create(cr, uid, vals, context)
        datap={'proj_id':prj_id,
              'ind_id':ind_id,
              'anno':anno,
              'imp':evaso}
        print 'data project ',datap
        prj_indio.create(cr,uid,datap)
        print 'fine salvataggio id %d'	%idr
        return idr


    def write(self, cr, uid, ids,vals, context=None):
        print 'Write rubrica '
        print vals
        if type(ids)==int:
            line_id=ids
        if type(ids)==long:
            line_id=ids
        elif type( ids ) == list:
            line_id=ids[0]
        prj_indi=self.pool.get('cbase.indigente')
        o=self.browse(cr,uid,line_id)

        idr=super(palim_emerrubrica,self).write(cr, uid,ids, vals, context)

        indi=prj_indi.browse(cr,uid,o.name.id)
        if indi:
            res={}
            if o.nome!=indi.nome: res['nome']= o.nome
            if o.cognome!=indi.cognome: res['cognome']=o.cognome
            if o.nome!=indi.nome: res['codfis']= o.codfis
            if o.codfis!=indi.codfis: res['strada']= o.strada
            if o.risidente!=indi.risidente: res['risidente']=o.risidente
            if o.cittares.id!=indi.cittares.id: res['cittares']=o.cittares.id
            if o.civico!=indi.civico: res['civico']= o.civico
            if o.tel!=indi.tel: res['tel']=o.tel
            if o.cell!=indi.cell: res['cell']=o.cell
            if indi.nazionalitar==False: res['nazionalitar']=o.nazionalita.id
            prj_indi.write(cr,uid,o.name.id,res)
        return idr


    _order = 'punti desc, isee asc'
    _rec_name='cognome'
    _sql_constraints = [('palim_codfis_uniq','UNIQUE(codfis)', u'Questo utente esiste già...')]




# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


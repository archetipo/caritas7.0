# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP,  Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation,  either version 3 of the
#    License,  or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------
#   Modulo Gestione Progetti
#   Autore: Alessio Gerace
#   data  : Dicembre 2012
#
#
#
#
#
#
##############################################################################

from openerp.osv import orm, fields
from tools.translate import _
from datetime import datetime,  timedelta,  time,  date
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import decimal_precision as dp
import string
import resource
import tools
import addons
import datetime
import calendar
import operator
import math


class caritas_progetti(orm.Model):
    _name = "caritas.progetti"
    _description = "Base per Gestione Progetti"
    _columns = {
        'name': fields.char('Nome Progetto / Bando',  size=128,
                            select=True,  required=True),
        'dtdal': fields.date("Dal",  required=True),
        'dtal': fields.date("Al",  required=True),
        'conf_user': fields.one2many(
            'caritas.progettiutenti',  'name',
            'Configurazione Utenti',  required=False),
        'stato': fields.selection([('bozza',  'Bozza'),
                                  ('nuovo',  'Nuovo'),
                                  ('aperto',  'Aperto'),
                                  ('chiuso',  'Chiuso')],  'Stato'),
        'dtin': fields.datetime("Data inserimento",  required=True),
        'opinsft': fields.char(
            'Operatore',  size=32,  required=True,  readonly=True),
        'idente': fields.many2one(
            'cbase.enti',  'Inserito Da',  size=128,  required=True),
        'comptenza': fields.many2one(
            'cbase.enti', 'Competenza di ',  size=128, required=True),
        'territorio': fields.related(
            'comptenza', 'territorio', type='many2one',
            relation='res.city',  string='Territorio',  readonly=True),
        }
    _defaults = {
        'name': 'inserisci qui',
        'stato': 'bozza',
        'dtin': fields.date.context_today,
        'opinsft': lambda self,  cr,  uid,  c: self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).name,
        'idente': lambda self,  cr,  uid,  c: self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).id_ente.id,
        'comptenza': lambda self,  cr,  uid,  c: self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).id_ente.id,
        }

    def prj_new(self,  cr,  uid,  ids,  *args):
        self.write(cr,  uid,  ids,  {'stato': 'nuovo'})
        return True

    def prj_open(self,  cr,  uid,  ids,  *args):
        self.write(cr,  uid,  ids,  {'stato': 'aperto'})
        return True

    def prj_chiuso(self,  cr,  uid,  ids,  *args):
        self.write(cr,  uid,  ids,  {'stato': 'chiuso'})
        return True

    def prj_reimposta(self,  cr,  uid,  ids,  *args):
        self.write(cr,  uid,  ids,  {'stato': 'nuovo'})
        return True


class caritas_progettiutenti(orm.Model):
    _name = 'caritas.progettiutenti'
    _description = 'Progetti e Utenti '

    _columns = {
        'name': fields.many2one('caritas.progetti',  'Progetti'),
        'user_id': fields.many2one('res.users',  'Utente', required=True),
        'comptenza': fields.many2one('cbase.enti',  'Competenza'),
        'territorio': fields.many2one('res.city',  'Territorio'),
        'prj_rule': fields.selection(
            [('conf',  'Tutti i privilegi'),
             ('superv',  'Responsabile Supervisore'),
             ('collab',  'Collaboratore Esterno'),
             ('user',  'Utente'),
             ('vis',  'Visualizzatore')],  'Ruolo Progetto',  required=True),

    }
    _defaults = {
        'prj_rule': 'user',
        }

    def on_change_user(self, cr, uid, ids, user, context=None):
        userc = self.pool.get('res.users')
        res = {'value': {}}
        if user:
            c = userc.browse(cr, uid, user)
            res['value']['comptenza'] = c.id_ente.id
            res['value']['territorio'] = c.id_ente.territorioprog.id
        return res




class res_users(orm.Model):
    _inherit = 'res.users'
    _description = 'User'

    _columns = {
        'proj_id': fields.one2many(
            'caritas.progettiutenti', 'user_id', 'Progetti', readonly=True),
    }

res_users()


class cbase_erogenti(orm.Model):
    _inherit = 'cbase.erogenti'
    _description = 'Progetti al quale ha partecipato'

    _columns = {
        'proj_id': fields.many2one(
            'caritas.progetti', 'Progetti', readonly=True),
        'anno': fields.integer('Anno'),
    }
    _defaults = {
        'anno': lambda *a: int(datetime.date.today().strftime('%Y')),
    }


class cbase_indigente(orm.Model):
    _inherit = 'cbase.indigente'
    _description = 'Progetti al quale ha partecipato'

    _columns = {
        'territoriop': fields.related(
            'comptenza', 'territorioprog',
            type='many2one',  relation='res.city',
            string='Territorio Progetti',  readonly=True),
    }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

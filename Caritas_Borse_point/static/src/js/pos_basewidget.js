function openerp_CB_basewidget(instance, module){ //module is instance.point_of_sale

    // This is a base class for all Widgets in the POS. It exposes relevant data to the 
    // templates : 
    // - widget.currency : { symbol: '$' | '€' | ..., position: 'before' | 'after }
    // - widget.format_currency(amount) : this method returns a formatted string based on the
    //   symbol, the position, and the amount of money.
    // if the PoS is not fully loaded when you instanciate the widget, the currency might not
    // yet have been initialized. Use __build_currency_template() to recompute with correct values
    // before rendering.

    module.PosBaseWidget = instance.web.Widget.extend({
        init:function(parent,options){
            this._super(parent);
            options = options || {};
            this.pos = options.pos || (parent ? parent.pos : undefined);
            this.pos_widget = options.pos_widget || (parent ? parent.pos_widget : undefined);
            this.build_currency_template();
        },
        build_currency_template: function(){

            if(this.pos && this.pos.get('currency')){
                this.currency = this.pos.get('currency');
            }else{
                this.currency = {symbol: '$', position: 'after'};
            }

            this.format_currency = function(amount){
                if(this.currency.position === 'after'){
                    return Math.round(amount*100)/100 + ' ' + this.currency.symbol;
                }else{
                    return this.currency.symbol + ' ' + Math.round(amount*100)/100;
                }
            }

        },
        show: function(){
            this.$element.show();
        },
        hide: function(){
            this.$element.hide();
        },
    });

}
function openerp_CB_widgets(instance, module){ //module is instance.point_of_sale
    var QWeb = instance.web.qweb;

    module.NumpadWidget = module.PosBaseWidget.extend({
        template:'NumpadWidget',
        init: function(parent, options) {
            this._super(parent);
            this.state = new module.NumpadState();
        },
        start: function() {
            this.state.bind('change:mode', this.changedMode, this);
            this.changedMode();
            this.$element.find('button#numpad-backspace').click(_.bind(this.clickDeleteLastChar, this));
            this.$element.find('button#numpad-minus').click(_.bind(this.clickSwitchSign, this));
            this.$element.find('button.number-char').click(_.bind(this.clickAppendNewChar, this));
            this.$element.find('button.mode-button').click(_.bind(this.clickChangeMode, this));
        },
        clickDeleteLastChar: function() {
            return this.state.deleteLastChar();
        },
        clickSwitchSign: function() {
            return this.state.switchSign();
        },
        clickAppendNewChar: function(event) {
            var newChar;
            newChar = event.currentTarget.innerText || event.currentTarget.textContent;
            return this.state.appendNewChar(newChar);
        },
        clickChangeMode: function(event) {
            var newMode = event.currentTarget.attributes['data-mode'].nodeValue;
            return this.state.changeMode(newMode);
        },
        changedMode: function() {
            var mode = this.state.get('mode');
            $('.selected-mode').removeClass('selected-mode');
            $(_.str.sprintf('.mode-button[data-mode="%s"]', mode), this.$element).addClass('selected-mode');
        },
    });

    // The PosWidget is the main widget that contains all other widgets in the PointOfSale.
    // It is mainly composed of :
    // - a header, containing the list of orders
    // - a leftpane, containing the list of bought products (orderlines) 
    // - a rightpane, containing the screens (see pos_screens.js)
    // - an actionbar on the bottom, containing various action buttons
    // - popups
    // - an onscreen keyboard
    // a screen_selector which controls the switching between screens and the showing/closing of popups

    module.CBWidget = module.PosBaseWidget.extend({
        template: 'CBWidget',
        init: function() { 
            this._super(arguments[0],{});
            
            this.pos = new module.CBModel(this.session);
            this.pos_widget = this; //So that pos_widget's childs have pos_widget set automatically

            this.numpad_visible = true;
            this.leftpane_visible = true;
            this.leftpane_width   = '440px';
            this.cashier_controls_visible = true;
        },
      
        start: function() {
            var self = this;
            return self.pos.ready.then(function() {
                self.build_currency_template();
                self.renderElement();
                
                self.$('.neworder-button').click(_.bind(self.create_new_order, self));
                
                //when a new order is created, add an order button widget
                self.pos.get('orders').bind('add', function(new_order){
                    var new_order_button = new module.OrderButtonWidget(null, {
                        order: new_order,
                        pos: self.pos
                    });
                    new_order_button.appendTo($('#orders'));
                    new_order_button.selectOrder();
                }, self);

                self.pos.get('orders').add(new module.Order({ pos: self.pos }));

                //self.build_widgets();

                instance.webclient.set_content_full_screen(true);

                if (!self.pos.get('pos_session')) {
                    self.screen_selector.show_popup('error', 'Sorry, we could not create a user session');
                }else if(!self.pos.get('pos_config')){
                    self.screen_selector.show_popup('error', 'Sorry, we could not find any PoS Configuration for this session');
                }
            
                self.$('.loader').animate({opacity:0},3000,'swing',function(){$('.loader').hide();});
                self.$('.loader img').hide();

            });/*,function(){   // error when loading models data from the backend
                self.$('.loader img').hide();
                return new instance.web.Model("ir.model.data").get_func("search_read")([['name', '=', 'action_pos_session_opening']], ['res_id'])
                    .pipe( _.bind(function(res){
                        return instance.connection.rpc('/web/action/load', {'action_id': res[0]['res_id']})
                            .pipe(_.bind(function(result){
                                var action = result.result;
                                this.do_action(action);
                            }, this));
                    }, self));
            });*/
        },


        //FIXME this method is probably not at the right place ... 
        //~ scan_product: function(parsed_ean){
            //~ var selectedOrder = this.pos.get('selectedOrder');
            //~ var scannedProductModel = this.get_product_by_ean(parsed_ean);
            //~ if (!scannedProductModel){
                //~ return false;
            //~ } else {
                //~ if(parsed_ean.type === 'price'){
                    //~ selectedOrder.addProduct(new module.Product(scannedProductModel), { price:parsed_ean.value});
                //~ }else if(parsed_ean.type === 'weight'){
                    //~ selectedOrder.addProduct(new module.Product(scannedProductModel), { quantity:parsed_ean.value, merge:false});
                //~ }else{
                    //~ selectedOrder.addProduct(new module.Product(scannedProductModel));
                //~ }
                //~ return true;
            //~ }
        //~ },

        //~ get_product_by_ean: function(parsed_ean) {
            //~ var allProducts = this.pos.get('product_list');
            //~ var allPackages = this.pos.get('product.packaging');
            //~ var scannedProductModel = undefined;
//~ 
            //~ if (parsed_ean.type === 'price' || parsed_ean.type === 'weight') {
                //~ var itemCode = parsed_ean.id;
                //~ var scannedPackaging = _.detect(allPackages, function(pack) { 
                    //~ return pack.ean && pack.ean.substring(0,7) === itemCode;
                //~ });
                //~ if (scannedPackaging) {
                    //~ scannedProductModel = _.detect(allProducts, function(pc) { return pc.id === scannedPackaging.product_id[0];});
                //~ }else{
                    //~ scannedProductModel = _.detect(allProducts, function(pc) { return pc.ean13  && (pc.ean13.substring(0,7) === parsed_ean.id);});   
                //~ }
            //~ } else if(parsed_ean.type === 'unit'){
                //~ scannedProductModel = _.detect(allProducts, function(pc) { return pc.ean13 === parsed_ean.ean;});   //TODO DOES NOT SCALE
            //~ }
            //~ return scannedProductModel;
        //~ },
        //~ // creates a new order, and add it to the list of orders.
        //~ create_new_order: function() {
            //~ var new_order;
            //~ new_order = new module.Order({ pos: this.pos });
            //~ this.pos.get('orders').add(new_order);
            //~ this.pos.set({ selectedOrder: new_order });
        //~ },
        //~ changed_pending_operations: function () {
            //~ var self = this;
            //~ this.synch_notification.on_change_nbr_pending(self.pos.get('nbr_pending_operations').length);
        //~ },
        // shows or hide the numpad and related controls like the paypad.
        set_numpad_visible: function(visible){
            if(visible !== this.numpad_visible){
                this.numpad_visible = visible;
                if(visible){
                    this.numpad.show();
                    this.paypad.show();
                    this.order_widget.set_compact(true);
                }else{
                    this.numpad.hide();
                    this.paypad.hide();
                    this.order_widget.set_compact(false);
                }
            }
        },
        //shows or hide the leftpane (contains the list of orderlines, the numpad, the paypad, etc.)
        set_leftpane_visible: function(visible){
            if(visible !== this.leftpane_visible){
                this.leftpane_visible = visible;
                if(visible){
                    $('#leftpane').show().animate({'width':this.leftpane_width},500,'swing');
                    $('#rightpane').animate({'left':this.leftpane_width},500,'swing');
                }else{
                    var leftpane = $('#leftpane');
                    $('#leftpane').animate({'width':'0px'},500,'swing', function(){ leftpane.hide(); });
                    $('#rightpane').animate({'left':'0px'},500,'swing');
                }
            }
        },
        //shows or hide the controls in the PosWidget that are specific to the cashier ( Orders, close button, etc. ) 
        //~ set_cashier_controls_visible: function(visible){
            //~ if(visible !== this.cashier_controls_visible){
                //~ this.cashier_controls_visible = visible;
                //~ if(visible){
                    //~ $('#loggedas').show();
                    //~ $('#rightheader').show();
                //~ }else{
                    //~ $('#loggedas').hide();
                    //~ $('#rightheader').hide();
                //~ }
            //~ }
        //~ },
        try_close: function() {
            var self = this;
            self.pos.flush().then(function() {
                self.close();
            });
        },
        close: function() {
            this.pos.barcode_reader.disconnect();
            return 1;/*new instance.web.Model("ir.model.data").get_func("search_read")([['name', '=', 'action_client_pos_menu']], ['res_id']).pipe(
                    _.bind(function(res) {
                return this.rpc('/web/action/load', {'action_id': res[0]['res_id']}).pipe(_.bind(function(result) {
                    var action = result.result;
                    action.context = _.extend(action.context || {}, {'cancel_action': {type: 'ir.actions.client', tag: 'reload'}});
                    this.do_action(action);
                }, this));
            }, this));*/
        },
        destroy: function() {
            instance.webclient.set_content_full_screen(false);
            self.pos = undefined;
            this._super();
        }
    });
}

# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#~ ,('stato','!=','scad')
from osv import osv,fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource 
import tools
import addons
import datetime
import calendar
import operator
import math

GIORNI=[('1', 'Lunedì'),
        ('2', 'Martedì'),
        ('3', 'Mercoledì'),
        ('4', 'Giovedì'),
        ('5', 'Venerdì')]

MESI=[('1', 'Gennaio'),
        ('2', 'Febbraio'),
        ('3', 'Marzo'),
        ('4', 'Aprile'),
        ('5', 'Maggio'),
        ('6', 'Giugno'),
        ('7', 'Luglio'),
        ('8', 'Agosto'),
        ('9', 'Settembre'),
        ('10', 'Ottobre'),
        ('11', 'Novembre'),
        ('12', 'Dicembre')]


class cborse_func(osv.osv):
    _name = "cborse.func"
    def add_months(self,sourcedate,months):
		month = sourcedate.month - 1 + months
		year = sourcedate.year + month / 12
		month = month % 12 + 1
		day = min(sourcedate.day,calendar.monthrange(year,month)[1])
		return datetime.date(year,month,day),month
		 
    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))  
        
    def gen_data_mese(self,data,mese):
		dt=data.split(" ")
		dt=dt[0].split("-")
		a,m,g=dt[:3]
		data1=date(int(a),int(m),int(g))
		continua=True
		meser=0
		while continua:
			data1,meser=self.add_months(data1,1)
			print meser
			if meser==int(mese):
				continua=False
		return data1
     
    def pianif_ora(self,list_id,d,t):
        idd=list_id
        if idd.__len__()==0:
            cmb=datetime.datetime.combine(d,t)
        else:
            cmb=datetime.datetime.combine(d,t)
            t=timedelta(minutes=idd.__len__()*7)
            cmb=cmb+t  
        return cmb


    def set_stat(self,stato,data):
        stat={'new':'pian','pian':'post'}
        if stato=='new':
            stato='pian' 
        else:
          stato=self.ctl_scad(data)
        return stato
          
    def data_cmp(self,dat1,data2):
        val=(data2-data1).days
        if val>0:
           return False 
        if val<=0:
           return True
           
    def ctl_scad(self,data):
        dt=self.gen_data(data)
        oggi=datetime.date.today()
        val=(oggi-dt).days
        if val>= 1:
           return 'scad'
        if val==0:
           return 'pian'
        if val<=-1:
           return 'post'
       
    def get_autodtprimacons(self,data,d,t,delta):
        cmb=datetime.datetime.combine(d,t)
        print "delta data prima:" 
        ctrl=datetime.datetime.combine(self.gen_data(data),t)
        deltadt=(cmb-ctrl)
        if deltadt.days <= delta:
			td=timedelta(days=delta)
			cmb=cmb+td
        return cmb
        
    def ctl_scadc(self,data,scad):
        dt=self.gen_data(data)
        td=in_the_future(dt,scad)
        oggi=datetime.date.today()
        val=(oggi-dt).days
        if val>=1:
           return True
        return False

    def in_the_future(self,data,months=1): 
        year, month, day = data.timetuple()[:3] 
        new_month = month + months 
        return datetime.date(year + (new_month / 12), new_month % 12, day) 

        
    def tra_n_giorni(self,data,giorni):
        td=timedelta(days=giorni)
        return data+td

    def anno_corr(self):
		ret=int(datetime.date.today().strftime('%Y'))
		return ret

    def mese_corr(self): 
        return int(datetime.date.today().strftime('%m'))
    
    def mese_data(self,data): 
        return int(data.strftime('%m'))

    def oltre15(self,data):
        year, month, day = data.timetuple()[:3] 
        if day>=15:
            return True
        return False

    def lista_date(self,cr,uid,numero,mesi,datapc):
        v={1:30,2:15}
        res=[]
        annoc=self.anno_corr()
        tid=self.pool.get('cborse.conf').search(cr, uid, [('anno','=',annoc)])
        conf=self.pool.get('cborse.conf').browse(cr, uid, tid[0]) 
        dateno=self.dict_datano(conf.date)
        mesic=self.genmese(conf.mesi)      
        print 'mesi'
        print mesic
        volte=v.get(numero)
        continua=True 
        borse=numero*mesi
        dataf=datapc
        datai=datapc 
        res.append(datai)
        if len(res)==(borse):
		    continua=False
        while continua:
			print dataf
			dataf=self.tra_n_giorni(dataf, volte)
			m=self.mese_data(dataf)
			print  mesic.__contains__(m)
			if mesic.__contains__(m):
				res.append(dataf)
			if res.__len__()==borse:
					continua=False
        return res

    def dict_datano(self,dateno):
        res=[]
        v={'ddata':datetime.date.today(),'adata':datetime.date.today()}
        for r in dateno:
            v['ddata']=self.gen_data(r['ddata'])
            v['adata']=self.gen_data(r['adata'])
            res.append(v.copy())
        return res 

    def inrange(self,dateno,data):
        r1=False
        r2=False
        for r in dateno:
             r1=False
             r2=False
             if data >= r['ddata']:
                   r1=True
             if  data <= r['adata']:
                   r2=True
             if r1 and r2:
                  return True
        return False

    def genmese(self,brmese):
        r=[]
        for i in range(0,brmese.__len__()):
            a=int(brmese[i].mese)
            r.append(a)
        return r
            
    def gengiorni(self,brgiorni):
        r=[]
        for i in range(0,brgiorni.__len__()):
            a=int(brgiorni[i].giorno)-1
            r.append(a)
        return r
    #math.fabs((a[0]['data']-a[1]['data']).days)
    def raggruppa_per_cons(self,consdict):
        return consdict.sort(key=operator.itemgetter('ncons'))

    def calc_datadelta(self,dataprev,listdate):
        ret=[]
        conta=0
        for cons in listdate:
            v=math.fabs((dataprev-cons['data']).days)
            if v<=3:
              rig={'id':conta,'delta':v,'ncons':cons['ncons']}
              ret.append(rig.copy())
            conta+=1        
        return ret
        
    def Lista_date_cons_annoc(self,cr,uid,durata):
        r=[]
        annocorrente=self.anno_corr()
        tid=self.pool.get('cborse.conf').search(cr, uid, [('anno','>=',annocorrente)])
        for idat in tid:
			conf=self.pool.get('cborse.conf').browse(cr, uid, idat)
			annoc=conf.anno
			dateno=self.dict_datano(conf.date)
			giorni=self.gengiorni(conf.giorni)
			mesi=self.genmese(conf.mesi)
			if annoc==annocorrente:
				da=self.mese_corr()
				a=self.mese_corr()+durata+4
			else:
				da=1
				a=1+durata+4
			for m in range(da,a):
				if mesi.__contains__(m):
					c=calendar.monthcalendar(annoc,m)
					for x in c:
					  conta=0
					  for y in x:
						conta+=1
						if y<>0:
							if giorni.__contains__(calendar.weekday(annoc,m,y)):
							  dt=date(annoc,m,y)
							  if not self.inrange(dateno, dt) : 
								  if dt>datetime.date.today():
									  n={'data':dt,'ncons':0,'cons':True}  
									  r.append(n.copy())
                        
        return r
        
    def data_in_lista(self,data,listadate):
        ret=False
        conta=0
        for g in listadate:
            v=math.fabs((data-g['data']).days)
            if v==0:ret=True
        return ret
cborse_func()   
 
class cborse_cgiorni(osv.osv):
    _name = "cborse.cgiorni"
    _description = "conf Lista giorni consegna"
    _columns = {
        'confb_id': fields.integer('id configurazione'),
        'giorno':fields.selection(GIORNI, 'Giorno'),
    }
cborse_cgiorni()
    
class cborse_cmesi(osv.osv):
    _name = "cborse.cmesi"
    _description = "conf Lista mesi consegna"
    _columns = {
        'confb_id': fields.integer('id configurazione'),
        'mese':fields.selection(MESI,'Mese'),
    }
cborse_cmesi()
    
class cborse_cdate(osv.osv):
    _name = "cborse.cdate"
    _description = "conf Lista date escluse"
    _columns = {
        'confb_id': fields.integer('id configurazione'),
        'ddata': fields.date("dal"),
        'adata': fields.date("al"),
    }
cborse_cdate()   
    
class cborse_conf(osv.osv):
    def _ut_ente(self, cr, uid, ids,name, args, context=None):
        pass
    _name = "cborse.conf"
    _description = "configurazione Pianificazione consegne"
    _columns = {
        'anno':fields.integer('Configurazione valida Per l anno',required=True,select=True),
        'nbmax':fields.integer('Numero Max di Borse consegnate al giorno'),
        'nbext':fields.integer('Numero Max di Borse posticipate'),
        'arai':fields.integer('Orario Inizio consegna'),
        'araf':fields.integer('Orario Fine consegna'),
        'tdcons':fields.float('durata cons',  digits=(14, 2),readonly=True),
        'qcons':fields.float('intervallo di tempo minimo di ogni consegna',  digits=(14, 2),readonly=True),
        'giorni':fields.one2many('cborse.cgiorni','confb_id','Giorni Consegna' ,required=True),        
        'mesi':fields.one2many('cborse.cmesi','confb_id','Mesi Consegna',required=True),
        'date':fields.one2many('cborse.cdate','confb_id','Date e Festività',required=True),
        'opinsft':fields.char('Operatore', size=32),
        'Opinsf':fields.function(_ut_ente, method=True, type="char", string='Operatore',readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    def calcola_dati(self, cr, uid,ids,context=None):
        cid=ids[0]
        res=0.0
        dat=self.browse(cr, uid, cid)
        res=round((float(dat.araf)-float(dat.arai))/(float(dat.nbmax)+float(dat.nbext)),2)
        self.write(cr, uid, cid, {'qcons': res})

    _defaults = {
            'nbmax':30,
            'nbext':5,
            'dtin':fields.date.context_today,
            'anno':datetime.date.today().strftime('%Y'),
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
    }
cborse_conf()


class cborse_calend(osv.osv):
    _name = "cborse.calend"
    _description = "Calendario Borse "
    _columns = {
        'pnl_id':fields.integer('id auto pianif'),
        'ind_id':fields.many2one('cbase.indigente', 'Indigente',select=True,required=True), 
        'dtcons': fields.datetime("Data consegna calc"),
        'dtconsv':fields.date("Data consegna prevista",required=True),
        'nome': fields.char('Nome', size=128),
        'delegato':fields.char('delegato', size=128),
        'indig_code': fields.char('Codice', size=32,select=True),
        'scad': fields.date("scadenza"),
        'ora' : fields.float('Ora Pianificata', help='ora stimata di consegna',readonly=True),
        'stato':fields.selection([('new', 'Nuova'),
                                 ('pian', 'Pianificata'),
                                 ('cons', 'Consegnata'),
                                 ('scad', 'Scaduta'),
                                 ('post', 'Posticipata')], 'Stato'),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'lung_day':fields.integer('Lunghezza Giorno'),
        'opinsft':fields.char('Operatore', size=32),
    }
    
    def cambio_id(self,cr,uid,ids,context=None):
        cid=ids[0]
        br=self.browse(cr, uid, cid)
        if br.ind_id.active:
            self.write(cr, uid, cid, {'nome': br.ind_id.name, 'indig_code': br.ind_id.indig_id})
            
    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))          

    def change_data(self,cr,uid,ids,data,context=None):
        func=self.pool.get('cborse.func')
        annoc=func.anno_corr()
        tid=self.pool.get('cborse.conf').search(cr, uid, [('anno','=',annoc)])
        conf=self.pool.get('cborse.conf').browse(cr, uid, tid[0])   
        d=func.gen_data(data)       
        idret=self.search(cr,uid,[('dtconsv','=',d.strftime('%Y-%m-%d'))])
        maxc=conf.nbmax
        lst=func.Lista_date_cons_annoc(cr,uid,2)
        if func.data_in_lista(d,lst):
            if idret.__len__()>=20:
                str='In questa data sono pianificate %d Consegne' % idret.__len__()
                raise osv.except_osv(_('Info'), _(str)) 
        else:
          str='Questa non è una data di consegna'
          raise osv.except_osv(_('Attenzione'), _(str))  
        
        
    def read_conf(self, cr, uid, ids,context=None):
        anno=self.pool.get('cborse.func').anno_corr()
        tid=self.pool.get('cborse.conf').search(cr, uid, [('anno','=',anno)])
        func=self.pool.get('cborse.conf').browse(cr, uid, tid[0])
        return func        
       
    def Calc_tempi(self, cr, uid, ids,context=None):
        cid=ids[0]
        func=self.pool.get('cborse.func')
        annoc=func.anno_corr()
        br=self.browse(cr, uid, cid)
        cfg=self.read_conf(cr, uid, ids, context)
        data=br.dtconsv
        d=func.gen_data(data)
        stato=br.stato
        idd=self.search(cr, uid,[('dtconsv','=',data)])
        t=datetime.time(cfg.arai)
        d=func.gen_data(data)         
        cmb=func.pianif_ora(idd,d,t)
        stato=func.set_stat(stato,data)
        self.write(cr, uid, cid, {'dtcons':cmb.strftime('%d-%m-%Y %H:%M:%S'),'stato':stato,'scad':data})
                 
    def genera_dati(self,cr,uid,ids,context=None):
        self.cambio_id(cr, uid, ids, None)
        self.Calc_tempi(cr, uid, ids, None)
        
    
        
    _defaults = {
                 'dtin':fields.date.context_today,
                 'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
                 'ora':0.5,
                 'lung_day':4,
                 'stato':'new',
                 }
    _order = 'dtconsv asc'
cborse_calend()

class cborse_auto(osv.osv):
    
    _name = "cborse.auto"
    _description = "pianificazione Automatica Consegna Borsa"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente', 'Nome',required=True),
        'ind_code':fields.char('Codice', size=32, select=True),
        'pianif':fields.one2many('cborse.calend','pnl_id','Lista date Pianificate',required=True),
        'dtpc':fields.date("Data prima consegna",required=True),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'numero':fields.integer('numero'),
        'mesi':fields.integer('mesi'),
        'id_borsa':fields.integer('borsa'),
        'stat':fields.integer('stato'),
        'opinsft':fields.char('Operatore', size=32),
        }
        
    _defaults = {
            'dtini':fields.date.context_today,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'stat':0
    }
    def chk_indc(self,cr,uid,ids,ind_code,context=None):
        res = {'value':{}}
        if ind_code:
            indic=self.pool.get('cbase.indigente').search(cr, uid, [('indig_id','=',ind_code.upper())])
            if indic.__len__()>0:
                func=self.pool.get('cborse.func')
                indib=self.pool.get('cbase.indigente').browse(cr,uid,indic[0]).borse
                for b in indib:
                    if not(b.st_scad | b.st_proc):
						t=datetime.time(0)
						if b.primacons:
							d=func.gen_data_mese(b.dtini,b.primacons)
						else:
							d=func.gen_data(b.dtini)
						dpc= func.get_autodtprimacons(b.dtini,d,t,15)
						num=b.qt
						dur=b.scad
						res = {'value': {'ind_id' : indic[0],'dtpc' : dpc.strftime('%Y-%m-%d'),'numero':int(num),'mesi':int(dur),'id_borsa':b.id,}}
        return res
        
    def chk_indi(self,cr,uid,ids,indi_id,context=None):
        res = {'value':{}}
        if indi_id:
            func=self.pool.get('cborse.func')
            indib=self.pool.get('cbase.indigente').browse(cr,uid,indi_id).borse
            for b in indib:
                if not(b.st_scad | b.st_proc):
                        t=datetime.time(0)
                        d=func.gen_data(b.dtini)  
                        dpc= func.get_autodtprimacons(b.dtini,d,t,15)
                        num=b.qt
                        dur=b.scad
                        res ={'value': {'dtpc' : dpc.strftime('%Y-%m-%d'),'numero':int(num),'mesi':int(dur),'id_borsa':b.id}}
        return res
        
    def genera_dati(self,cr,uid,ids,context=None):
		p=self.browse(cr, uid, ids[0])
		stat=self.pool.get('cbase.borse').browse(cr,uid,p.id_borsa)
		if not stat.st_proc:
			func=self.pool.get('cborse.func')			
			datai=func.gen_data(p.dtpc)
			lst=[]
			br=self.pool.get('cborse.calend')
			annoc=func.anno_corr()
			tid=self.pool.get('cborse.conf').search(cr, uid, [('anno','=',annoc)])
			conf=self.pool.get('cborse.conf').browse(cr, uid, tid[0])
			lnc=[]
			brcons=stat=self.pool.get('cbase.borse')
			maxc=conf.nbmax
			lista=func.lista_date(cr,uid,p.numero,p.mesi,datai)
			lst=func.Lista_date_cons_annoc(cr,uid,p.mesi)
			##############################################################################
			#In lista ho le date calcolate
			#in lst ho le date reali di consegna da oggi a oggi + i mesi 
			#Devo fare la ricerca delle date di lista in lst e caricare in lst il num di consegne del giorno
			#controllo la data provo -1 e +1 guardo quale ha meno consegne se le consegne sono uguali fa il giorno -1
			#di una data di consegna controllo sempre la consegna successiva se ha molte meno consegne tipo 5 in meno la elaboro la cons succ
			#dovremmo quasi esserci
			##############################################################################
			final=[]
			idr=[]
			print 'lista'
			print lista
			print 'lst'
			print lst
			for d in lista:
				continua=True
				cnt=0
				r=func.calc_datadelta(d,lst)
				r.sort(key=operator.itemgetter('delta'))
				print 'r'
				print r
				while continua:
					print lst[r[cnt]['id']]['data'].strftime('%Y-%m-%d')
					idret=br.search(cr,uid,[('dtconsv','=',lst[r[cnt]['id']]['data'].strftime('%Y-%m-%d'))])
					if idret==[]:
						continua=False
					else:
						if idret.__len__()>maxc:
							cnt+=1
						else:
							continua=False
				f=lst[r[cnt]['id']]['data'] #in d aggiorno la data a quella più vicina per ora non tengo ancora 
				#conto del numero id ocnsegne appena ho più dati aggiungo la procedura
				dt=f.strftime('%d-%m-%Y')
				idr=[]
				s=br.create(cr,uid,{'ind_id':p.ind_id.id,'dtconsv':dt,'pnl_id':ids[0],})
				idr.append(s)
				br.genera_dati(cr,uid,idr,None)
				br.read(cr,uid,idr[0])
				self.write(cr,uid,ids[0],{'stat':1})
			self.pool.get('cbase.borse').write(cr,uid,p.id_borsa,{'st_proc':True})
		return True
cborse_auto()

class cbase_famiglia(osv.osv):
    _inherit = 'cbase.famiglia'
    _description = 'famiglia'

    _columns = {
        'id_cons':fields.integer('consegna'),
    }
cbase_famiglia()

class cborse_borsa(osv.osv):
    def _ut_ente(self, cr, uid, ids,name, args, context=None):
        pass
    _name = "cborse.borsa"
    _description = "Consegna Borsa"
    _columns = {
        'cod_tessera':fields.char('Codice', size=32, select=True, states={'bozza':[('readonly',False)]}),
        'ind_id':fields.many2one('cbase.indigente', 'Persona',required=True,select=True),
        'deleg':fields.char('Delegato', size=32),
        'doc_del':fields.char('Documento Delegato', size=32),
        'cod_doc':fields.char('Codice Borsa', size=32,required=True),
        'fam_id':fields.one2many('cbase.famiglia','id_cons','Famiglia'),
        #'numpar':fields.integer('Nucleo Famigliare'),
        #'figli':fields.integer('Figli'),
        #'neon':fields.integer('di cui neonati'),
        #'coniug':fields.boolean('Coniuge'),
        'riga_b':fields.one2many('maga.scarico','id_borsa','Lista',required=True),
        'stat':fields.boolean('Stato'),
        'state': fields.selection([
            ('bozza','Si'),
            ('compl','No'),
            ],'Modificabile', readonly=True),
        'id_calend':fields.integer('id calendario',states={'bozza':[('readonly',False)]}),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,states={'bozza':[('readonly',False)]}),

        }
        
    _defaults = {
            'stat':False,
            'state':'bozza',
            'dtini':fields.date.context_today,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
                }
    
    def codice_borsa(self,cr,uid,ids,cod_tessera,context=None):
        res={'value':{}}    
        print cod_tessera
        ststo=0
        res=""
        func=self.pool.get('cborse.func')
        borse=self.pool.get('cborse.borsa').search(cr, uid, [('cod_doc','<>',False)])
        numborse=borse.__len__()
       # borsa=self.browse(cr, uid, cid)
        brid=self.pool.get('cborse.calend').search(cr, uid, [('indig_code','=',cod_tessera.upper())])
        br=self.pool.get('cborse.calend').browse(cr, uid,brid)
        if br.__len__()>0:
            oggi=datetime.date.today()
            idcal=0
            stato=0
            idp=0
            for b in br:
                dt=func.gen_data(b.dtconsv)
                if ((dt-oggi).days==0) & (b.stato=='pian' or b.stato=='post'):
                   stato=1
                   idcal=b.id 
                   idp=b.ind_id.id
            if stato==1:
                res="B%d" % (numborse) 
                #Devo Salvare il record in cui lavoro per avere l'id e poi ficcarlo nella tabella!! è l'unica!!
                #indi=self.browse(cr, uid, ids[0])
                res= {'value':{'cod_doc':res,
                                 'id_calend':idcal,
                                 'ind_id':idp,
                                 'cod_tessera':cod_tessera}}
            else:
                delta=50
                c=0
                i=0
                data=""
                for b in br:
                    if b.stato=='pian' or b.stato=='post':
                        min=(func.gen_data(b.dtconsv)-oggi).days
                        if min<delta :
                             delta=min
                             data=b.dtconsv
                        c+=1
                str='Per oggi non sono previste consegne,la prossima consegna sarà %s' % data
                raise osv.except_osv(_('Attenzione'), _(str))
        else:
          str='codice utente errato' 
          raise osv.except_osv(_('Attenzione'), _(str))  
        return res
    
    def init_cons(self,cr,uid,ids,context=None):
        res={'value':{}} 
        idc=ids[0]
        b=self.browse(cr,uid,idc)
        idp=b.ind_id.id
        idf=self.pool.get('cbase.famiglia').search(cr,uid,[('ind_id','=',idp)])
        for iidf in idf:
			if self.pool.get('cbase.famiglia').browse(cr,uid,iidf).pborsa:
				a=self.pool.get('cbase.famiglia').write(cr,uid,iidf,{'id_cons':idc})
		#delegato
        deleg=self.pool.get('cbase.indigente').browse(cr,uid,idp).deleg
        doc_del=self.pool.get('cbase.indigente').browse(cr,uid,idp).doc_del
        self.write(cr,uid,idc,{'stat':True,'deleg':deleg,'doc_del':doc_del})
        return True
    
    def valida(self,cr,uid,ids,context=None):
        cid=ids[0]
        borsa=self.browse(cr, uid, cid)
        idlinee=self.pool.get('maga.scarico').search(cr, uid, [('id_borsa','=',cid)])
        linee=self.pool.get('maga.scarico').browse(cr, uid, idlinee)
        for linea in linee:
            if not linea.stat:
                self.pool.get('maga.scarico').write(cr,uid,linea.id,{'stat':1})
                idd=[]
                idd.append(linea.id)
                self.pool.get('maga.scarico').Calc_tot(cr,uid,idd)
        self.pool.get('cborse.calend').write(cr, uid,borsa.id_calend,{'stato':'cons'})
        self.write(cr,uid,cid,{'state':'compl','stat':False})
        return True
#    def annulla(self,cr,uid,ids,context=None):
  
    _order = 'dtini desc'
cborse_borsa() 


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


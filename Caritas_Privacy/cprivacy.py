# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv,fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource 
import tools
import addons
import datetime
import calendar
import operator
import math
import cpassw as p

class cprivacy_listapw(osv.osv):
	_name = "cprivacy.listapw"
	_description = "Stoto civile della persona"
	_columns = {
		'user_id':fields.integer('Operatore'),
		'password': fields.char('Password', size=64,required=True),
		'data': fields.datetime("Data nuova password",readonly=True,required=True)
	}
	_defaults = {
		'data':fields.date.context_today,
		'user_id':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id,
		 }
cprivacy_listapw()


class res_users(osv.osv):
    _inherit = 'res.users'
    _description = 'User'
    
    _columns = {
    	'pw_cons':fields.char('Password Consigliata', size=64),
    	'listpw':fields.one2many('cprivacy.listapw','user_id','Lista Password'),
    }
    def passcons(self, cr, uid,ids,context=None):
    	pw=p.r_password()
    	res={'value':{'pw_cons':pw,}}
    	#print res
    	self.write(cr, uid, ids[0], {'pw_cons':pw})
    	return True
    def genpassammi(self,cr,uid,context={}):
        print 'sched pass'
        pwok=False
        pws=[]
        pws.append(self.browse(cr, uid, uid).password)
        print pws
        testlist=self.pool.get('cprivacy.listapw').search(cr, uid,[('user_id','=',0)])
        if len(testlist) > 0:
            for pw in self.pool.get('cprivacy.listapw').browse(cr, uid, uid):
                if pw:
                    pws.append(pw.password) 
        while (pwok==False):
        	pw=p.r_password()
        	pwok=True
        	for pwt in pws:
        		if pwt==pw:pwok=False						
        self.pool.get('cprivacy.listapw').create(cr,uid,{'password':pw})
		
res_users()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


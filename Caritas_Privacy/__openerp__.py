# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Caritas gestione della Privacy',
    'version': '0.1',
    'category': 'Modulo di Gestione Privacy',
    'description': """
     Questo modulo Gestice la consegna della borsa spesa alle persone bisognose""",
    'author': 'Caritas Interparrochiale Bra',
    'website': 'http://www.caritasbra.it',
    'depends': [],
    'init_xml': [],
    'update_xml': [
		'cprivacy_view.xml',
		'cprivacy_scheduler.xml',
       # 'security/ir.model.access.csv', 
    ],
    'installable': True,
    'active': False
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

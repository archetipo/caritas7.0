# -*- coding: utf-8 -*-
#!/usr/bin/env python

from random import *
import string
 
chars = string.ascii_letters + string.digits
 
def r_password(min=8,max=8):
    """ Genera una password casuale alfanumerica 
        di lunghezza variabile da  min a max
    """
    return "".join(choice(chars) for x in range(randint(min, max)))

#print random_password()

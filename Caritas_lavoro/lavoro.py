# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------------
#   Modulo Gestione  Fondo Case
#   Autore: Alessio Gerace
#   data  : gennaio 2012
#   update : febbraio 2012
#   
#
#
#
#
##############################################################################

from osv import osv,fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp
import string
import resource 
import tools
import addons
import datetime
import calendar
import operator
import math


class lavoro_gestattach(osv.osv):
    _name = "lavoro.gestattach"
    _description = "destinazione dei curriculum"
    _columns = {
		'name': fields.many2one('document.directory', 'Directory di destinazione allegati')
		}

lavoro_gestattach()

class lavoro_categ(osv.osv):
    _name = "lavoro.categ"
    _description = "Categorie di Lavori"
    _columns = {
		'name':fields.char('Categoria', size=128),
		}

lavoro_categ()

class lavoro_rubrica(osv.osv):
	_name = "lavoro.rubrica"
	_description = "Rubrica Aziende"
	_columns = {
		'name':fields.char('Nome Azienda', size=64,select=True,required=True),
		'categ':fields.many2one('lavoro.categ','Categoria', size=128),
		'ind':fields.char('indirizzo', size=128),
		'zip': fields.char('CAP', size=24),
		'municipality': fields.many2one('res.city', 'Citta'),
		'province': fields.many2one('res.province', string='Provincia'),
		'region': fields.many2one('res.region', string='Regione'),
		'tel':fields.char('Telefono', size=32),
		'mail':fields.char('Mail', size=200),
		'note':fields.text('Note Centro Ascolto'),
		'opinsft':fields.char('Operatore', size=32),
		'dtin': fields.datetime("Data inserimento",readonly=True),
		
			}
	_defaults = {
			'dtin':fields.date.context_today,
			'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
			}

	def on_change_municipality(self, cr, uid, ids, city):
		res = {'value':{}}
		if(city):
			city_obj = self.pool.get('res.city').browse(cr, uid, city)
			res = {'value': {
				'province':city_obj.province_id.id,
				'region':city_obj.region.id,
				'zip': city_obj.zip,
				'municipality': city,
				}}
		return res
		
lavoro_rubrica()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


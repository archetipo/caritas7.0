# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv,fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource 
import tools
import addons
import datetime
import calendar
import operator
import math

GIORNI=[('1', 'Lunedì'),
        ('2', 'Martedì'),
        ('3', 'Mercoledì'),
        ('4', 'Giovedì'),
        ('5', 'Venerdì'),
        ('6', 'Sabato')]

MESI=[('1', 'Gennaio'),
        ('2', 'Febbraio'),
        ('3', 'Marzo'),
        ('4', 'Aprile'),
        ('5', 'Maggio'),
        ('6', 'Giugno'),
        ('7', 'Luglio'),
        ('8', 'Agosto'),
        ('9', 'Settembre'),
        ('10', 'Ottobre'),
        ('11', 'Novembre'),
        ('12', 'Dicembre')]


class farmaci_func(osv.osv):
    _name = "farmaci.func"
    def add_months(self,sourcedate,months):
		month = sourcedate.month - 1 + months
		year = sourcedate.year + month / 12
		month = month % 12 + 1
		day = min(sourcedate.day,calendar.monthrange(year,month)[1])
		return datetime.date(year,month,day),month
		 
    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))  
        
    def gen_data_mese(self,data,mese):
		dt=data.split(" ")
		dt=dt[0].split("-")
		a,m,g=dt[:3]
		data=date(int(a),int(m),int(g))
		data1=date(int(a),int(m),int(g))
		continua=True
		print 'mese data gen_data',int(mese)
		meser=0
		while continua:
			data1,meser=self.add_months(data1,1)
			print meser,data1,mese
			if (data1>data) and (self.mese_del(data1)==int(mese)):
				continua=False
		return data1
     
    def pianif_ora(self,list_id,d,t):
        idd=list_id
        if idd.__len__()==0:
            cmb=datetime.datetime.combine(d,t)
        else:
            cmb=datetime.datetime.combine(d,t)
            t=timedelta(minutes=idd.__len__()*7)
            cmb=cmb+t  
        return cmb


    def set_stat(self,stato,data):
        stat={'new':'pian','pian':'post'}
        if stato=='new':
            stato='pian' 
        else:
          stato=self.ctl_scad(data)
        return stato
          
    def data_cmp(self,dat1,data2):
        val=(data2-data1).days
        if val>0:
           return False 
        if val<=0:
           return True
           
    def ctl_scad(self,data):
        dt=self.gen_data(data)
        oggi=datetime.date.today()
        val=(oggi-dt).days
        if val>= 1:
           return 'scad'
        if val==0:
           return 'pian'
        if val<=-1:
           return 'post'
       
    def get_autodtprimacons(self,data,d,t,delta):
		cmb=datetime.datetime.combine(d,t)
		print "delta data prima:" 
		ctrl=datetime.datetime.combine(self.gen_data(data),t)
		deltadt=(cmb-ctrl)
		print deltadt.days
		if deltadt.days <= delta:
			td=timedelta(days=delta)
			cmb=cmb+td
			print 'if delta data %s' % cmb
		print 'norm delta data %s' % cmb
		return cmb
        
    def ctl_scadc(self,data,scad):
        dt=self.gen_data(data)
        td=in_the_future(dt,scad)
        oggi=datetime.date.today()
        val=(oggi-dt).days
        if val>=1:
           return True
        return False

    def in_the_future(self,data,months=1): 
        year, month, day = data.timetuple()[:3] 
        new_month = month + months 
        return datetime.date(year + (new_month / 12), new_month % 12, day) 

        
    def tra_n_giorni(self,data,giorni):
        td=timedelta(days=giorni)
        return data+td

    def anno_corr(self):
		ret=int(datetime.date.today().strftime('%Y'))
		return ret

    def mese_corr(self): 
        return int(datetime.date.today().strftime('%m'))
        
    def mese_del(self,data): 
        return int(data.strftime('%m'))
    
    def mese_data(self,data): 
        return int(data.strftime('%m'))

    def oltre15(self,data):
        year, month, day = data.timetuple()[:3] 
        if day>=15:
            return True
        return False

    def lista_date(self,cr,uid,numero,mesi,datapc):
        v={1:30,2:15}
        res=[]
        annoc=self.anno_corr()
        user = self.pool.get('res.users').browse(cr, uid, uid)
        tid=self.pool.get('farmaci.conf').search(cr, uid, [('comptenza','=',user.id_ente.id),('anno','=',annoc)])
        conf=self.pool.get('farmaci.conf').browse(cr, uid, tid[0]) 
        dateno=self.dict_datano(conf.date)
        mesic=self.genmese(conf.mesi)      
        print 'mesi'
        print mesic
        volte=v.get(numero)
        continua=True 
        Farmaci=numero*mesi
        dataf=datapc
        datai=datapc 
        res.append(datai)
        if len(res)==(Farmaci):
		    continua=False
        while continua:
			print dataf
			dataf=self.tra_n_giorni(dataf, volte)
			m=self.mese_data(dataf)
			print  mesic.__contains__(m)
			if mesic.__contains__(m):
				res.append(dataf)
			if res.__len__()==Farmaci:
					continua=False
        return res

    def dict_datano(self,dateno):
        res=[]
        v={'ddata':datetime.date.today(),'adata':datetime.date.today()}
        for r in dateno:
            v['ddata']=self.gen_data(r['ddata'])
            v['adata']=self.gen_data(r['adata'])
            res.append(v.copy())
        return res 

    def inrange(self,dateno,data):
        r1=False
        r2=False
        for r in dateno:
             r1=False
             r2=False
             if data >= r['ddata']:
                   r1=True
             if  data <= r['adata']:
                   r2=True
             if r1 and r2:
                  return True
        return False

    def genmese(self,brmese):
        r=[]
        for i in range(0,brmese.__len__()):
            a=int(brmese[i].mese)
            r.append(a)
        return r
            
    def gengiorni(self,brgiorni):
        r=[]
        for i in range(0,brgiorni.__len__()):
            a=int(brgiorni[i].giorno)-1
            r.append(a)
        return r
    #math.fabs((a[0]['data']-a[1]['data']).days)
    def raggruppa_per_cons(self,consdict):
        return consdict.sort(key=operator.itemgetter('ncons'))

    def calc_datadelta(self,dataprev,listdate):
        ret=[]
        conta=0
        minimo=0
        elenco=[]
        for cons in listdate:
            elenco.append(math.fabs((dataprev-cons['data']).days))
            minimo=min(elenco)
        for cons in listdate:
            v=math.fabs((dataprev-cons['data']).days)
            if v<=minimo+3:
              rig={'id':conta,'delta':v,'ncons':cons['ncons']}
              ret.append(rig.copy())
            conta+=1        
        return ret
        
    def Lista_date_cons_annoc(self,cr,uid,durata):
        r=[]
        user = self.pool.get('res.users').browse(cr, uid, uid)
        borsa_calend=self.pool.get('farmaci.calend')
        annocorrente=self.anno_corr()
        tid=self.pool.get('farmaci.conf').search(cr, uid, [('comptenza','=',user.id_ente.id),('anno','>=',annocorrente)])
        for idat in tid:
			conf=self.pool.get('farmaci.conf').browse(cr, uid, idat)
			annoc=conf.anno
			dateno=self.dict_datano(conf.date)
			giorni=self.gengiorni(conf.giorni)
			mesi=self.genmese(conf.mesi)
			if annoc==annocorrente:
				da=self.mese_corr()
				a=self.mese_corr()+durata+4
			else:
				da=1
				a=1+durata+4
			for m in range(da,a):
				if mesi.__contains__(m):
					c=calendar.monthcalendar(annoc,m)
					for x in c:
					  conta=0
					  for y in x:
						conta+=1
						if y<>0:
							if giorni.__contains__(calendar.weekday(annoc,m,y)):
							  dt=date(annoc,m,y)
							  if not self.inrange(dateno, dt) : 
								  if dt>datetime.date.today():
									  retid=borsa_calend.search(cr,uid,[('comptenza','=',user.id_ente.id),('dtconsv','=',dt.strftime('%Y-%m-%d'))])
									  n={'data':dt,'ncons':len(retid),'cons':True}  
									  r.append(n.copy())
                        
        return r
        
    def data_in_lista(self,data,listadate):
        ret=False
        conta=0
        for g in listadate:
            v=math.fabs((data-g['data']).days)
            if v==0:ret=True
        return ret
farmaci_func()   
 
class farmaci_cgiorni(osv.osv):
    _name = "farmaci.cgiorni"
    _description = "conf Lista giorni consegna"
    _columns = {
        'confb_id':fields.many2one('farmaci.conf','id configurazione'),
        'giorno':fields.selection(GIORNI, 'Giorno'),
    }
farmaci_cgiorni()
    
class farmaci_cmesi(osv.osv):
    _name = "farmaci.cmesi"
    _description = "conf Lista mesi consegna"
    _columns = {
        'confb_id':fields.many2one('farmaci.conf','id configurazione'),
        'mese':fields.selection(MESI,'Mese'),
    }
farmaci_cmesi()
    
class farmaci_cdate(osv.osv):
    _name = "farmaci.cdate"
    _description = "conf Lista date escluse"
    _columns = {
        'confb_id':fields.many2one('farmaci.conf','id configurazione'),
        'ddata': fields.date("dal"),
        'adata': fields.date("al"),
    }
farmaci_cdate()   
    
class farmaci_conf(osv.osv):
	_name = "farmaci.conf"
	_description = "configurazione Pianificazione Buoni"
	_columns = {
		'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
		'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
		'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
		'anno':fields.integer('Configurazione valida Per l anno',required=True,select=True),
		'tipo':fields.selection([('buoni', 'Solo Buoni per Farmacie'),
								 ('interna', 'Interna con prenotazione e gestione ')], 'Modalitàdi distribuzione'),
		'nbmax':fields.integer('Numero Max di Farmaci consegnati al giorno'),
		'nbext':fields.integer('Numero Max di Farmaci posticipate'),
		'arai':fields.integer('Orario Inizio consegna'),
		'araf':fields.integer('Orario Fine consegna'),
		'tdcons':fields.float('durata cons',  digits=(14, 2),readonly=True),
		'qcons':fields.float('intervallo di tempo minimo di ogni consegna',  digits=(14, 2),readonly=True),
		'giorni':fields.one2many('farmaci.cgiorni','confb_id','Giorni Consegna' ),        
		'mesi':fields.one2many('farmaci.cmesi','confb_id','Mesi Consegna'),
		'date':fields.one2many('farmaci.cdate','confb_id','Date e Festività'),
		'opinsft':fields.char('Operatore', size=32),
		'dtin': fields.datetime("Data inserimento",readonly=True),
	}
	_defaults = {
		'nbmax':30,
		'nbext':5,
		'dtin':fields.date.context_today,
		'anno':int(datetime.date.today().strftime('%Y')),
		'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
		'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,			
		'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,	
	}

	def calcola_dati(self, cr, uid,ids,context=None):
		cid=ids[0]
		res=0.0
		dat=self.browse(cr, uid, cid)
		res=round((float(dat.araf)-float(dat.arai))/(float(dat.nbmax)+float(dat.nbext)),2)
		self.write(cr, uid, cid, {'qcons': res})

	def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
		user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
		new_args = []
		#~ print 'ovverride Search'
		#~ print args
		for arg in args:
			if (type(arg) is not tuple):
				if (type(arg) is not list):
					new_args += arg
					continue
			#~ print arg[2]
			if arg[2] == 'USER_COMPETENZA':
				new_args += [(arg[0], arg[1], user.id_ente.id)]
			elif arg[2] == 'USER_PAYMENT_MODE_IDS':
				new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
			else:
				new_args += [arg]
		#~ print new_args
		return super(farmaci_conf, self).search(cr, uid, new_args)
	def read_group(self, cr, uid, domain,fields,groupby, offset=0, limit=None,order=None,context=None):	
		#~ print 'ovverride Group Articoli'
		user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
		new_domain =[]
		for arg in domain:
			if (type(arg) is not tuple):
				if (type(arg) is not list):
					new_args += arg
					continue
			#~ print arg[2]
			if arg[2] == 'USER_COMPETENZA':
				new_domain += [(arg[0], arg[1], user.id_ente.id)]
			elif arg[2] == 'USER_PAYMENT_MODE_IDS':
				new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
			else:
				new_domain += [arg]
		#~ print 'domain gruppo articoli %s' % new_domain
		return super(farmaci_conf, self).read_group(cr, uid, new_domain, fields , groupby, limit, context)   		
	_rec_name = 'anno'	
farmaci_conf()

class farmaci_buoni(osv.osv):
	_name = "farmaci.buoni"
	_description = "Gestione Buono Farmaci "
	_columns = {
		'name':fields.char('bo', size=128),
		'ind_id':fields.many2one('cbase.indigente', 'Indigente',select=True,required=True), 
		'ind_ric':fields.many2one('cbase.richieste', 'Richiesta',select=True,required=True), 
		'code': fields.char('Buono Num', size=64,select=True),
		'dtcons': fields.date("Data Buono",required=True),
		'datar':fields.date("Data Restituzione Buono"),
		'scontrino': fields.char('Num', size=128),
		'indig_code': fields.char('Codice', size=32,select=True),
		'nota':fields.text('Aggiungi un Nota - Farmaci Acquistati'),
		'opinsft':fields.char('Operatore', size=32),
		'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
		'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
		'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True), 
		'dtin': fields.datetime("Data inserimento",readonly=True),
		}
	def genera_codice(self,cr,uid,idrichiesta,context):
		res = ''
		if idrichiesta:
			func=self.pool.get('farmaci.func')	
			annoc=func.anno_corr()
			user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
			config=self.pool.get('farmaci.conf').search(cr, uid,[('comptenza','=',user.id_ente.id),('anno','=',annoc)])
			idric=self.pool.get('cbase.definiz').search(cr,uid,[('tipo','=','farmaci')])			
			objric=self.pool.get('cbase.richieste')
			richiesta=objric.browse(cr,uid,idrichiesta)
			print 'La richiesta è = ' ,richiesta.defin.name
			mindate=date(int(annoc),1, 1).strftime('%Y-%m-%d') 
			maxdate=date(int(annoc), 12, 31).strftime('%Y-%m-%d')
			num=len(objric.search(cr,uid,[('comptenza','=',user.id_ente.id),('ind_id','=',richiesta.ind_id.id),
										   ('dtda','>',mindate),('dtda','<',maxdate)]))
			code='%s%d_%s_%d' % (richiesta.defin.name[0].upper(),richiesta.ind_id.id,str(annoc)[2:],num)
			linec={'ind_id':richiesta.ind_id.id,
				   'ind_ric':idrichiesta,
				   'code':code,
				   'dtcons':richiesta.dtda,
				   'indig_code':richiesta.ind_id.indig_id,
				   }
			self.create(cr,uid,linec)
			res=code
		return res
		
	def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
		user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
		new_args = []
		print 'ovverride Search Farmaci'
		print args
		print self._rec_name
		for arg in args:
			if (type(arg) is not tuple):
				if (type(arg) is not list):
					new_args += arg
					continue
			#~ print arg[2]
			if arg[2] == 'USER_COMPETENZA':
				new_args += [(arg[0], arg[1], user.id_ente.id)]
			else:
				new_args += [arg]
		print new_args
		a=super(farmaci_buoni, self).search(cr, uid, new_args)  
		print 'torna ',a
		return a
		  
	def read_group(self, cr, uid, domain,fields,groupby, offset=0, limit=None,order=None,context=None):	
		print 'ovverride Group Articoli'
		user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
		new_domain =[]
		for arg in domain:
			if (type(arg) is not tuple):
				if (type(arg) is not list):
					new_args += arg
					continue
			#~ print arg[2]
			if arg[2] == 'USER_COMPETENZA':
				new_domain += [(arg[0], arg[1], user.id_ente.id)]
			else:
				new_domain += [arg]

		return super(farmaci_buoni,self).read_group(cr, uid, new_domain, fields , groupby, limit, context)         
	_defaults = {
				'dtin':fields.date.context_today,
				'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
				'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,			
				'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,	
				}
	_order = 'dtcons asc'
	_rec_name = 'ind_ric'
farmaci_buoni()








# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


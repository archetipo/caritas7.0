# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------------
#   Modulo Punto Consegna
#   Autore: Alessio Gerace
#   data  : Febbraio 2013
#   update : Giugno 2011
#   
#
#
#
#
##############################################################################
from osv import fields, osv
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource 
import tools
import addons
import datetime
import calendar
import operator
import math
from tools.translate import _

class pb_pianif(osv.osv):
	_inherit = 'cborse.calend'
	_description = 'eredita calendario borse'

	def aggiornaborse(self,cr,uid,context=None):
		return True

class pb_borsa(osv.osv):
	_inherit = 'cborse.borsa'
	_description = 'eredita calendario borse'

	def prepara_borsa(self,cr,uid,ids,brid,context=None):
		user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
		res={'value':{}}    
		print cod_tessera
		ststo=0
		res=""
		func=self.pool.get('cborse.func')
		borse=self.pool.get('cborse.borsa').search(cr, uid, [('comptenza','=',user.id_ente.id),('cod_doc','<>',False)])
		numborse=borse.__len__()
		br=self.pool.get('cborse.calend').browse(cr, uid,brid)
		if br.__len__()>0:
			oggi=datetime.date.today()
			idcal=0
			stato=0
			idp=0
			for b in br:
				dt=func.gen_data(b.dtconsv)
				if ((dt-oggi).days==0) & (b.stato=='pian' or b.stato=='post'):
				   stato=1
				   idcal=b.id 
				   idp=b.ind_id.id
			if stato==1:
				res="B%d" % (numborse) 
				#Devo Salvare il record in cui lavoro per avere l'id e poi ficcarlo nella tabella!! è l'unica!!
				#indi=self.browse(cr, uid, ids[0])
				res= {'value':{'cod_doc':res,
								 'id_calend':idcal,
								 'ind_id':idp,
								 'cod_tessera':cod_tessera}}
			else:
				delta=50
				c=0
				i=0
				data=""
				for b in br:
					if b.stato=='pian' or b.stato=='post':
						min=(func.gen_data(b.dtconsv)-oggi).days
						if min<delta :
							 delta=min
							 data=b.dtconsv
						c+=1
				str='Per oggi non sono previste consegne,la prossima consegna sarà %s' % data
				raise osv.except_osv(_('Attenzione'), _(str))
		else:
		  str='codice utente errato' 
		  raise osv.except_osv(_('Attenzione'), _(str))  
		print res
		return res
Array.prototype.groupBy=function(property){
    "use strict";
    function deepCopy(p) {
        var c = {};
        for (var i in p) {
              if (typeof p[i] === 'object') {
                c[i] = (p[i].constructor === Array)?[]:{};
                deepCopy(p[i],c[i]);
              } 
        else {
                c[i] = p[i];
              }
        }
        return c;
    }
    var retarr=[];
    var len=this.length;
    for(var i=0;i<len;i++){
        var groupedlen=retarr.length,found=false;
        for(var j=0;j<groupedlen;j++){
            if(this[i][property]===retarr[j].key){
                retarr[j].values.push(deepCopy(this[i]));
                found=true;
                break;
            }
        }
        if (found === false) {
            retarr.push({
                key: this[i][property],
                values: []
            });
            retarr[retarr.length-1].values.push(deepCopy(this[i]));
        }
    }
    return retarr;    
};

function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1, property.length - 1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

Array.prototype.sortBy = function(property) {
    return this.sort(dynamicSort(property));
}

// var a = "10:15"
// var b = toDate(a,"h:m")
// alert(b);
function toTime(dStr,format) {
    var now = new Date();
    if (format == "h:m") {
        var h=dStr.split(":");
        now.setHours(parseInt(h[0]));
        now.setMinutes(parseInt(h[1]));
        now.setSeconds(0);
        return now;
    }else 
        return "Invalid Format";
}
# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------
#   Modulo Gestione  contabilità onlus
#   Autore: Alessio Gerace
#   data  : marzo 2014
#
#
#
#
#
#
##############################################################################

from openerp.osv import orm,  fields
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp
import string
import addons
import calendar
import operator
import math



class contab_movelines(orm.Model):
    _name = "contab.movelines"
    _description = "linee conti"
    def _calc_anno(self, cr, uid, ids,name, args, context=None):
        res = {}
        for mov in self.browse(cr, uid, ids):
              res[mov.id] = self.get_fiscaly(cr,uid,mov.data,context)
        return res

    _columns = {
        'reg_mov':fields.many2one(
            'contab.registerline', 'Movimento', select=True),
        'reg_balance':fields.many2one(
            'contab.initbalance', 'Saldo', select=True),
        'account_id':fields.many2one('contab.account', 'Conto', select=True),
        'name': fields.char('Descrizione',size=128,required=True),
        'data': fields.date("Data"),
        'anno': fields.function(
            _calc_anno,method=True,type='integer',
            string='Anno',store=True),
        'dare':fields.float('Dare € ',digits=(14, 2)),
        'avere':fields.float('Avere € ',digits=(14, 2)),
        'comptenza':fields.many2one(
            'cbase.enti','Competenza di ', size=128,select=True),
        'closed':fields.boolean('chiuso'),
        'territorio':fields.related(
        'comptenza','territorio',type='many2one',
            relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one(
            'cbase.enti','Inserito Da', size=128,select=True,readonly=True),

        }
    _defaults={
            'data': fields.date.context_today,
            'closed':False,
            'idente':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def get_fiscaly(self,cr,uid,data,context=None):
        objfy=self.pool.get('contab.fiscalyear')
        req_year=int(datetime.strptime(data, DEFAULT_SERVER_DATE_FORMAT).strftime('%Y'))
        ids=objfy.search(cr,uid,[('name','=',req_year),('stato','=','conf')])
        if ids:
            return int(req_year)
        else:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Attivare un anno fiscale prima di procedere'))
            return False
    _order = 'data desc'


class contab_balance(orm.Model):
    _name = "contab.balance"
    _description = "saldo conto"
    def _calc_anno(self, cr, uid, ids,name, args, context=None):
        res = {}
        for mov in self.browse(cr, uid, ids):
              res[mov.id] = self.get_fiscaly(cr,uid,mov.data,context)
        return res
    _columns = {
        'reg_balance':fields.many2one(
            'contab.initbalance', 'Saldo', select=True),
        'account_id':fields.many2one('contab.account', 'Conto', select=True),
        'name': fields.char('Descrizione',size=128,required=True),
        'anno': fields.function(
            _calc_anno,method=True,type='integer',
            string='Anno', store=True),
        'data': fields.date("Data"),
        'dt_from': fields.date("Data Inizio"),
        'dt_to': fields.date("Data Fine"),
        'dare':fields.float('Dare € ',digits=(14, 2)),
        'avere':fields.float('Avere € ',digits=(14, 2)),
        'comptenza':fields.many2one(
            'cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related(
        'comptenza','territorio',type='many2one',
            relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one(
            'cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }

    _defaults={
            'data': fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def get_fiscaly(self,cr,uid,data,context=None):
        objfy=self.pool.get('contab.fiscalyear')
        req_year=int(datetime.strptime(data, DEFAULT_SERVER_DATE_FORMAT).strftime('%Y'))
        ids=objfy.search(cr,uid,[('name','=',req_year),('stato','=','conf')])
        if ids:
            return int(req_year)
        else:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Attivare un anno fiscale prima di procedere'))
            return False

    _order = 'data desc'

class contab_account(orm.Model):
    _name = "contab.account"
    _description = "Conto"

    def _get_tot(self,  cr,  uid,  ids, field_name,  args,  context=None):
        res =  {
                'tot_dare': 0.0,
                'tot_avere': 0.0,
                'tot_diff': 0.0,
                }
        user=self.pool.get('res.users').browse(cr,uid,uid,context=context)
        curry=int(date.today().strftime('%Y'))
        ente=user.id_ente.id
        if ids:
            obj_conti = self.browse(cr,  uid,  ids)
            for ric in obj_conti:
                if ric.idente.id == ente:
                    res[ric.id] = {
                        'tot_dare': 0.0,
                        'tot_avere': 0.0,
                        'tot_diff': 0.0,
                        }
                    res[ric.id]['tot_dare']=ric.tot_mov_dare
                    res[ric.id]['tot_avere']=ric.tot_mov_avere
                    for c in ric.child_ids:
                        res[ric.id]['tot_dare']+= c.tot_mov_dare
                        res[ric.id]['tot_avere']+= c.tot_mov_avere
                    res[ric.id]['tot_diff']=res[ric.id]['tot_dare']-res[ric.id]['tot_avere']
        return res


    def name_get(self, cr, uid, ids, context=None):
        res = []
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for id in ids:
            elmt = self.browse(cr, uid, id, context=context)
            res.append((id, self._get_one_full_name(elmt)))
        return res

    def _get_full_name(self, cr, uid, ids, name=None, args=None, context=None):
        if context == None:
            context = {}
        res = {}
        for elmt in self.browse(cr, uid, ids, context=context):
            res[elmt.id] = self._get_one_full_name(elmt)
        return res

    def _get_one_full_name(self, elmt, level=6):
        if level<=0:
            return '...'
        if elmt.parent_id :
            parent_path = self._get_one_full_name(elmt.parent_id, level-1) + " / "
        else:
            parent_path = ''
        return parent_path + elmt.name

    def _child_compute(self, cr, uid, ids, name, arg, context=None):
        result = {}
        if context is None:
            context = {}

        for account in self.browse(cr, uid, ids, context=context):
            result[account.id] = map(lambda x: x.id, [child for child in account.child_ids])

        return result

    #~ def _complete_name_calc(self, cr, uid, ids, prop, unknow_none, unknow_dict):
        #~ res = self.name_get(cr, uid, ids)
        #~ return dict(res)


    _columns = {
            'name':fields.char('Nome',size=256,required=True),
            'code':fields.char('Codice',size=128),
            'parent_id': fields.many2one(
                'contab.account', 'Conto Padre', select=2),
            'child_ids': fields.one2many(
                'contab.account', 'parent_id', 'Lista Sotto Conti'),
            'child_complete_ids': fields.function(
                _child_compute, relation='contab.account',
                string="Gerarchia Conti", type='many2many'),
            'dt_from': fields.date("Data Inizio"),
            'dt_to': fields.date("Data Fine"),
            'banca':fields.boolean('Banca'),
            'cassa':fields.boolean('Cassa'),
            'patr':fields.boolean('patrimoniale'),
            'movimenti':fields.one2many(
                'contab.movelines','account_id','Lista Movimenti'),
            'saldi':fields.one2many(
                'contab.balance','account_id','Lista Saldi'),
            'tot_mov_dare': fields.float(
                'Importo movimenti Dare', digits=(14, 2),
                readonly=True),
            'tot_mov_avere':fields.float(
                'Importo movimenti Avere', digits=(14, 2),
                readonly=True),
            'differenza': fields.float(
                'Differenza Movimenti Dare/Avere', digits=(14, 2),
                readonly=True),
            'tot_dare':fields.float(
                'Totale  Dare', digits=(14, 2),
                readonly=True),
            'tot_avere':fields.float(
                'Totale  Avere', digits=(14, 2),
                readonly=True),
            'tot_diff':fields.float(
                'Totale  Differenza', digits=(14, 2),
                readonly=True),
            'comptenza':fields.many2one(
                'cbase.enti','Competenza di ', size=128,select=True),
            'territorio':fields.related(
            'comptenza','territorio',type='many2one',
                relation='res.city', string='Territorio', readonly=True),
            'idente':fields.many2one(
                'cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }

    _defaults={
            'tot_mov_dare': 0.0,
            'tot_mov_avere': 0.0,
            'differenza': 0.0,
            'idente':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def calc_mov(self,cr,uid,ids,context=None):
        tot_mov_dare=0.0
        tot_mov_avere=0.0
        tot_dare=0.0
        tot_avere=0.0
        tot_diff=0.0
        for id in ids:
            mov=self.browse(cr, uid, id)
            for mov_lines in mov.movimenti:
                if mov_lines.closed is False:
                    tot_mov_dare+=mov_lines.dare
                    tot_mov_avere+=mov_lines.avere
            tot_avere=tot_mov_avere
            tot_dare=tot_mov_dare
            tot_diff=tot_mov_dare-tot_mov_avere
            self.write(cr, uid, id, {
                'differenza': tot_mov_dare-tot_mov_avere,
                'tot_mov_dare': tot_mov_dare,
                'tot_mov_avere': tot_mov_avere })
        return True

    _order = 'name desc'

class contab_registerline(orm.Model):
    _name = "contab.registerline"
    _description = "registrazione movimento"

    def _calc_anno(self, cr, uid, ids,name, args, context=None):
        res = {}
        for mov in self.browse(cr, uid, ids):
              res[mov.id] = self.get_fiscaly(cr,uid,mov.data,context)
        return res

    _columns = {
        'name':fields.integer('numero movimento'),
        'conto_avere':fields.many2one(
            'contab.account','Dal Conto' , select=True,required=True),
        'conto_dare':fields.many2one(
            'contab.account', 'Al Conto', select=True,required=True),
        'desc': fields.char('Descrizione',size=128,required=True),
        'anno': fields.function(
            _calc_anno,method=True,type='integer',
            string='Anno', store=True),
        'data': fields.date("Data"),
        'valore':fields.float('Importo € ',digits=(14, 2) ,required=True),
        'stato':fields.selection([
            ('new', 'Da convalidare'),
            ('conf', 'Confermato'),
            ('canc', 'Annullato')], 'Stato movimento', required=True),
        'comptenza':fields.many2one(
            'cbase.enti','Competenza di ', size=128,select=True,readonly=True) ,
        'territorio':fields.related(
        'comptenza','territorio',type='many2one',
            relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one(
            'cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }
    _defaults={
            'stato':'new',
            'data':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def get_fiscaly(self,cr,uid,data,context=None):
        objfy=self.pool.get('contab.fiscalyear')
        req_year=int(datetime.strptime(data, DEFAULT_SERVER_DATE_FORMAT).strftime('%Y'))
        ids=objfy.search(cr,uid,[('name','=',req_year),('stato','=','conf')])
        print ids
        if ids:
            return int(req_year)
        else:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Attivare un anno fiscale prima di procedere'))
            return False


    def convalida_movimento(self,  cr,  uid, ids, context=None):
        obj_lineconto=self.pool.get('contab.movelines')
        obj_conto=self.pool.get('contab.account')
        mov=self.browse(cr, uid, ids[0])
        conto_dare=mov.conto_dare
        conto_avere=mov.conto_avere
        valore=mov.valore
        myid=mov.id
        name=mov.desc
        if valore<=0.0:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Inserire un importo valido'))
            return False
        else:
            vals_avere=self.make_newline(
                name,mov.data,0.0,valore,conto_avere.id,myid)
            obj_lineconto.create(cr,uid,vals_avere,context)
            obj_conto.calc_mov(cr,uid,[conto_avere.id])

            vals_dare=self.make_newline(
                name,mov.data,valore,0.0,conto_dare.id,myid)
            obj_lineconto.create(cr,uid,vals_dare,context)
            obj_conto.calc_mov(cr,uid,[conto_dare.id])

            self.write(cr, uid, ids, {'stato': 'conf'})
        return True

    def make_newline(self,name,data,dare,avere,conto,cid):
        return {'name': u'{0}'.format(name),
                    'data':data,
                    'account_id':conto,
                    'dare':dare,
                    'avere':avere,
                    'reg_mov':cid }

    def annulla_movimento(self,  cr,  uid, ids, context=None):
        obj_lineconto=self.pool.get('contab.movelines')
        obj_conto=self.pool.get('contab.account')
        for id in ids:
            r_ids=obj_lineconto.search(cr,uid,[('reg_mov','=',id)])
            unlinked=False
            reg=self.browse(cr, uid, ids[0])
            for mid in r_ids:
                mov=obj_lineconto.browse(cr,uid,mid,context)
                if mov.closed is False:
                    unlinked=obj_lineconto.unlink(cr,uid,mid,context)
            if unlinked:
                conto_dare=reg.conto_dare.id
                conto_avere=reg.conto_avere.id
                obj_conto.calc_mov(cr,uid,[conto_dare])
                obj_conto.calc_mov(cr,uid,[conto_avere])
                self.write(cr, uid, id, {'stato': 'canc'})
        return True

    def create(self,cr, uid, vals, context=None):
        id = super(contab_registerline, self).create(cr,  uid,  vals,  context)
        self.write(cr, uid, id, {'name': id})
        return id

    def unlink(self,cr, uid, ids, context=None):
        raise orm.except_orm(
            _(u'Attenzione'),
            _(u'Questi record non si possono cancellare,annulare il movimento'))
        return False

    _order = 'data desc'
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

class contab_initbalance(orm.Model):
    _name = "contab.initbalance"
    _description = "Gestione Saldi iniziali"
    def _calc_anno(self, cr, uid, ids,name, args, context=None):
        res = {}
        for mov in self.browse(cr, uid, ids):
              res[mov.id] = self.get_fiscaly(cr,uid,mov.data,context)
        return res

    _columns = {
        'fiscal_id':fields.many2one(
            'contab.fiscalyear', 'Anno fiscale', select=True,required=True),
        'name':fields.integer('numero movimento'),
        'anno': fields.function(
            _calc_anno,method=True,type='integer',
            string='Anno', store=True),
        'data': fields.date("Data"),
        'desc': fields.char('Descrizione',size=128,required=True),
        'conto':fields.many2one(
            'contab.account', 'Conto Saldo iniziale', select=True,required=True),
        'valore':fields.float('Importo € ',digits=(14, 2) ,required=True),
        'stato':fields.selection([
            ('new', 'Da convalidare'),
            ('conf', 'Confermato'),
            ('canc', 'Annullato')], 'Stato movimento', required=True),
        'comptenza':fields.many2one(
            'cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related(
        'comptenza','territorio',type='many2one',
            relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one(
            'cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }

    _defaults={
            'stato':'new',
            'data':fields.date.context_today,
            'idente':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def get_fiscaly(self,cr,uid,data,context=None):
        objfy=self.pool.get('contab.fiscalyear')
        req_year=int(datetime.strptime(data, DEFAULT_SERVER_DATE_FORMAT).strftime('%Y'))
        ids=objfy.search(cr,uid,[('name','=',req_year),('stato','=','conf')])
        if ids:
            return int(req_year)
        else:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Attivare un anno fiscale prima di procedere'))
            return False

    def convalida_movimento(self,  cr,  uid, ids, context=None):
        obj_linebalance=self.pool.get('contab.balance')
        obj_lineconto=self.pool.get('contab.movelines')
        obj_conto=self.pool.get('contab.account')
        bal=self.browse(cr, uid, ids[0])
        conto_dare=bal.conto
        valore=bal.valore
        myid=bal.id
        name=bal.desc
        if valore<=0.0:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Inserire un importo valido'))
            return False
        else:

            vals_dare=self.make_newlineconto(
                name,bal.data,valore,0.0,conto_dare.id,myid)
            obj_linebalance.create(cr,uid,vals_dare,context)
            obj_lineconto.create(cr,uid,vals_dare,context)
            obj_conto.calc_mov(cr,uid,[conto_dare.id])

            self.write(cr, uid, ids, {'stato': 'conf'})
        return True

    def make_newlineconto(self,name,data,dare,avere,conto,bid):
        return {'name': u'{0}'.format(name),
                'data':data,
                'account_id':conto,
                'dare':dare,
                'avere':avere,
                'reg_balance':bid }

    def annulla_movimento(self,  cr,  uid, ids, context=None):
        obj_lineconto=self.pool.get('contab.movelines')
        obj_linebalance=self.pool.get('contab.balance')
        obj_conto=self.pool.get('contab.account')
        for id in ids:
            rm_ids=obj_lineconto.search(cr,uid,[('reg_balance','=',id)])
            unlinked=False
            reg=self.browse(cr, uid, id)
            for mid in rm_ids:
                mov=obj_lineconto.browse(cr,uid,mid,context)
                conto=mov.account_id.id
                if mov.closed is False:
                    unlinked=obj_lineconto.unlink(cr,uid,mid,context)
                if unlinked:
                    obj_conto.calc_mov(cr,uid,[conto])
                else:
                    return False
            if unlinked:
                unlinked=False
                rb_ids=obj_linebalance.search(cr,uid,[('reg_balance','=',id)])
                reg=self.browse(cr, uid, id)
                for mid in rb_ids:
                    mov=obj_linebalance.browse(cr,uid,mid,context)
                    unlinked=obj_linebalance.unlink(cr,uid,mid,context)
            else:
                return False
            if unlinked:
                self.write(cr, uid, id, {'stato': 'canc'})
            else:
                return False
        return True


    def unlink(self,cr, uid, ids, context=None):
        raise orm.except_orm(
            _(u'Attenzione'),
            _(u'Questi record non si possono cancellare,annulare il movimento'))
        return False
    _order = 'data desc'

class contab_fiscalyear(orm.Model):
    _name = "contab.fiscalyear"
    _description = "gestione anni fiscali e saldi iniziali"

    def _calc_data(self, cr, uid, ids,name, args, context=None):
        res = {}
        for fiscal_year in self.browse(cr, uid, ids):
            _date_from=date(int(fiscal_year.name),int(1),int(1))
            _date_to=date(int(fiscal_year.name),int(12),int(31))
            date(int(curr_year),int(1),int(1))
            res[fiscal_year.id]['datefrom'] =_date_from.strftime(DEFAULT_SERVER_DATE_FORMAT)
            res[fiscal_year.id]['dateto'] =_date_to.strftime(DEFAULT_SERVER_DATE_FORMAT)
        return res

    _columns = {
        'name': fields.integer('Anno Fiscale',required=True),
        'datefrom':fields.date('Data inizio',  readonly=True),
        'dateto': fields.date('Data Fine',  readonly=True ),
        'reg_saldi':fields.one2many(
                'contab.initbalance','fiscal_id','Registra Saldi Iniziali'),
        'stato':fields.selection([
            ('new', 'Nuovo'),
            ('conf', 'Aperto'),
            ('canc', 'Chiuso')], 'Stato', required=True),
        'comptenza':fields.many2one(
            'cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related(
        'comptenza','territorio',type='many2one',
            relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one(
            'cbase.enti','Inserito Da', size=128,select=True,readonly=True),

        }
    _defaults={
            'stato':'new',
            'idente':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get(
                'res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def convalida_movimento(self,  cr,  uid, ids, context=None):
        res = {}
        for fiscal_year in self.browse(cr, uid, ids):
            _date_from=date(int(fiscal_year.name),int(1),int(1))
            _date_to=date(int(fiscal_year.name),int(12),int(31))
            res['datefrom'] =_date_from.strftime(DEFAULT_SERVER_DATE_FORMAT)
            res['dateto'] =_date_to.strftime(DEFAULT_SERVER_DATE_FORMAT)
            res['stato'] ='conf'
            self.write(cr, uid, fiscal_year.id, res)

    def annulla_movimento(self,  cr,  uid, ids, context=None):
        self.write(cr, uid, ids, {'stato': 'canc'})

    _order = 'name desc'

    _sql_constraints = [(
        'ente_anno_stato_uniq',  'unique(idente, name)',
        u'Attenzione!! Questo anno fsicale esiste già, \
        per il tuo ente.'
        )]

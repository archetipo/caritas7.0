# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------------
#   Modulo Gestione  Fondo Case
#   Autore: Alessio Gerace
#   data  : gennaio 2012
#   update :
##############################################################################


{
    'name': 'contabilità per onlus',
    'version': '1.1',
    'category': 'Modulo per gestione contabilità per onlus e Caritas',
    'description': """
     Questo modulo serve per gestire la contabilità semplice di una onlus""",
    'author': 'Alessio Gerace',
    'website': 'http://www.caritasbra.it',
    'depends': ['base','Caritas_base'],
    'init_xml': [],
    'update_xml': [
        'security/contab_security.xml',
        'security/ir.model.access.csv',
        'contab_view.xml',
    ],
    'installable': True,
    'application': True,
    'active': False
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

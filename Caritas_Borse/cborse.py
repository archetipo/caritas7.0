# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import orm, fields
from tools.translate import _

from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource
import tools
import addons
import calendar
import operator
import math
import datetime

GIORNI=[('1', 'Lunedì'),
        ('2', 'Martedì'),
        ('3', 'Mercoledì'),
        ('4', 'Giovedì'),
        ('5', 'Venerdì'),
        ('6', 'Sabato')]

MESI=[('1', 'Gennaio'),
        ('2', 'Febbraio'),
        ('3', 'Marzo'),
        ('4', 'Aprile'),
        ('5', 'Maggio'),
        ('6', 'Giugno'),
        ('7', 'Luglio'),
        ('8', 'Agosto'),
        ('9', 'Settembre'),
        ('10', 'Ottobre'),
        ('11', 'Novembre'),
        ('12', 'Dicembre')]


class cborse_func(orm.Model):
    _name = "cborse.func"
    def add_months(self,sourcedate,months):
        month = sourcedate.month - 1 + months
        year = sourcedate.year + month / 12
        month = month % 12 + 1
        day = min(sourcedate.day,calendar.monthrange(year,month)[1])
        return datetime.date(year,month,day),month

    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))

    def gen_data_mese(self,data,mese):
        #print 'generazione per data dal %s ' % data
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]

        data=date(int(a),int(m),int(g))
        g=calendar.monthrange(int(a),int(mese))[1]
        data1=date(int(a),int(mese),g)
        continua=True
        #print 'mese data gen_data',int(mese),int(g)
        meser=0
        while continua:
            #print meser,data1,mese
            if (data1>=data) and (self.mese_del(data1)==int(mese)):
                continua=False
            else:
                data1,meser=self.add_months(data1,1)
        return data1

    def pianif_ora(self,list_id,d,t):
        idd=list_id
        if idd.__len__()==0:
            cmb=datetime.datetime.combine(d,t)
        else:
            cmb=datetime.datetime.combine(d,t)
            t=timedelta(minutes=idd.__len__()*7)
            cmb=cmb+t
        return cmb


    def set_stat(self,stato,data):
        stat={'new':'pian','pian':'post'}
        if stato=='new':
            stato='pian'
        else:
          stato=self.ctl_scad(data)
        return stato

    def data_cmp(self,dat1,data2):
        val=(data2-data1).days
        if val>0:
           return False
        if val<=0:
           return True

    def ctl_scad(self,data):
        dt=self.gen_data(data)
        oggi=date.today()
        val=(oggi-dt).days
        if val>= 1:
           return 'scad'
        if val==0:
           return 'pian'
        if val<=-1:
           return 'post'

    def get_autodtprimacons(self,data,d,t,delta):
        cmb=datetime.datetime.combine(d,t)
        #print "delta data prima:"
        ctrl=datetime.datetime.combine(self.gen_data(data),t)
        deltadt=(cmb-ctrl)
        #print deltadt.days
        if deltadt.days <= delta:
            td=timedelta(days=delta)
            cmb=cmb+td
            #print 'if delta data %s' % cmb
        #print 'norm delta data %s' % cmb
        return cmb

    def ctl_scadc(self,data,scad):
        dt=self.gen_data(data)
        td=in_the_future(dt,scad)
        oggi=date.today()
        val=(oggi-dt).days
        if val>=1:
           return True
        return False

    def in_the_future(self,data,months=1):
        year, month, day = data.timetuple()[:3]
        new_month = month + months
        return datetime.date(year + (new_month / 12), new_month % 12, day)


    def tra_n_giorni(self,data,giorni):
        td=timedelta(days=giorni)
        return data+td

    def anno_corr(self):
        ret=int(date.today().strftime('%Y'))
        return ret

    def mese_corr(self):
        return int(date.today().strftime('%m'))

    def mese_del(self,data):
        return int(data.strftime('%m'))

    def mese_data(self,data):
        return int(data.strftime('%m'))

    def oltre15(self,data):
        year, month, day = data.timetuple()[:3]
        if day>=15:
            return True
        return False

    def lista_date(self,cr,uid,numero,mesi,datapc):
        v={1:30,2:15}
        res=[]
        annoc=self.anno_corr()
        user = self.pool.get('res.users').browse(cr, uid, uid)
        tid=self.pool.get('cborse.conf').search(cr, uid, [('comptenza','=',user.id_ente.id),('anno','=',annoc)])
        conf=self.pool.get('cborse.conf').browse(cr, uid, tid[0])
        dateno=self.dict_datano(conf.date)
        mesic=self.genmese(conf.mesi)
        #print 'mesi'
        #print mesic
        volte=v.get(numero)
        continua=True
        borse=numero*mesi
        dataf=datapc
        datai=datapc
        res.append(datai)
        if len(res)==(borse):
            continua=False
        while continua:
            #print dataf
            dataf=self.tra_n_giorni(dataf, volte)
            m=self.mese_data(dataf)
            #print  mesic.__contains__(m)
            if mesic.__contains__(m):
                res.append(dataf)
            if res.__len__()==borse:
                    continua=False
        return res

    def dict_datano(self,dateno):
        res=[]
        v={'ddata':date.today(),'adata':date.today()}
        for r in dateno:
            v['ddata']=self.gen_data(r['ddata'])
            v['adata']=self.gen_data(r['adata'])
            res.append(v.copy())
        return res

    def inrange(self,dateno,data):
        r1=False
        r2=False
        for r in dateno:
             r1=False
             r2=False
             if data >= r['ddata']:
                   r1=True
             if  data <= r['adata']:
                   r2=True
             if r1 and r2:
                  return True
        return False

    def genmese(self,brmese):
        r=[]
        for i in range(0,brmese.__len__()):
            a=int(brmese[i].mese)
            r.append(a)
        return r

    def gengiorni(self,brgiorni):
        r=[]
        for i in range(0,brgiorni.__len__()):
            a=int(brgiorni[i].giorno)-1
            r.append(a)
        return r
    #math.fabs((a[0]['data']-a[1]['data']).days)
    def raggruppa_per_cons(self,consdict):
        return consdict.sort(key=operator.itemgetter('ncons'))

    def calc_datadelta(self,dataprev,listdate):
        ret=[]
        conta=0
        minimo=0
        elenco=[]
        for cons in listdate:
            elenco.append(math.fabs((dataprev-cons['data']).days))
            minimo=min(elenco)
        for cons in listdate:
            v=math.fabs((dataprev-cons['data']).days)
            if v<=minimo+3:
              rig={'id':conta,'delta':v,'ncons':cons['ncons']}
              ret.append(rig.copy())
            conta+=1
        return ret

    def Lista_date_cons_annoc(self,cr,uid,durata):
        r=[]
        user = self.pool.get('res.users').browse(cr, uid, uid)
        borsa_calend=self.pool.get('cborse.calend')
        annocorrente=self.anno_corr()
        tid=self.pool.get('cborse.conf').search(cr, uid, [('comptenza','=',user.id_ente.id),('anno','>=',annocorrente)])
        for idat in tid:
            conf=self.pool.get('cborse.conf').browse(cr, uid, idat)
            annoc=conf.anno
            dateno=self.dict_datano(conf.date)
            giorni=self.gengiorni(conf.giorni)
            mesi=self.genmese(conf.mesi)
            if annoc==annocorrente:
                da=self.mese_corr()
                a=self.mese_corr()+durata+4
            else:
                da=1
                a=1+durata+4
            for m in range(da,a):
                if mesi.__contains__(m):
                    c=calendar.monthcalendar(annoc,m)
                    for x in c:
                      conta=0
                      for y in x:
                        conta+=1
                        if y!=0:
                            if giorni.__contains__(calendar.weekday(annoc,m,y)):
                              dt=date(annoc,m,y)
                              if not self.inrange(dateno, dt) :
                                  if dt>date.today():
                                      retid=borsa_calend.search(cr,uid,[('comptenza','=',user.id_ente.id),('dtconsv','=',dt.strftime('%Y-%m-%d'))])
                                      n={'data':dt,'ncons':len(retid),'cons':True}
                                      r.append(n.copy())

        return r

    def data_in_lista(self,data,listadate):
        ret=False
        conta=0
        for g in listadate:
            v=math.fabs((data-g['data']).days)
            if v==0:ret=True
        return ret


class cborse_cgiorni(orm.Model):
    _name = "cborse.cgiorni"
    _description = "conf Lista giorni consegna"
    _columns = {
        'confb_id': fields.many2one('cborse.conf','id configurazione'),
        'giorno':fields.selection(GIORNI, 'Giorno'),
    }


class cborse_cmesi(orm.Model):
    _name = "cborse.cmesi"
    _description = "conf Lista mesi consegna"
    _columns = {
        'confb_id': fields.many2one('cborse.conf','id configurazione'),
        'mese':fields.selection(MESI,'Mese'),
    }


class cborse_cdate(orm.Model):
    _name = "cborse.cdate"
    _description = "conf Lista date escluse"
    _columns = {
        'confb_id': fields.many2one('cborse.conf','id configurazione'),
        'ddata': fields.date("dal"),
        'adata': fields.date("al"),
    }


class cborse_conf(orm.Model):
    _name = "cborse.conf"
    _description = "configurazione Pianificazione consegne"
    _columns = {
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'anno':fields.integer('Configurazione valida Per l anno',required=True,select=True),
        'nbmax':fields.integer('Numero Max di Borse consegnate al giorno'),
        'nbext':fields.integer('Numero Max di Borse posticipate'),
        'arai':fields.integer('Orario Inizio consegna'),
        'araf':fields.integer('Orario Fine consegna'),
        'tdcons':fields.float('durata cons',  digits=(14, 2),readonly=True),
        'qcons':fields.float('intervallo di tempo minimo di ogni consegna',  digits=(14, 2),readonly=True),
        'giorni':fields.one2many('cborse.cgiorni','confb_id','Giorni Consegna' ,required=True),
        'mesi':fields.one2many('cborse.cmesi','confb_id','Mesi Consegna',required=True),
        'date':fields.one2many('cborse.cdate','confb_id','Date e Festività',required=True),
        'opinsft':fields.char('Operatore', size=32),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    _defaults = {
        'nbmax':30,
        'nbext':5,
        'dtin':fields.date.context_today,
        'anno':int(date.today().strftime('%Y')),
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
    }

    def calcola_dati(self, cr, uid,ids,context=None):
        cid=ids[0]
        res=0.0
        dat=self.browse(cr, uid, cid)
        res=round((float(dat.araf)-float(dat.arai))/(float(dat.nbmax)+float(dat.nbext)),2)
        self.write(cr, uid, cid, {'qcons': res})

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #~ #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(cborse_conf, self).search(cr, uid, new_args)
    def read_group(self, cr, uid, domain,fields,groupby, offset=0, limit=None,order=None,context=None):
        #~ #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #~ #print 'domain gruppo articoli %s' % new_domain
        return super(cborse_conf, self).read_group(cr, uid, new_domain, fields , groupby, limit, context)


class cborse_calend(orm.Model):
    _name = "cborse.calend"
    _description = "Calendario Borse "
    _columns = {
        'pnl_id':fields.many2one('cborse.auto','id auto pianif'),
        'ind_id':fields.many2one('cbase.indigente', 'Indigente',select=True,required=True),
        'dtcons': fields.datetime("Data consegna calc"),
        'dtconsv':fields.date("Data consegna prevista",required=True),
        'nome': fields.char('Nome', size=128),
        'delegato':fields.char('delegato', size=128),
        'indig_code': fields.char('Codice', size=32,select=True),
        'scad': fields.date("scadenza"),
        'ora' : fields.float('Ora Pianificata', help='ora stimata di consegna',readonly=True),
        'stato':fields.selection([('new', 'Nuova'),
                                 ('pian', 'Pianificata'),
                                 ('cons', 'Consegnata'),
                                 ('scad', 'Scaduta'),
                                 ('prep', 'Preparata'),
                                 ('post', 'Posticipata')], 'Stato'),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'lung_day':fields.integer('Lunghezza Giorno'),
        'prova':fields.integer('Persone',readonly=True),
        'opinsft':fields.char('Operatore', size=32),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        }

    def _count_at_data(self,cr,uid,ids,data,context=None):
        cid=ids[0]
        num=len(self.search(cr,uid,[('dtconsv','=',data)]))
        #print 'chiamato conteggio e vale ',num
        return num

    def cambio_id(self,cr,uid,ids,context=None):
        cid=ids[0]
        br=self.browse(cr, uid, cid)
        if br.ind_id.active:
            self.write(cr, uid, cid, {'nome': br.ind_id.name, 'indig_code': br.ind_id.indig_id})

    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))

    def change_data(self,cr,uid,ids,data,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        func=self.pool.get('cborse.func')
        annoc=func.anno_corr()
        tid=self.pool.get('cborse.conf').search(cr, uid, [('comptenza','=',user.id_ente.id),('anno','=',annoc)])
        conf=self.pool.get('cborse.conf').browse(cr, uid, tid[0])
        d=func.gen_data(data)
        idret=self.search(cr,uid,[('comptenza','=',user.id_ente.id),('dtconsv','=',d.strftime('%Y-%m-%d'))])
        maxc=conf.nbmax
        lst=func.Lista_date_cons_annoc(cr,uid,2)
        if func.data_in_lista(d,lst):
            if idret.__len__()>=20:
                str='In questa data sono pianificate %d Consegne' % idret.__len__()
                raise orm.except_orm(_('Info'), _(str))
        else:
          str='Questa non è una data di consegna'
          raise orm.except_orm(_('Attenzione'), _(str))


    def read_conf(self, cr, uid, ids,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        anno=self.pool.get('cborse.func').anno_corr()
        tid=self.pool.get('cborse.conf').search(cr, uid, [('comptenza','=',user.id_ente.id),('anno','=',anno)])
        func=self.pool.get('cborse.conf').browse(cr, uid, tid[0])
        return func

    def Calc_tempi(self, cr, uid, ids,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        cid=ids[0]
        func=self.pool.get('cborse.func')
        annoc=func.anno_corr()
        br=self.browse(cr, uid, cid)
        cfg=self.read_conf(cr, uid, ids, context)
        data=br.dtconsv
        d=func.gen_data(data)
        stato=br.stato
        idd=self.search(cr, uid,[('comptenza','=',user.id_ente.id),('dtconsv','=',data)])
        t=datetime.time(cfg.arai)
        d=func.gen_data(data)
        cmb=func.pianif_ora(idd,d,t)
        stato=func.set_stat(stato,data)
        self.write(cr, uid, cid, {'dtcons':cmb.strftime('%d-%m-%Y %H:%M:%S'),'stato':stato,'scad':data})

    def genera_dati(self,cr,uid,ids,context=None):
        self.cambio_id(cr, uid, ids, None)
        self.Calc_tempi(cr, uid, ids, None)

    def aggiornaborse(self,cr,uid,context=None):
        self.idcompetenze=self.pool.get('cbase.enti').search(cr,uid,[('esterno','=',False)])
        self.competenza=0
        self.func=self.pool.get('cborse.func')
        self.oggi=date.today()
        self.dtsearch=date.today()-datetime.timedelta(days=1)
        for self.competenza in self.idcompetenze:
            self.idborse=self.search(cr,uid,[('comptenza','=',self.competenza),('dtconsv','=',self.dtsearch),('stato','!=','cons')])
            #print self.idborse
            if len(self.idborse)>0:
                for idt in self.idborse:
                    self.Calc_tempi(cr, uid, [idt], None)

    def unlink(self, cr, uid, ids, context=None):
        lineas=self.browse(cr,uid,ids)
        for linea in lineas:
            if (linea.stato=='scad' or linea.stato=='cons'):
                raise orm.except_orm('Attenzione','Impossibile eleiminare borse consegnate o scadute' )
                return True
        return super(cborse_calend,self).unlink(cr, uid,ids, context)


    def read_group(self, cr, uid, domain,*args, **kwargs):
        #~ #print 'ovverride Group Articoli'
        #print args
        #print kwargs
        if 'context' in kwargs:
            context=kwargs['context']
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]

        return super(cborse_calend, self).read_group(cr, uid, new_domain,  *args, **kwargs)
    _defaults = {
                'dtin':fields.date.context_today,
                'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
                'ora':0.5,
                'lung_day':4,
                'stato':'new',
                'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
                'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
                }
    _order = 'dtconsv asc'


class cborse_auto(orm.Model):

    _name = "cborse.auto"
    _description = "pianificazione Automatica Consegna Borsa"
    _columns = {
        'ind_id':fields.many2one('cbase.indigente', 'Nome',required=True),
        'ind_code':fields.char('Codice', size=32, select=True),
        'pianif':fields.one2many('cborse.calend','pnl_id','Lista date Pianificate',required=True),
        'dtpc':fields.date("Data prima consegna",required=True),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'numero':fields.integer('numero'),
        'mesi':fields.integer('mesi'),
        'id_borsa':fields.integer('borsa'),
        'stat':fields.integer('stato'),
        'opinsft':fields.char('Operatore', size=32),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }

    _defaults = {
        'dtini':fields.date.context_today,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
        'stat':0,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def chk_indc(self,cr,uid,ids,ind_code,context=None):
        res = {'value':{}}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if ind_code:
            indic=self.pool.get('cbase.indigente').search(cr, uid, [('comptenza','=',user.id_ente.id),('indig_id','=',ind_code.upper())])
            if indic.__len__()>0:
                func=self.pool.get('cborse.func')
                indib=self.pool.get('cbase.indigente').browse(cr,uid,indic[0]).borse
                #print 'indib %s' % indib
                for b in indib:
                    #print b
                    if not(b.st_scad | b.st_proc) and b:
                        t=datetime.time(0)
                        if b.primacons:
                            #print b.primacons
                            #print b.dtini
                            d=func.gen_data_mese(b.dtini,b.primacons)
                        else:
                            d=func.gen_data(b.dtini)
                        #print 'data gen data %s' % d
                        dpc= func.get_autodtprimacons(b.dtini,d,t,15)
                        num=b.qt
                        dur=b.scad
                        res = {'value': {'ind_id' : indic[0],'dtpc' : dpc.strftime('%Y-%m-%d'),'numero':int(num),'mesi':int(dur),'id_borsa':b.id,}}
        return res

    def chk_indi(self,cr,uid,ids,indi_id,context=None):
        res = {'value':{}}
        if indi_id:
            func=self.pool.get('cborse.func')
            indib=self.pool.get('cbase.indigente').browse(cr,uid,indi_id).borse
            for b in indib:
                if not(b.st_scad | b.st_proc):
                        t=datetime.time(0)
                        d=func.gen_data(b.dtini)
                        dpc= func.get_autodtprimacons(b.dtini,d,t,15)
                        num=b.qt
                        dur=b.scad
                        res ={'value': {'dtpc' : dpc.strftime('%Y-%m-%d'),'numero':int(num),'mesi':int(dur),'id_borsa':b.id}}
        return res

    def genera_dati(self,cr,uid,ids,context=None):
        p=self.browse(cr, uid, ids[0])
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        stat=self.pool.get('cbase.borse').browse(cr,uid,p.id_borsa)
        if not stat.st_proc:
            func=self.pool.get('cborse.func')
            datai=func.gen_data(p.dtpc)
            lst=[]
            br=self.pool.get('cborse.calend')
            annoc=func.anno_corr()
            tid=self.pool.get('cborse.conf').search(cr, uid, [('comptenza','=',user.id_ente.id),('anno','=',annoc)])
            conf=self.pool.get('cborse.conf').browse(cr, uid, tid[0])
            lnc=[]
            brcons=stat=self.pool.get('cbase.borse')
            maxc=conf.nbmax
            lista=func.lista_date(cr,uid,p.numero,p.mesi,datai)
            lst=func.Lista_date_cons_annoc(cr,uid,p.mesi)
            ##############################################################################
            #In lista ho le date calcolate
            #in lst ho le date reali di consegna da oggi a oggi + i mesi
            #Devo fare la ricerca delle date di lista in lst e caricare in lst il num di consegne del giorno
            #controllo la data provo -1 e +1 guardo quale ha meno consegne se le consegne sono uguali fa il giorno -1
            #di una data di consegna controllo sempre la consegna successiva se ha molte meno consegne tipo 5 in meno la elaboro la cons succ
            #dovremmo quasi esserci
            ##############################################################################
            final=[]
            idr=[]
            #print 'lista'
            #print lista
            #print 'lst'
            #print lst
            for d in lista:
                continua=True
                cnt=0
                r=func.calc_datadelta(d,lst)
                r.sort(key=operator.itemgetter('delta'))
                #print 'r'
                #print r
                lista_numcons=[]
                for nc in r:
                    lista_numcons.append(nc['ncons'])
                #print 'lista num consegne',lista_numcons
                cnt=lista_numcons.index(min(lista_numcons))
                f=lst[r[cnt]['id']]['data'] #in d aggiorno la data a quella più vicina per ora non tengo ancora
                #conto del numero id ocnsegne appena ho più dati aggiungo la procedura
                dt=f.strftime('%d-%m-%Y')
                idr=[]
                s=br.create(cr,uid,{'ind_id':p.ind_id.id,'dtconsv':dt,'pnl_id':ids[0],})
                idr.append(s)
                br.genera_dati(cr,uid,idr,None)
                br.read(cr,uid,idr[0])
                self.write(cr,uid,ids[0],{'stat':1})
            self.pool.get('cbase.borse').write(
                cr,uid,p.id_borsa,{'st_proc':True})
        return True

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #print new_args
        return super(cborse_auto, self).search(cr, uid, new_args)

    def read_group(self, cr, uid, domain,fields,groupby, offset=0, limit=None,order=None,context=None,orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]

        return super(cborse_auto, self).read_group(cr, uid, new_domain, fields , groupby, limit, context,orderby)

    def unlink(self, cr, uid, ids, context=None):
        for id in ids:
            if len(self.pool.get('cborse.calend').search(cr,uid,[('pnl_id','=',id)])) >0:
                raise orm.except_orm('Attenzione','Impossibile eleiminare il gruppo ci sono pianificazioni attive.' )
            else:
                idb=self.read(cr,uid,id,['id_borsa'])['id_borsa']
                self.pool.get('cbase.borse').unlink(cr,uid,[idb])
                super(cborse_auto,self).unlink(cr, uid,[id], context)
        return True

    _order = 'dtini desc'
    _rec_name = 'ind_id'
cborse_auto()

class cbase_famiglia(orm.Model):
    _inherit = 'cbase.famiglia'
    _description = 'famiglia'

    _columns = {
        'id_cons':fields.integer('consegna'),
    }


class cborse_borsa(orm.Model):
    def _ut_ente(self, cr, uid, ids,name, args, context=None):
        pass
    _name = "cborse.borsa"
    _description = "Consegna Borsa"
    _columns = {
        'cod_tessera':fields.char('Codice', size=32, states={'bozza':[('readonly',False)]}),
        'ind_id':fields.many2one('cbase.indigente', 'Persona',required=True),
        'deleg':fields.char('Delegato', size=32),
        'doc_del':fields.char('Documento Delegato', size=32),
        'cod_doc':fields.char('Codice Borsa', size=32,required=True),
        'fam_id':fields.one2many('cbase.famiglia','id_cons','Famiglia'),
        'riga_b':fields.one2many('maga.scarico','id_borsa','Lista',required=True),
        'stat':fields.boolean('Stato'),
        'state': fields.selection([
            ('bozza','Si'),
            ('preparata','Si'),
            ('compl','No'),
            ],'Modificabile', readonly=True),
        'id_calend':fields.integer('id calendario',states={'bozza':[('readonly',False)]}),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32,states={'bozza':[('readonly',False)]}),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,readonly=True),
        }

    _defaults = {
        'stat':False,
        'state':'bozza',
        'dtini':fields.date.context_today,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def chk_artic(self,cr,uid,ids,riga_b,context=None):
        res = {'value':{}}
        if riga_b:
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            idc=ids[0]
            borsacorr=self.browse(cr,uid,idc)
            oggi=date.today()
            da=oggi-datetime.timedelta(days=30)
            borset=self.pool.get('cborse.borsa').search(cr, uid, [('comptenza','=',user.id_ente.id),
                                                                 ('ind_id','=',borsacorr.ind_id.id),
                                                                 ('stat','=',False),
                                                                 ('dtini','>=',da.strftime('%Y-%m-%d')),
                                                                 ('dtini','<',oggi.strftime('%Y-%m-%d')),
                                                                ])
            listart=[]
            scar=self.pool.get('maga.scarico').search(cr,uid,[('id_borsa', 'in', borset)])
            objart=self.pool.get('maga.scarico').browse(cr,uid,scar)
            for art in objart:
                listart.append(art.cod_art.id)
            l=int(len(riga_b)-1)
            if l<0:l=0
            #print listart
            #print riga_b
            #print l
            if riga_b[l][0]==0:
                if listart.__contains__(riga_b[l][2]['cod_art']):
                    str='%s è già stato/a consegnato/a negli ultimi 30 giorni' % riga_b[l][2]['nome']
                    return {'warning': {
                                'title': _('Attenzione'),
                                'message':  _(str),
                                }
                            }
        return res

    def change_ind(self,cr,uid,ids,ind_id,context=None):
        if ind_id:
            ct=self.pool.get('cbase.indigente').browse(cr,uid,ind_id).indig_id
            res=self.codice_borsa(cr,uid,ids,ct,False,context)
            return res
        else:
            return {}

    def codice_borsa(self,cr,uid,ids,cod_tessera,ch=True,context=None):
        if cod_tessera:
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            res={'value':{}}
            #print cod_tessera
            ststo=0
            res=""
            func=self.pool.get('cborse.func')
            borse=self.pool.get('cborse.borsa').search(cr, uid, [('comptenza','=',user.id_ente.id),('cod_doc','<>',False)])
            numborse=borse.__len__()
            # borsa=self.browse(cr, uid, cid)
            brid=self.pool.get('cborse.calend').search(cr, uid, [('comptenza','=',user.id_ente.id),('indig_code','=',cod_tessera.upper())])
            br=self.pool.get('cborse.calend').browse(cr, uid,brid)
            if br.__len__()>0:
                oggi=date.today()
                idcal=0
                stato=0
                idp=0
                for b in br:
                    dt=func.gen_data(b.dtconsv)
                    if ((dt-oggi).days==0) & (b.stato=='pian' or b.stato=='post'):
                       stato=1
                       idcal=b.id
                       idp=b.ind_id.id
                if stato==1:
                    res="B%d" % (numborse)
                    #Devo Salvare il record in cui lavoro per avere l'id e poi ficcarlo nella tabella!! è l'unica!!
                    #indi=self.browse(cr, uid, ids[0])
                    res= {'value':{'cod_doc':res,
                                     'id_calend':idcal,
                                     'cod_tessera':cod_tessera}}
                    if ch:res['value']['ind_id']=idp
                else:
                    delta=50
                    c=0
                    i=0
                    data=""
                    for b in br:
                        if b.stato=='pian' or b.stato=='post':
                            min=(func.gen_data(b.dtconsv)-oggi).days
                            if min<delta :
                                 delta=min
                                 data=b.dtconsv
                            c+=1
                    str='Per oggi non sono previste consegne,la prossima consegna sarà %s' % data
                    raise orm.except_orm(_('Attenzione'), _(str))
            else:
              str='codice utente errato'
              raise orm.except_orm(_('Attenzione'), _(str))
            #print res
            return res
        else:
            return {}

    def init_cons(self,cr,uid,ids,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        res={'value':{}}
        idc=ids[0]
        b=self.browse(cr,uid,idc)
        idp=b.ind_id.id
        idf=self.pool.get('cbase.famiglia').search(cr,uid,[('ind_id','=',idp)])
        for iidf in idf:
            if self.pool.get('cbase.famiglia').browse(cr,uid,iidf).pborsa:
                a=self.pool.get('cbase.famiglia').write(cr,uid,iidf,{'id_cons':idc})
        #delegato
        deleg=self.pool.get('cbase.indigente').browse(cr,uid,idp).deleg
        doc_del=self.pool.get('cbase.indigente').browse(cr,uid,idp).doc_del
        self.write(cr,uid,idc,{'stat':True,'deleg':deleg,'doc_del':doc_del,'state':'preparata'})
        return True

    def valida(self,cr,uid,ids,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        cid=ids[0]
        borsa=self.browse(cr, uid, cid)
        idlinee=self.pool.get('maga.scarico').search(cr, uid, [('comptenza','=',user.id_ente.id),('id_borsa','=',cid)])
        linee=self.pool.get('maga.scarico').browse(cr, uid, idlinee)
        for linea in linee:
            if not linea.stat:
                self.pool.get('maga.scarico').write(cr,uid,linea.id,{'stat':1})
                idd=[]
                idd.append(linea.id)
                self.pool.get('maga.scarico').Calc_tot(cr,uid,idd)
        self.pool.get('cborse.calend').write(cr, uid,borsa.id_calend,{'stato':'cons'})
        self.write(cr,uid,cid,{'state':'compl','stat':False})
        return True
    #    def annulla(self,cr,uid,ids,context=None):
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #print new_args
        return super(cborse_borsa, self).search(cr, uid, new_args)

    def read_group(self, cr, uid, domain,fields,groupby, offset=0, limit=None,order=None,context=None,orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]

        return super(cborse_borsa, self).read_group(cr, uid, new_domain, fields , groupby, limit, context,orderby)
    _order = 'dtini desc'



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


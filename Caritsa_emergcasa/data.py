# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------------
#   Modulo Base gestione utenti Caritas e servizi sociali
#           utenti famiglia lavoro
#   Autore: Alessio Gerace
#   data  : Dicembre 2012
#   update : Gennaio 2013
#
#
#
#
#
##############################################################################
#campi selection

soglie={'primo':10.0,'secondo':15.0,'terzo':20.0,'quarto':15.0,'quinto':15.0}

famiglia=[('mono', 'Monoparentale'),
          ('fatto', 'Di fatto o di diritto')]

famigliar=[('monor', 'Unico'),
          ('piur', 'oltre un percettore')]



occupazioned=[('def', 'Altra condizione'),
			('occp-occp', 'Occupazione Precaria - Occupazione Precaria'),
			('occp-ci', 'Occupazione Precaria - Cassa Integrazione'),
			('occp-mob', 'Occupazione Precaria - Mobilità'),
			('occp-dis', 'Occupazione Precaria - Disoccupazione'),
			('occp-ciz', 'Occupazione Precaria - Cassa Integrazione Zero ore'),
			('ci-ci', 'Cassa Integrazione - Cassa Integrazione'),
			('ci-ciz', 'Cassa Inegrazione - Cassa Inegrazione Zero ore'),
			('ci-mob', 'Cassa Inegrazione - Mobilita'),
			('ci-dis', 'Cassa Inegrazione - Disoccupazione'),
			('ciz-ciz', 'Cassa Inegrazione Zero ore - Cassa Inegrazione Zero ore'),
			('ciz-mob', 'Cassa Inegrazione Zero ore - Mobilita'),
			('ciz-dis', 'Cassa Inegrazione Zero ore - Disoccupazione'),
			('mob-mob', 'Mobilita - Mobilita'),
			('dis-mob', 'Disoccupazione - Mobilita'),
			('dis-dis', 'Disoccupazione - Disoccupazione')]



numpar=[    ('duep', 'Due persone'),
			('trep', 'Tre persone'),
			('quatp','Quattro persone'),
			('cinqp','Cinque persone'),
			('seip', 'Sei persone'),
			('setp', 'Sette persone'),
			('ottop', 'Otto persone'),
			('ottooltr', 'Oltre otto persone'),
			]


anziano=[('no','No'),('u65','A carico'),('pens','Pensionato')]


#tabella dei punti
dat={		'mono':7.0, #mono parentale
			'fatto':0.0,
			'monor':1.0, #mono reddito
            'fm':1.0,  #figlio minorenne
            'Pp':3.0, #persona con patologia
            'u65':2.0,
            'pens':-2.0, # ? chiedere
            'def':0.0,
            'no':0.0,
            'cp':1.0,   # coniuge con patologia
            'occs':-3.0, #occupato stabile -3
            'occp':20.0, #occupato precario
            'dis':5.0,  #disoccupato
            'mob':5.0, #mobilità
            'ci':15.0,   #cassa integrazione
            'ciz':15.0,   #cassa integ. zero
            '2':2.0,
			'3':4.0,
			'4':6.0,
			'5':8.0,
			'6':10.0,
			'7':12.0,
			'8':14.0,
			'9':15.0,
			'occp-occp':20.0,
			'occp-ci':18.0,
			'occp-mob':18.0,
			'occp-dis':16.0,
			'occp-ciz':16.0,
			'ci-ci':14.0,
			'ci-ciz':14.0,
			'ci-mob':12.0,
			'ci-dis':8.0,
			'ciz-ciz':12.0,
			'ciz-mob':10.0,
			'ciz-dis':8.0,
			'mob-mob':8.0,
			'dis-mob':5.0,
			'dis-dis':5.0,
			'piur':0.0
            }

#resituisce ill punteggio in base al valore isee
def calcisee(val):
    retv=0.0
    val=int(val)
    if val<=3000:
        return 0.0
    elif val <=4000:
        return 3.0
    elif val <=5000:
        return 6.0
    elif val <=6000:
        return 9.0
    elif val <=7000:
        return 12.0
    elif val <=7500:
        return 15.0
    elif val<=8000:
        return 12.0
    elif val<=9000:
        return 9.0
    elif val <=10000:
        return 6.0
    elif val<=11000:
        return 3.0
    else:
        return 0.0

def calcredd(val):
    retv=0.0
    val=int(val)
    if val<=6000:
        return 0.0
    elif val <=8000:
        return 3.0
    elif val <=10000:
        return 6.0
    elif val <=12000:
        return 9.0
    elif val <=14000:
        return 12.0
    elif val <=17000:
        return 15.0
    elif val<=19000:
        return 12.0
    elif val<=21000:
        return 9.0
    elif val<=23000:
        return 6.0
    elif val<=25000:
        return 3.0
    else:
        return 0.0


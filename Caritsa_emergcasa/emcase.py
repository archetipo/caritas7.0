# -*- coding:  utf-8 -*-
##############################################################################
#
#    OpenERP,  Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http: //tiny.be>).
#
#    This program is free software:  you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation,  either version 3 of the
#    License,  or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not,  see <http: //www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------
#   Modulo Gestione  Fondo Case
#   Autore:  Alessio Gerace
#   data  :  gennaio 2012
#   update :  febbraio 2012
#
#
#
#
#
##############################################################################

from openerp.osv import orm,  fields
from tools.translate import _
from datetime import datetime,  timedelta,  time,  date
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from openerp import SUPERUSER_ID
import decimal_precision as dp
import string
import resource
import tools
import addons
import calendar
import operator
import math
import data as data

occupazione = [
    ('def',  'Altra condizione'),
    ('occs',  'Occupazione Stabile'),
    ('ci',  'Cassa Integrazione'),
    ('ciz',  'Cassa Inegrazione Zero ore'),
    ('occp',  'Occupazione precaria'),
    ('mob',  'Mobilita'),
    ('dis',  'Disoccupato  Cessazione autonomo')
    ]


class emerg_config(orm.Model):
    _name = "emerg.config"
    _description = "Rubrica Locatore Conduttore punti"
    _columns = {
        'name': fields.many2one(
            'caritas.progetti', 'Progetto', required=True),
        'parent_id': fields.many2one(
            'caritas.progetti', 'Controlla i dati del Progetto'),
        'anno': fields.integer('Anno', required=True),
        'annodati': fields.integer(
            'I criteri sono relativi all\' anno', required=True),
        'maxcandidati': fields.one2many(
            'emerg.confvalente', 'name',
            'Configurazione Numero candidati per Comune',
            required=False),
        'dtin': fields.datetime("Data inserimento"),
        'opinsft': fields.char('Operatore',  size=32),
        }

    _default = {
        'anno': lambda *a: int(datetime.date.today().strftime('%Y')),
        'dtin': fields.date.context_today,
        'opinsft': lambda self,  cr,  uid,  c:  self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).name,
        }

    _sql_constraints = [(
        'name_anno_uniq',  'unique(name, anno)',
        u'Attenzione!! Questo nome esiste già, \
        selezionarlo dalla lista e modificare quello.'
        )]


class emerg_confvalente(orm.Model):
    _name = "emerg.confvalente"
    _description = "Rubrica Locatore Conduttore punti"

    def _getdiff(self,  cr,  uid,  ids, field_name,  args,  context=None):
        res = {}
        emrr = self.pool.get('emerg.emerrubrica')
        anno=int(date.today().strftime('%Y'))
        if ids:
            objse = self.browse(cr,  uid,  ids)
            for ent in objse:
                res[ent.id] = {'erogc': 0.0, 'diffe': 0.0}
                idss = emrr.search(
                    cr, uid, [
                    ('territorio', '=', ent.territorio.id),
                    ('anno', '=', ent.name.anno),
                    ])
                domande = emrr.browse(cr, uid, idss)
                totdom = 0.0
                totpdom = 0.0
                for domanda in domande:
                    if domanda.evaso != '0' and domanda.stato == 'chiuso':
                        totdom += float(domanda.evaso)
                    elif domanda.evaso != '0' and domanda.stato == 'accettata':
                        totpdom += float(domanda.evaso)

                res[ent.id]['erogc'] = (totdom)
                res[ent.id]['assegnato'] = (totpdom+totdom)
                res[ent.id]['diffe'] = (ent.numc - totdom)
                res[ent.id]['diffe_assegnato'] = (ent.numc - (totpdom+totdom))
        else:
            res = {}
        return res

    _columns = {
        'name': fields.many2one('emerg.config', 'Configurazione', size=128),
        'numc': fields.float('Importo Massimo', digits=(14, 2)),
        'assegnato': fields.function(
            _getdiff, method = True, store=False,
            string = 'Importo Preventivo Assegnato', digits=(14, 2),
            type = 'float', multi = 'utente'),
        'diffe_assegnato': fields.function(
            _getdiff, method = True, store=False,
            string = 'Importo Residuo Assegnabile', digits=(14, 2),
            type = 'float', multi = 'utente'),
        'erogc': fields.function(
            _getdiff, method = True, store=False,
            string = 'Tot Importo Erogato', digits=(14, 2),
            type = 'float', multi = 'utente'),
        'diffe': fields.function(
            _getdiff, method = True,  store=False,
            string = 'Da erogare', type = 'float',
            digits=(14, 2), multi = 'utente'),
        'comptenza': fields.many2one(
            'cbase.enti', 'Ente Riferimento',
            select = True, required=True),
        'territorio': fields.related(
            'comptenza', 'territorioprog', type = 'many2one',
            relation = 'res.city',  string = 'Territorio',  readonly=True),
        'dtin':  fields.datetime("Data inserimento"),
        'opinsft': fields.char('Operatore',  size=32),
        }
    _default = {
        'dtin': fields.date.context_today,
        'opinsft': lambda self,  cr,  uid,  c:  self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).name,
        }


class emerg_lavori(orm.Model):
    _name = "emerg.lavori"
    _description = "Lavori"
    _columns = {
        'opinsft': fields.char('Operatore',  size=128, readonly=True),
        'name':  fields.char(
            'Condizione Lavorativa',  size=128, required=True),
        'relat': fields.many2one('cbase.lavori', 'Lavoro',  size=128),
        'statcod':  fields.char('Codice statistico',  size=32),
        }
    _defaults = {
        'opinsft': lambda self,  cr,  uid,  c:  self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).name,
        }


class emerg_famiglia(orm.Model):
    _inherit = 'cbase.famiglia'
    _name = 'emerg.famiglia'
    _description = 'Progetti al quale ha partecipato'

    _columns = {
        'emrg_id': fields.many2one('emerg.emerrubrica',  'Capo famiglia'),
        'anno': fields.integer('Anno'),
        'annodati': fields.integer('Anno'),
        'reddito': fields.float('Reddito € ', digits=(14, 2)),
        'lavoro': fields.many2one('emerg.lavori', 'Lavoro',  size=128),
        'redditoap': fields.float('Stima ultimo anno € ', digits=(14, 2)),
        'invalidita': fields.boolean('Invalidità Superiore 66%'),
        'incarico': fields.boolean('A carico'),
        'redditop': fields.boolean('Percettore Reddito'),
        'comptenza': fields.many2one(
            'cbase.enti', 'Competenza di ',  size=128, select = True),
        'territorio': fields.related(
            'comptenza', 'territorioprog', type = 'many2one',
            relation = 'res.city',  string = 'Territorio Progetti',
            readonly=True),
        }
    _defaults = {
        'anno': lambda *a: int(date.today().strftime('%Y')),
        'comptenza': lambda self,  cr,  uid,  c:  self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).id_ente.id,
        }

    def add_aggfam(self, cr, uid, id):
        indi_fam = self.pool.get('cbase.famiglia')
        indig = self.pool.get('cbase.famiglia')
        obj = self.browse(cr, uid, id)
        find_parente = indi_fam.search(cr, uid, [('name', '=', obj.name.id)])
        find_capfam = indi_fam.search(cr, uid, [('ind_id', '=', obj.name.id)])
        #aggiorna la scheda generale famiglari e le singole schede TODO

        if len(find_parente) > 0:
            idf = find_parente[0]
        elif len(find_capfam) > 0:
            idf = find_capfam[0]
        else:
            vals = {
                'ind_id': obj.ind_id.id, 'name': obj.name.id,
                'eta': obj.eta, 'grad': obj.grad.id, 'isee': True,
                'statfam': True, 'conv': True, 'opinsft': obj.opinsft}
            idf = indi_fam.create(cr, 1, vals)
        return True

    def write(self,  cr,  uid,  ids, vals,  context=None):
        for fam in ids:
            if 'name' in vals:
                id_trov = self.search(
                    cr, uid, [('emrg_id', '=', vals['name'])])
                if id_trov and len(id_trov) > 0:
                    raise orm.except_orm(
                        _(u'Attenzione!!'),
                        _(u'Questo utente è già un capofamiglia'))
                    return False
            obj = self.browse(cr, uid, fam)
            if obj.ind_id != obj.emrg_id.name.id:
                vals['ind_id'] = obj.emrg_id.name.id
            idr = super(emerg_famiglia, self).write(
                cr,  uid, ids,  vals,  context)
            self.add_aggfam(cr, uid, fam)
        return idr

    def create(self,  cr,  uid,  vals,  context=None):
        if (
                vals['nome'] is False or
                vals['cognome'] is False or
                vals['codfis'] is False
                ):
            raise orm.except_orm(
                _(u'Attenzione!!'),
                _(u'Nome Cognome e Codice fiscale sono obbligatori'))
            return False
        p_rubr = self.pool.get('emerg.emerrubrica')

        id_trov = p_rubr.search(cr, uid, [
            ('codfis', '=', vals['codfis']),
            ('anno', '=', vals['anno'])
            ])
        if id_trov and len(id_trov) > 0:
            raise orm.except_orm(
                _(u'Attenzione!!'),
                _(u'Questo utente è già caricato come \
                    intestatario di una scheda.'))
            return False
        o_rubr = p_rubr.browse(cr, uid, vals['emrg_id'])

        p_indi = self.pool.get('cbase.indigente')
        indid_trov = p_indi.search(cr, uid, [(
            'codfis', '=', o_rubr.name.codfis)])
        if len(indid_trov) > 0:
            o_indi = p_indi.browse(cr, uid, indid_trov[0])
            vals['ind_id'] = o_indi.id
        else:
            vals['ind_id'] = o_rubr.name.id
        id = super(emerg_famiglia, self).create(cr,  uid,  vals,  context)
        self.add_aggfam(cr, uid, id)
        return id

    _sql_constraints = [(
        'name_parente_uniq_anno',  'unique(name, anno)',
        u'Attenzione!! questo utente è già presente come famigliare'),
        ]


class emerg_emerrubrica(orm.Model):
    _name = "emerg.emerrubrica"
    _description = "Rubrica Locatore Conduttore punti"

    def _getrule(self,  cr,  uid,  ids, field_name,  args,  context=None):
        ogg_confe = self.pool.get('emerg.confvalente')
        res = {}
        if ids:
            objssk = self.browse(cr,  uid,  ids)
            for ric in objssk:
                #~ user,
                #~ prj_rule,
                #~ uprj_id,
                #~ user_territorio,
                #~ conf
                rule_dic= self.get_user_prj_rule_anno(cr, uid, ric.anno)
                res[ric.id] = {
                    'userruolo': rule_dic['prj_rule'],
                    'userente': False,
                    'userterritorio': rule_dic['user_territorio'],
                    'numc': 0.0,
                    'erogc': 0.0,
                    'diffe': 0.0
                    }
                if (rule_dic['user'].id == 1 or ric.territorio.id == rule_dic['user'].id_ente.territorioprog.id):
                    res[ric.id]['userente'] = True
                for c in rule_dic['conf'].maxcandidati:
                    if c.territorio.id == ric.territorio.id:
                        res[ric.id]['numc'] = c.numc
                        res[ric.id]['erogc'] = c.erogc
                        res[ric.id]['diffe'] = c.diffe
                        res[ric.id]['assegnato'] = c.assegnato
                        res[ric.id]['diffe_assegnato'] = c.diffe_assegnato
        else:
            res = {'userruolo': 'user', 'userente': True}
        return res

    _columns = {
        'name': fields.many2one('cbase.indigente',  'Conduttore'),
        'nome':  fields.char('Nome',  size=128,  required=True),
        'cognome':  fields.char('Cognome',  size=128,  required=True),
        'codfis': fields.char(
            'Codice Fiscale',  size=32,  help="Codice fiscale", required=True),
        'strada': fields.char('in Strada',  size=128),
        'risidente': fields.char('Frazione / Luogo', size=128),
        'cittares': fields.many2one('res.city',  string='Residente a: '),
        'civico': fields.char('num Civico',  size=12),
        'tel': fields.char('Telefono Richiedente',  size=32, select = True),
        'cell': fields.char('Cellulare Richiedente',  size=32,  select = True),
        'lavoro': fields.many2one('emerg.lavori', 'Lavoro',  size=128),
        'redditop': fields.float('Reddito 2011 € ', digits=(14, 2)),
        'redditopap': fields.float('Reddito 2012 € ', digits=(14, 2)),

        'numprat': fields.integer('Numero pratica (solo numeri)'),

        'affittoa': fields.float('Canone Affitto annuale € ', digits=(14, 2)),
        'spcond': fields.float('Canone Spese condoinio € ', digits=(14, 2)),

        'invalidita': fields.boolean('Invalidità Superiore 66%'),
        'single': fields.boolean('Utnete Singolo senza Famiglia'),
        'famigliari': fields.one2many('emerg.famiglia', 'emrg_id', 'Famigliari'),
        #~ capofamiglia e nucleo famigliare
        'nucleof': fields.related(
            'name', 'nucleof', type = 'many2one',
            relation = 'cbase.indigente',  string = 'Nucleo Famigliare'),
        'parentinucleo': fields.related(
            'nucleof', 'famiglia', type = 'one2many',
            relation = 'cbase.famiglia',  string = 'Nucleo Famigliare'),
        #~ parenti trovati
        'parenti': fields.related(
            'name', 'famiglia', type = 'one2many',
            relation = 'cbase.famiglia',  string = 'Nucleo Famigliare'),

        'crit1': fields.float('Criterio 1', digits=(14, 1), readonly=True),
        'isee': fields.float(
            'ISEE 2012 € ', digits=(14, 2), required=True),
        'crit2': fields.float('Criterio 2', digits=(14, 1), readonly=True),
        'reddito': fields.float(
            'Reddito 2012 € ', digits=(14, 2), required=True),

        'evaso': fields.selection([('0',  '0'),
                                  ('750',  '750 €'),
                                  ('1100',  '1100 €'),
                                  ('1500',  '1500 €')],
                                  'Contributo erogato di', select = True),
        'nomep': fields.char('Nome Locatore',  size=64, select = True),
        'indp': fields.char('indirizzo',  size=128),
        'zipp':  fields.char('CAP',  size=24),
        'municipalityp':  fields.many2one('res.city',  'Citta'),
        'provincep':  fields.many2one('res.province',  string = 'Provincia'),
        'regionp':  fields.many2one('res.region',  string = 'Regione'),
        'telp': fields.char('Telefono',  size=32),



        'crit3': fields.float('Criterio 3', digits=(14, 1), readonly=True),
        'duepiu': fields.boolean('più di due percettori di reddito'),
        'monoreddito': fields.boolean('Unico percettore di redito'),
        'occstab': fields.boolean('Un percettore di reddito è occupato stabile'),
        'lav': fields.selection(occupazione,  'Condizione Lavorativa', required=True),
        'lavd': fields.selection(data.occupazioned,
            'Condizione Lavorativa due percettori', required=True),

        'crit4': fields.float('Criterio 4', digits=(14, 1), readonly=True),
        'numpers': fields.integer('Famiglia composta da', required=True),
        'famiglia': fields.selection(data.famiglia,  'Nucleo Famigliare', required=True),
        'figlimin': fields.integer('Numero Figli minorenni', required=True),
        'perspat': fields.integer('Numero persone con Patologia-Invalidita sup. 66%', required=True),
        'anziani': fields.selection(data.anziano,  'Ultra 65 enne', required=True),
        'famigliar': fields.selection(data.famigliar,  'Percettori di reddito', required=True),

        'crit5': fields.float('Criterio 5', digits=(14, 1)),
        'impegno':  fields.boolean('Il Locatore ha aderito al patto di solidarietà'),
        'punti': fields.float('Punteggio totale', digits=(14, 1), select = True),
        'candidato': fields.selection([('si', 'Candidato'), ('no', 'Escluso')],  'Candidato'),
        'caritas': fields.selection([('si', 'SI'), ('no', 'NO'), ('inc', 'Controllare')],  'In Caritas'),
        'note': fields.text('Note Generali'),
        'debiti': fields.float('Situazione Debitoria', digits=(14, 2)),
        'opinsft': fields.char('Operatore',  size=32, readonly=True),

        'dtin':  fields.datetime("Data inserimento", readonly=True),
        'idente': fields.many2one(
            'cbase.enti', 'Inserito Da',  size=128, select=True, readonly=True),
        'comptenza': fields.many2one(
            'cbase.enti', 'Competenza di ',  size=128, select = True),
        'territorio': fields.related(
            'comptenza', 'territorioprog', type = 'many2one',
            relation = 'res.city',  string = 'Territorio', select = True),
        'provincia': fields.related(
            'comptenza', 'province', type = 'many2one',
            relation = 'res.city',  string = 'Provincia',  readonly=True),
        'proj_id': fields.many2one(
            'emerg.config',  'Progetto emergenza casa al quale si riferisce'),
        'numc': fields.function(
            _getrule, method = True,  store=False, type = 'float',
            string = 'Totale Disponibilità',  readonly=True, multi = 'utente'),
        'assegnato': fields.function(
            _getrule, method = True, store=False,
            string = 'Importo Preventivo Assegnato', digits=(14, 2),
            type = 'float', multi = 'utente'),
        'diffe_assegnato': fields.function(
            _getrule, method = True, store=False,
            string = 'Importo Residuo Assegnabile', digits=(14, 2),
            type = 'float', multi = 'utente'),
        'erogc': fields.function(
            _getrule, method = True,  store=False, type = 'float',
            string = 'Totale Già Approvato',  readonly=True, multi = 'utente'),
        'diffe': fields.function(
            _getrule, method = True,  store=False, type = 'float',
            string = 'Disponibilità Residua',  readonly=True, multi = 'utente'),
        'anno': fields.integer('Anno'),
        'userente': fields.function(
            _getrule, method = True,  store=False,
            string = 'attiva', type = 'boolean',  multi = 'utente'),
        'userruolo': fields.function(
            _getrule, method = True,  store=False,
            string = 'ruolo', type = 'char' ,  size=32, multi = 'utente'),
        'carcodfis': fields.char(
            'Carica Tessera Sanitaria',  size=200,
            help = "Carica un utente tramite Tessera Sanitaria"),
        'inizia': fields.boolean('continua caricamento'),
        'stato': fields.selection([('bozza',  'Nuova'),
                                  ('nuova',  'In caricamento'),
                                  ('valutaz',  'Completato'),
                                  ('accettata',  'Assegnato'),
                                  ('escluso',  'Non Erogabile'),
                                  ('chiuso',  'Erogato')],
                                  'Stato', select = True),
        'borsa_lavoro': fields.boolean('Si rende disponible per borsa lavoro'),
        'pscode': fields.char('Codice doc p stab',  size=64, select = True),
        'psdate': fields.date("Data p stab")
        }

    _defaults = {
        'anno': lambda *a: int(date.today().strftime('%Y')),
        'dtin': fields.date.context_today,
        'opinsft': lambda self,  cr,  uid,  c:  self.pool.get(
            'res.users').browse(cr,  uid,  uid,  c).name,
        'figlimin': 0,
        'famiglia': 'fatto',
        'famigliar': 'monor',
        'perspat': 0,
        'anziani': 0,
        'anziani': 0,
        'crit1': 0.0,
        'isee': 0.0,
        'crit2': 0.0,
        'reddito': 0.0,
        'crit3': 0.0,
        'lav': 'def',
        'lavd': 'def',
        'crit4': 0.0,
        'crit5': 0.0,
        'punti': 0.0,
        'candidato': 'no',
        'anziani': 'no',
        'caritas': 'no',
        'evaso': '0',
        'userente': True,
        'userruolo': 'user',
        'inizia': False,
        'single':False,
        'numpers': 1,
        'stato': 'bozza',
        'idente': lambda self,  cr,  uid,  c:  self.pool.get('res.users').browse(cr,  uid,  uid,  c).id_ente.id,
        'comptenza': lambda self,  cr,  uid,  c:  self.pool.get('res.users').browse(cr,  uid,  uid,  c).id_ente.id,
        'territorio': lambda self,  cr,  uid,  c:  self.pool.get('res.users').browse(cr,  uid,  uid,  c).id_ente.territorioprog.id,
        }

    def inserisci_dati(self,  cr,  uid, ids, context=None):
        res = {'value': {}}
        print ids
        self.write(cr, uid, ids, {'inizia': True, 'stato': 'nuova'})
        ids = ids[0]
        obj = self.browse(cr, uid, ids)
        indio = self.pool.get('cbase.indigente')
        emer_fam = self.pool.get('emerg.famiglia')
        if obj.nucleof:
            for pn in obj.parentinucleo:
                d = {'name': False, 'nome': '', 'cognome': '',
                    'codfis': '', 'grad': False, 'anno':obj.anno}
                if pn.name.id == obj.name.id:
                    d['name'] = obj.nucleof.id
                    d['nome'] = obj.nucleof.nome
                    d['cognome'] = obj.nucleof.cognome
                    d['codfis'] = obj.nucleof.codfis
                    d['grad'] = pn.grad.id
                else:
                    d['name'] = pn.name.id
                    d['nome'] = pn.nome
                    d['cognome'] = pn.cognome
                    d['codfis'] = pn.codfis
                    d['grad'] = pn.grad.id
                d['emrg_id'] = ids
                print 'nucleo fam ', d
                emer_fam.create(cr, uid, d)
        elif obj.parenti:
            for pn in obj.parenti:
                d = {'name': False, 'nome': '', 'cognome': '',
                    'codfis': '', 'grad': False, 'anno':obj.anno}
                d['name'] = pn.name.id
                d['nome'] = pn.nome
                d['cognome'] = pn.cognome
                d['codfis'] = pn.codfis
                d['grad'] = pn.grad.id
                d['emrg_id'] = ids
                print 'parenti ', d
                emer_fam.create(cr, uid, d)
        return res

    def get_projectid_confid_from_year(self, cr, uid, anno):
        conf_id = self.pool.get('emerg.config').search(cr,  uid, [('anno', '=', anno)])
        prj_id = self.pool.get('emerg.config').browse(cr, uid, conf_id[0]).name.id
        return prj_id, conf_id[0]

    def aggiorna_erogaenti(self, cr, uid, progetto, importo, indi, anno):
        prj_indio = self.pool.get('cbase.erogenti')
        idriga = prj_indio.search(cr, uid, [('ind_id', '=', indi), ('proj_id', '=', progetto)])
        if len(idriga) > 0:
            val = {'imp': importo}
            return prj_indio.write(cr, uid, idriga[0], val)
        else:
            data = {
                'proj_id': progetto,
                'ind_id': indi,
                'anno': anno,
                'imp': importo}
            return prj_indio.create(cr, uid, data)

    def accetta_domanda(self,  cr,  uid, ids, context=None):
        self.calcola_punti(cr, uid, ids, context)
        obj = self.browse(cr, uid, ids[0])
        prj_id, conf_id = self.get_projectid_confid_from_year(cr, uid, obj.anno)
        if obj.crit5 <= 0:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Impostare un valore maggiore di zero al Criterio 5 \
                    prima di procedere con l\'accettazione'))
            return False
        if self.aggiorna_erogaenti(cr, uid, prj_id, obj.evaso, obj.name.id, obj.anno):
            self.write(cr, uid, ids, {'stato': 'accettata'})
            return True
        else:
            return False

    def escludi_domanda(self,  cr,  uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0])
        prj_id, conf_id = self.get_projectid_confid_from_year(cr, uid, obj.anno)
        if self.aggiorna_erogaenti(cr, uid, prj_id, 0.0, obj.name.id, obj.anno):
            self.write(cr, uid, ids, {'stato': 'escluso'})
            return True
        else:
            return False

    def reimposta_domanda(self,  cr,  uid, ids, context=None):
        self.calcola_punti(cr, uid, ids, context)
        obj = self.browse(cr, uid, ids[0])
        prj_id, conf_id = self.get_projectid_confid_from_year(cr, uid, obj.anno)
        if self.aggiorna_erogaenti(cr, uid, prj_id, 0.0, obj.name.id, obj.anno):
            self.write(cr, uid, ids, {'stato': 'valutaz'})
            return True
        else:
            return False

    def imposta_erogata(self,  cr,  uid, ids, context=None):
        prj_indio = self.pool.get('cbase.erogenti')
        id = ids[0]
        if self.browse(cr, uid, id).evaso == 'no':
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Selezionare il valore da erogare e poi procedere'))
        else:
            self.write(cr, uid, ids, {'stato': 'chiuso'})
        return True

    def get_user_prj_rule_anno(self, cr, uid, anno):
        print 'User Function'
        emerg_conf = self.pool.get('emerg.config')
        prj_conf = self.pool.get('caritas.progettiutenti')
        idc = emerg_conf.search(cr,  uid, [('anno', '=', anno)])
        conf = emerg_conf.browse(cr, uid, idc[0])
        # da inserire gestione controllo stato progetto
        statoproj = conf.name.stato
        usert = prj_conf.search(cr,  uid, [
            ('name', '=', conf.name.id), ('user_id', '=', uid)])
        if statoproj == 'aperto':
            if usert and len(usert) > 0:
                prj_rule = prj_conf.browse(cr, uid, usert[0]).prj_rule
                user_territorio = 0
                prj_id = conf.name.id
                user = self.pool.get('res.users').browse(
                    cr,  uid,  uid,  context={})
                dict = {
                    'user': user,
                    'prj_rule': prj_rule,
                    'prj_id':prj_id,
                    'user_territorio': user_territorio,
                    'conf': conf
                    }
                return dict
            else:
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'Utente non abilitato a questo progetto, \
                        contattare amministrazione'))
                return False
        elif statoproj == 'chiuso':
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'Questo progetto è concluso'))
                return False
        elif statoproj == 'nuovo':
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'Questo progetto non è ancora attivo'))
                return False

    def get_user_prj_rule(self, cr, uid):
        anno = datetime.date.today().strftime('%Y')
        return self.get_user_prj_rule_anno(cr, uid, anno)

    def test_indi(self, cr, uid, codfisc):
        oindi = self.pool.get('cbase.indigente')
        indi_id = oindi.search(cr, uid, [('codfis', '=', codfisc)])
        if indi_id and len(indi_id) > 0:
            return indi_id[0]
        else:
            return 0

    def on_change_codf(self, cr, uid, ids, nome, cognome, ccf, context=None):
        oindi = self.pool.get('cbase.indigente')
        print nome, cognome, ccf
        indiexist = 0
        if nome and cognome and ccf:
            if ccf and len(ccf) == 16:
                ccf = ccf.upper()
                indiexist = self.test_indi(cr, uid, ccf)
                func = oindi.pool.get('cbase.func')
                italia = func.get_nazione(cr, uid)
                res = oindi.load_cf(cr, uid, nome, cognome, ccf)
                if res['value']['codfis'] == ccf:
                    if indiexist == 0:
                        del res['value']['name']
                        return res
                    else:
                        res['value']['name'] = indiexist
                        notaauto = 'Il richiedente è presente nel sistema o caricato dalla Caritas  oppure ha partecipato la scorsa edizione senza ricevere il contributo \n Per maggiorni informazioni ricercare il nominativo del conduttore in gestione anagrafiche \n o contattare il centro d\'ascolto di zona.\n '
                        res['value']['note'] = notaauto
                        return res
                else:
                    raise orm.except_orm(
                        _(u'Attenzione'),
                        _(u'Il codice fiscale non è congruo con i dati \
                           inseriti, verificare nome e cognome o il codice'))
                    return False
            else:
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'Il codice fiscale non è valido \
                       mancano o eccedono caratteri'))
                return False
        else:
            return False

    def on_change_ccf(self, cr, uid, ids, ccf, context=None):
        oindi = self.pool.get('cbase.indigente')
        if ccf:
            t = ccf.split(' ')
            if t.count('') == 1:
                ccf = ccf.upper()
                func = self.pool.get('cbase.func')
                indiexist = self.test_indi(cr, uid, ccf)
                nome, cognome, cf, code = func.decodeCf(ccf)
                italia = func.get_nazione(cr, uid)
                res = oindi.load_cf(cr, uid, nome, cognome, cf)
                if indiexist == 0:
                    del res['value']['name']
                    return res
                else:
                    res['value']['name'] = indiexist
                    res['value']['note'] = 'Presente in Caritas \n'
                    return res
                #isvalid,  get_birthday,  get_sex,  control_code,  build
            else:
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'La Tessera sanitaria non è valida o è scaduta'))
                return False
        else:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'Strisciare la tessera sanitaria nell\'apposito lettore'))
            return False

    def on_change_name(self, cr, uid, ids, name, context=None):
        print name
        res = {'value': {}}
        if name:
            indi = self.pool.get('cbase.indigente').browse(cr, uid, name)
            if indi:
                res['value'] = {
                    'nome':  indi.nome,
                    'cognome': indi.cognome,
                    'codfis':  indi.codfis,
                    'strada':  indi.strada,
                    'risidente': indi.risidente,
                    'cittares': indi.cittares.id,
                    'civico':  indi.civico,
                    'tel': indi.tel,
                    'cell': indi.cell,
                    }
        return res



    def on_change_municipalityp(self,  cr,  uid,  ids,  city):
        res = {'value': {}}
        if(city):
            city_obj = self.pool.get('res.city').browse(cr,  uid,  city)
            res = {'value':  {
                'provincep': city_obj.province_id.id,
                'regionp': city_obj.region.id,
                'zipp':  city_obj.zip,
                'municipalityp':  city,
                }}
        return res

    def test_city(self, city, territorio):
        print 'test',  city, territorio
        if city!=territorio:
            raise orm.except_orm(
                _(u'Attenzione'),
                _(u'La città di residenza non può essere quella inserita'))
            return False
        else:
            return True

    def on_change_city(self,  cr,  uid,  ids,  city, territorio):
        res = {'value': {}}
        if self.test_city(city, territorio):
            return res
        else:
            return False

    def on_change_monor(self,  cr,  uid,  ids,  monoreddito):
        res = {'value': {}}
        if monoreddito:
            res = {'value':  {
                'famigliar': 'monor'}}
        else:
            res = {'value':  {
                'famigliar': 'piur'}}
        return res

    def calcola_punti(self,  cr,  uid,  ids,  context=None):
        famo = self.pool.get('emerg.famigliari')
        cid = ids[0]
        punti = 0.0
        pool = self.browse(cr,  uid,  cid)
        pini = 0.0
        if pool:
            if  len(pool.famigliari)==0 and pool.single==False:
                raise orm.except_orm(
                    _(u'Attenzione'),  _(u'Inserire almeno un famigliare'))
                return False


            pool.numpers += len(pool.famigliari)
            if pool.numpers >= 9:
                pool.numpers = 9

            crit1 = data.calcisee(pool.isee)
            crit2 = data.calcredd(pool.reddito)

            lavfam = []
            redditf = 0
            disab = 0
            utlra65 = 0
            figlimin = 0
            fammoccst = False
            crit3 = 0
            if pool.redditop>0 or pool.redditopap>0 :
                print 'reddito personal' , pool.reddito
                redditf += 1

            if pool.lavoro.statcod:
                if pool.lavoro.statcod=='occs':
                    pool.occstab = True
                else:
                    if pool.lavoro.statcod !='def':
                        lavfam.append(pool.lavoro.statcod)
            else:
                raise orm.except_orm(
                    _(u'Attenzione'),
                    _(u'Manca la situazione lavorativa del conduttore.'))
                return False

            if pool.invalidita:
                disab += 1

            if pool.single == False:
                for f in pool.famigliari:
                    if f.reddito >0 or f.redditoap >0:
                        redditf += 1
                    if f.lavoro.statcod:
                        if f.lavoro.statcod == 'occs':
                            if pool.occstab:
                                fammoccst = True
                            else:
                                pool.occstab = True
                        else:
                            if f.lavoro.statcod != 'def':
                                lavfam.append(f.lavoro.statcod)

                    else:
                        str = 'Manca la situazione lavorativa \
                               del parente %s.'  % (f.name.name)
                        raise orm.except_orm(_(u'Attenzione'),  _(str))
                        return False

                    if f.eta < 18 and f.incarico:
                        figlimin += 1
                    if f.eta >= 65 and f.incarico:
                        utlra65 += 1
                    if f.invalidita and f.incarico:
                        disab += 1

                if redditf == 1 and fammoccst is False:
                    pool.famigliar = 'monor'
                if redditf >= 2:
                    pool.famigliar = 'piur'
                if redditf <= 2 and fammoccst is False:
                    if pool.occstab and len(lavfam) > 0:
                        crit3 = data.dat[lavfam[0]]-3.0
                    else:
                        if len(lavfam) == 1:
                            crit3 = data.dat[lavfam[0]]
                        elif len(lavfam) > 1:
                            test1 = '%s-%s' % (lavfam[0], lavfam[1])
                            test2 = '%s-%s' % (lavfam[1], lavfam[0])
                            if test1 in data.dat:
                                crit3 = data.dat[test1]
                            elif test2 in data.dat:
                                crit3 = data.dat[test2]
                else:
                    pool.famigliar = 'piur'
            else:
               if lavfam: crit3 = data.dat[lavfam[0]]
            print 'lavfam',  lavfam
            print 'redditf', redditf
            print 'disab', disab
            print 'utlra65', utlra65
            print 'figlimin', figlimin
            print 'figl redd', pool.famigliar

            if crit3 < 0:
                crit3 = 0
            crit4 = 0

            if pool.single == False :
                if pool.numpers > 1:
                    npersone = '%d' % pool.numpers
                    crit4 = data.dat[npersone]
                crit4 += data.dat[pool.famiglia]
                crit4 += data.dat[pool.famigliar] #uno - +percettori di reddito
                crit4 += (data.dat['fm'] * figlimin)
                crit4 += (data.dat['u65']*utlra65)
                crit4 += (data.dat['Pp'] * disab)
                if crit4 > 15.5:
                    crit4 = 15.0

            crit5 = pool.crit5
            pini = crit1+crit2+crit3+crit4+crit5
            if pool.stato == 'nuova' or pool.stato == 'bozza':
                self.write(cr,  uid,  cid,  {
                    'punti': pini,
                    'crit1': crit1,
                    'crit2': crit2,
                    'crit3': crit3,
                    'crit4': crit4,
                    'crit5': crit5,
                    'stato': 'valutaz'})
            else:
                self.write(cr,  uid,  cid,  {
                    'punti': pini,
                    'crit1': crit1,
                    'crit2': crit2,
                    'crit3': crit3,
                    'crit4': crit4,
                    'crit5': crit5})

        return {'value': {}}

    def get_evaso(self, val):
        if val == '0':
            return 0.0
        if val == '750':
            return 750.0
        if val == '1100':
            return 1100.0
        if val == '1500':
            return 1500.0

    def create_indi(self, cr, uid, nome, cognome, ccf, args):
        oindi = self.pool.get('cbase.indigente')
        res = oindi.load_cf(cr, uid, nome, cognome, ccf)
        res['value']['strada'] = args['strada']
        res['value']['risidente'] = args['risidente']
        res['value']['cittares'] = args['cittares']
        res['value']['civico'] = args['civico']
        res['value']['tel'] = args['tel']
        res['value']['cell'] = args['cell']
        indi_id = oindi.create(cr, uid, res['value'])
        indi = oindi.browse(cr, uid, indi_id)
        res = {}
        res['nazionalitar'] = indi.nazionalita.id
        oindi.write(cr, uid, indi_id, res)
        return indi_id

    def create(self,  cr,  uid,  vals,  context=None):
        print context
        vals['anno']=context['anno']
        rule_dict=self.get_user_prj_rule_anno(cr, uid, vals['anno'])
        if vals['name'] == 0:
            vals['name'] = self.create_indi(
                cr, uid, vals['nome'], vals['cognome'], vals['codfis'], vals)
        prj_indio = self.pool.get('cbase.erogenti')
        if 'anno' in context and context['anno']:
            anno = context['anno']
            idc = self.pool.get('emerg.config').search(
                cr,  uid, [('anno', '=', anno)])
            if idc and len(idc) > 0:
                print 'Trovato anno per forza'
                prj_conf=self.pool.get(
                    'emerg.config').browse(cr, uid, idc[0])
                print prj_conf.name.dtal
                c_data=datetime.strptime(prj_conf.name.dtal, DEFAULT_SERVER_DATE_FORMAT).date()
                print c_data,(date.today() > c_data)
                if date.today() > c_data:
                    raise orm.except_orm(
                        _(u'Attenzione'),
                        _(u'Non si possono più aggiungere richieste \n \
                            in questa edizione del progetto, si possono \n \
                            solo concludere le domande già caricate fino alla chiusura.')
                        )
                    return False
                prj_id = prj_conf.name.id
                ind_id = vals['name']
                evaso = self.get_evaso(vals['evaso'])
                vals['proj_id'] = idc[0]
                prj_s = self.pool.get('emerg.config').search(
                    cr,  uid, [('name', '=', prj_conf.parent_id.id)])
                if len(prj_s) > 0:
                    idret=self.search(cr,  uid, [
                        ('proj_id', '=', prj_s[0]),
                        ('codfis', '=', vals['codfis']),
                        ('stato', '=', 'chiuso')
                        ])
                    if len(idret)>0:
                        raise orm.except_orm(
                            _(u'Attenzione'),
                            _(u'Il richiedente che si tenta di caricare\n \
                                ha già ricevuto il contributo la scorsa edizione\n \
                                quindi non sarà registrato come da regolamento.')
                            )
                        return False

                    idret=self.search(cr,  uid, [
                        ('proj_id', '=', prj_s[0]),
                        ('parenti.codfis', '=', vals['codfis']),
                        ('stato', '=', 'chiuso')
                        ])
                    if len(idret)>0:
                        raise orm.except_orm(
                            _(u'Attenzione'),
                            _(u'Il richiedente che si tenta di caricare\n \
                                è un parente di un richiedete che ha già \
                                ricevuto il contributo la scorsa edizione\n \
                                quindi non sarà registrato come da regolamento.')
                            )
                        return False
                    idret=self.search(cr,  uid, [
                        ('proj_id', '=', prj_s[0]),
                        ('codfis', '=', vals['codfis']),
                        ('stato', '!=', 'chiuso')
                        ])
                    if len(idret)>0:
                        old = self.browse(cr, uid, idret[0])
                        vals['nomep']=old.nomep
                        vals['indp']=old.indp
                        vals['municipalityp']=old.municipalityp.id
                        vals['provincep']=old.provincep.id
                        vals['regionp']=old.regionp.id
                        vals['telp']=old.telp
                        vals['affittoa']=old.affittoa
                        vals['spcond']=old.spcond
        idr = super(emerg_emerrubrica, self).create(cr,  uid,  vals,  context)
        data = {
            'proj_id': prj_id,
            'ind_id': ind_id,
            'anno': anno,
            'imp': evaso}
        prj_indio.create(cr, uid, data)
        #controllare Flag inizia....
        self.inserisci_dati(cr,uid,[idr],context=context)
        return idr

    def write(self,  cr,  uid,  ids, vals,  context=None):
        print 'Write rubrica '
        print ids
        print vals
        if type(ids) == int:
            line_id = ids
        if type(ids) == long:
            line_id = ids
        elif type(ids) == list:
            line_id = ids[0]
        prj_indi = self.pool.get('cbase.indigente')
        o = self.browse(cr, uid, line_id)

        rule_dict=self.get_user_prj_rule_anno(cr, uid, o.anno)
        if hasattr(vals, '__iter__') and 'cittares' in vals:
            if vals['cittares'] is False:
                raise orm.except_orm(
                    _(u'Attenzione'),  _(u'Inserire la città di residenza'))
                return False
            else:
                if not self.test_city(vals['cittares'], o.territorio.id):
                    return False
        print 'Write exex '
        if not 'anno' in vals:
            vals['anno']=o.anno
        idr = super(emerg_emerrubrica, self).write(
            cr,  uid, ids,  vals,  context)
        print 'update cbase %s' % idr
        self.update_cbase(cr,uid,[line_id],context=context)
        return idr


    def update_cbase(self,cr,uid,ids,context=None):
        prj_indi = self.pool.get('cbase.indigente')
        emerg_lines = self.browse(cr, uid, ids)
        for emrg in emerg_lines:
            indi = prj_indi.browse(cr, uid, emrg.name.id)
            if emrg.id > 0 and indi:
                res = {}
                if emrg.nome != indi.nome:
                    res['nome'] = emrg.nome
                if emrg.cognome != indi.cognome:
                    res['cognome'] = emrg.cognome
                if emrg.nome != indi.nome:
                    res['codfis'] = emrg.codfis
                if emrg.codfis != indi.codfis:
                    res['strada'] = emrg.strada
                if emrg.risidente != indi.risidente:
                    res['risidente'] = emrg.risidente
                if emrg.cittares.id != indi.cittares.id:
                    res['cittares'] = emrg.cittares.id
                if emrg.civico != indi.civico:
                    res['civico'] = emrg.civico
                if emrg.tel != indi.tel:
                    res['tel'] = emrg.tel
                if emrg.cell != indi.cell:
                    res['cell'] = emrg.cell
                if indi.nazionalitar is False:
                    res['nazionalitar'] = emrg.nazionalita.id
                print ' Update cbase %s' % res
                prj_indi.write(cr, uid, emrg.name.id, res)




    _order = 'punti desc'
    _rec_name = 'cognome'
    _sql_constraints = [(
        'emerg_codfis_anno_uniq',
        'UNIQUE(codfis, anno)',
        u'Questo utente esiste già...')
        ]

# vim: expandtab: smartindent: tabstop = 4: softtabstop = 4: shiftwidth = 4:

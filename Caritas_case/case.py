# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------------
#   Modulo Gestione  Fondo Case
#   Autore: Alessio Gerace
#   data  : gennaio 2012
#   update : febbraio 2012
#   
#
#
#
#
##############################################################################

from osv import osv,fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp
import string
import resource 
import tools
import addons
import datetime
import calendar
import operator
import math
import data as data

print data.occupazione

class case_rubrica(osv.osv):
	_name = "case.rubrica"
	_description = "Rubrica Affittuario Locatore"
	_columns = {
		'name':fields.char('Nome Beneficiario', size=64,select=True,required=True),
		'cf':fields.char('Codice Fiscale', size=15),
		'ind':fields.char('indirizzo', size=128),
		'zip': fields.char('CAP', size=24),
		'municipality': fields.many2one('res.city', 'Citta'),
		'province': fields.many2one('res.province', string='Provincia'),
		'region': fields.many2one('res.region', string='Regione'),
		'tel':fields.char('Telefono', size=32),
		'nomep':fields.char('Nome Locatore', size=64,select=True,required=True),
		'indp':fields.char('indirizzo', size=128),
		'zipp': fields.char('CAP', size=24),
		'municipalityp': fields.many2one('res.city', 'Citta'),
		'provincep': fields.many2one('res.province', string='Provincia'),
		'regionp': fields.many2one('res.region', string='Regione'),
		'telp':fields.char('Telefono', size=32),
		'crit1':fields.float('Criterio 1',digits=(14, 1),readonly=True),
		'isee':fields.float('ISEE 2011 € ',digits=(14, 2),required=True),
		'crit2':fields.float('Criterio 2',digits=(14, 1),readonly=True),
		'reddito':fields.float('Reddito 2011 € ',digits=(14, 2),required=True),
		'crit3':fields.float('Criterio 3',digits=(14, 1),readonly=True),
		'duepiu':fields.boolean('più di due percettori di reddito'),
		'monoreddito':fields.boolean('Unico percettore di redito'),
		'occstab':fields.boolean('Un percettore di reddito è occupato stabile'),
		'lav':fields.selection(data.occupazione, 'Condizione Lavorativa',required=True),
		'lavd':fields.selection(data.occupazioned, 'Condizione Lavorativa due percettori',required=True),
		'crit4':fields.float('Criterio 4',digits=(14, 1),readonly=True),
		'numpers':fields.selection(data.numpar, 'Famiglia composta da',required=True),
		'famiglia':fields.selection(data.famiglia, 'Nucleo Famigliare',required=True),
		'figlimin':fields.integer('Numero Figli minorenni',required=True),
		'perspat':fields.integer('Numero persone con Patologia-Invalidita sup. 66%',required=True),
		'anziani':fields.selection(data.anziano, 'Ultra 65 enne',required=True),
		'famigliar':fields.selection(data.famigliar, 'Percettori di reddito',required=True),
		'crit5':fields.float('Criterio 5',digits=(14, 1)),
		'conpat':fields.boolean('Il Coniuge ha una patologia'),
		'canoni':fields.selection([('afpc', 'Affidabile'),
				   ('nafpc', 'Non Affidabile')], 'Pagamento canoni precendeti',required=True),
		'sussidi':fields.selection([('sus', 'SI'),
			('nsus', 'NO')], 'Percepisce sussidi',required=True),
		'impegno': fields.boolean('Il Locatore si impegna ad accettare il compromesso'),
		'punti':fields.float('Punteggio totale',digits=(14, 1)),
		'candidato':fields.selection([('si','Candidato'),('no','Escluso')], 'Candidato',select=True),
		'caritas':fields.selection([('si','SI'),('no','NO'),('inc','Controllare')], 'In Caritas',select=True),
		'info':fields.text('Informazioni',readonly=True),
		'note':fields.text('Note Centro Ascolto'),
		'opinsft':fields.char('Operatore', size=32),
		'dtin': fields.datetime("Data inserimento",readonly=True),
		
			}
	_defaults = {
			'dtin':fields.date.context_today,
			'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
			'figlimin':0,
			'famiglia':'fatto',
			'famigliar':'piur',
			'perspat':0,
			'anziani':0,
			'anziani':0,
			'crit1':0.0,
			'isee':0.0,            
			'crit2':0.0,
			'reddito':0.0,
			'crit3':0.0,
			'lav':'def',
			'lavd':'def',
			'crit4':0.0,
			'crit5':0.0,
			'sussidi':'nsus',
			'canoni':'nafpc',
			'punti':0.0,
			'candidato':'no',
			'anziani':'no',
			'caritas':'no',
			'info':''
			}

	def on_change_municipality(self, cr, uid, ids, city):
		res = {'value':{}}
		if(city):
			city_obj = self.pool.get('res.city').browse(cr, uid, city)
			res = {'value': {
				'province':city_obj.province_id.id,
				'region':city_obj.region.id,
				'zip': city_obj.zip,
				'municipality': city,
				}}
		return res
		

		
	def on_change_municipalityp(self, cr, uid, ids, city):
		res = {'value':{}}
		if(city):
			city_obj = self.pool.get('res.city').browse(cr, uid, city)
			res = {'value': {
				'provincep':city_obj.province_id.id,
				'regionp':city_obj.region.id,
				'zipp': city_obj.zip,
				'municipalityp': city,
				}}
		return res
		
	def on_change_monor(self, cr, uid, ids, monoreddito):
		res = {'value':{}}
		if monoreddito:
			res = {'value': {
				'famigliar':'monor'}}
		else:
			res = {'value': {
				'famigliar':'piur'}}
		return res
		
	def calcola_punti(self, cr, uid, ids, context=None):
		cid=ids[0]
		punti=0.0
		pool=self.browse(cr, uid, cid)
		pini=0.0
		if pool:
			print pool.famiglia
			crit1=data.calcisee(pool.isee)
			crit2=data.calcredd(pool.reddito)
			if not pool.duepiu:
				if pool.occstab:
					crit3=data.dat[pool.lav]-3.0
				else:
					if pool.monoreddito:
						crit3=data.dat[pool.lav]
					else:
						crit3=data.dat[pool.lavd]
			if crit3<0: crit3=0
			crit4=data.dat[pool.numpers]
			crit4+=data.dat[pool.famiglia]
			crit4+=data.dat[pool.famigliar]
			crit4+=(data.dat['fm'] * pool.figlimin)
			crit4+=data.dat[pool.anziani]
			crit4+=(data.dat['Pp'] * pool.perspat)
			if crit4 > 15.5: crit4=15.0
			crit5=pool.crit5
			#if pool.conpat: crit5+=(data.dat['cp'])
			#crit5+=data.dat[pool.sussidi]
			pini=crit1+crit2+crit3+crit4+crit5
			#print pini
		self.write(cr, uid, cid, {'punti':pini,'crit1':crit1,'crit2':crit2,'crit3':crit3,'crit4':crit4,'crit5':crit5})
		#return {'value':{}}
		
	def gencandidati(self,cr, uid, context=None):
		idd=self.search(cr,uid,[('punti','>','0.0')])
		
		cnt=1
		val='si'
		
		for idt in idd:
			pres='no'
			cand=self.browse(cr, uid, idt)
			idtr=self.pool.get('cbase.indigente').search(cr,uid,['|',('name','=',cand.name.upper()),('name','=',cand.name.lower())])
			if idtr!=[]:
				if len(idtr)==1:
					pres='si'
				elif len(idtr)>1:
					pres='inc'
			if cand.impegno:
				if cnt>32: val='no'
				self.write(cr,uid,idt,{'candidato':val,'caritas':pres})
				cnt+=1
			else:
				self.write(cr,uid,idt,{'candidato':'no','caritas':pres})
		return {'value':{}}
	_order = 'punti desc'
	_sql_constraints = [('case_cf_uniq','UNIQUE(cf)', u'Questo codice fiscale esiste già...')]	
case_rubrica()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


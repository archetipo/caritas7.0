# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    
#-----------------------------------------------------------------------------------
#   Modulo Base gestione utenti Caritas e servizi sociali
#           utenti famiglia lavoro 
#   Autore: Alessio Gerace
#   data  : Maggio 2011
#   update : Giugno 2011
#   
#
#
#
#
##############################################################################
#campi selection

soglie={'primo':15.0,'secondo':15.0,'terzo':20.0,'quarto':15.0,'quinto':15.0}

famiglia=[('mono', 'Monoparentale'),
          ('fatto', 'Di fatto o di diritto')]
          
famigliar=[('monor', 'Unico'),
          ('piur', 'oltre un percettore')]
          
occupazione=[('def', 'Altra condizione'),
			('ci', 'Cassa Integrazione'),
			('ciz', 'Cassa Inegrazione Zero ore'),
			('occ', 'Occupato precario'),
			('mob', 'Mobilita'),
			('dis', 'Disoccupato - Cessazione autonomo')]
			
occupazioned=[('def', 'Altra condizione'),
			('cid', 'Cassa Integrazione - Cassa Integrazione'),
			('cicizd', 'Cassa Inegrazione - Cassa Inegrazione Zero ore'),
			('cimd', 'Cassa Inegrazione - Mobilita'),
			('cidd', 'Cassa Inegrazione - Disoccupazione'),
			('cizcizd', 'Cassa Inegrazione Zero ore - Cassa Inegrazione Zero ore'),
			('cizmd', 'Cassa Inegrazione Zero ore - Mobilita'),
			('cizdd', 'Cassa Inegrazione Zero ore - Disoccupazione'),
			('mobd', 'Mobilita - Mobilita'),
			('dismd', 'Disoccupato - Mobilita'),
			('disdisd', 'Disoccupato - Disoccupato')]
			

			
numpar=[    ('duep', 'Due persone'),
			('trep', 'Tre persone'),
			('quatp','Quattro persone'),
			('cinqp','Cinque persone'),
			('seip', 'Sei persone'),
			('setp', 'Sette persone ed oltre')]
			

anziano=[('no','No'),('u65','A carico'),('pens','Pensionato')]

affidab=[('afpc', 'Affidabile'),
         ('nafpc', 'Non Affidabile')]
suss=[('sus', 'SI'),
      ('nsus', 'NO')]
      
#tabella dei punti
dat={		'mono':2.0, #mono parentale
			'fatto':0.0,
			'monor':1.0, #mono reddito
            'fm':1.0,  #figlio minorenne
            'Pp':3.0, #figlio minorenne con patologia
            'u65':2.0,
            'pens':-2.0,
            'def':0.0,
            'no':0.0,
            'cp':1.0,   # coniuge con patologia
            'occ':12.0, #occupato
            'dis':20.0,  #disoccupato
            'mob':15.0, #mobilità
            'ci':5.0,   #cassa integrazione
            'ciz':10.0,   #cassa integ. zero
            'afpc':1.0,  #affidabilità pregeressi canoni
            'nafpc':0.0, #non affid. pregressi canoni
            'sus':1.0, # presenza sussidi
            'nsus':0.0,  #assenza sussidi
            'duep':2.0,
			'trep':4.0,
			'quatp':6.0,
			'cinqp':8.0,
			'seip':10.0,
			'setp':12.0,
			'cid':2.0,
			'cicizd':4.0,
			'cimd':6.0,
			'cidd':8.0,
			'cizcizd':10.0,
			'cizmd':12.0,
			'cizdd':14.0,
			'mobd':16.0,
			'dismd':18.0,
			'disdisd':20.0,
			'piur':0.0
            }
            
#resituisce ill punteggio in base al valore isee
def calcisee(val):
    retv=0.0
    val=int(val)
    if val<=6000:
        return 0.0
    elif val <=7000:
        return 10.0
    elif val <=8000:
        return 9.0
    elif val <=9000:
        return 8.0
    elif val <=10000:
        return 7.0
    elif val <=11000:
        return 6.0
    elif val<=12000:
        return 5.0
    elif val<=13000:
        return 4.0
    elif val <=14000:
        return 3.0
    elif val<=15000:
        return 2.0
    else:
        return 0.0
        
def calcredd(val):
    retv=0.0
    val=int(val)
    if val<=15000:
        return 0.0
    elif val <=18000:
        return 15.0
    elif val <=20000:
        return 13.5
    elif val <=22000:
        return 12.0
    elif val <=23000:
        return 10.0
    elif val <=24000:
        return 9.0
    elif val<=25000:
        return 7.5
    elif val<=26000:
        return 6.0
    elif val <=27000:
        return 4.5
    elif val<=28000:
        return 3.0
    elif val<=30000:
        return 1.5
    else:
        return 0.0
        

# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------------
#   Modulo Gestione Magazzino
#   Autore: Alessio Gerace
#   data  : Maggio 2011
#   update : Giugno 2011
#
#
#
#
#
##############################################################################

from openerp.osv import orm,  fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource
import tools
import addons
import datetime
import calendar
import operator
import math

class maga_um(orm.Model):
    _name = "maga.um"
    _description = "unita misura"
    _columns = {
        'name': fields.char('UM', size=32,readonly=True),
        'descr': fields.text('Descrizione'),
    }


class maga_tipofor(orm.Model):
    _name = "maga.tipofor"
    _description = "tipo fornitore"
    _columns = {
        'name': fields.char('Nome', size=32,select=True),
        'description': fields.text('Descrizione'),
        'agea':fields.boolean('Magazzino Agea'),
        }
    _defaults = {
        'agea':False,
        #  'Opinsf':_ut_ente
        }



class maga_articolo(orm.Model):
    _name = "maga.articolo"
    _description = "tabella base dell' articolo magazzino"
    _columns = {
        'name': fields.char('Codice', size=32,select=True),
        'descr': fields.text('Descrizione'),
        'nome':fields.char('Nome', size=32,required=True,select=True),
        'tipof':fields.many2one('maga.tipofor', 'Tipo Fornitore',required=True,select=True),
        'um':fields.many2one('maga.um', 'Unità di Misura',required=True),
        'qtb':fields.float('Quantità base per unità di prodotto',  digits=(14, 2)),
        'rqtb':fields.float('Rapporto per numero pezzi',help="es. pasta Qantità base 0.5 Kg quindi 1 Kg = 2 pezzi"),
        'qscar':fields.float('Quantità attuale in Carico'),
        'qcar':fields.float('Quantità attuale'),
        'qtatt':fields.float( 'Giacenza',readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
    }
    _defaults = {
        'tipof':1,
        'qtb':1.0,
        'rqtb':1.0,
        'qtatt':0.0,
        'qscar':0.0,
        'qcar':0.0,
        'qtatt':0.0,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        #  'Opinsf':_ut_ente
        }

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        res = []
        for record in self.browse(cr, uid, ids, context=context):
            title = record.nome
            res.append((record.id, title))
        return res
    def on_change_um(self,cr,uid,ids,context=None):
        self.Genera_codice_id(cr, uid, ids, context)

    def Genera_codice_id(self,cr,uid,ids,context=None):
        cid=ids[0]
        res=""
        art=self.browse(cr, uid, cid)
        if not art.name:
            res="Art_0%d%d" % (art.id,art.um)
            self.write(cr, uid, cid, {'name': res})
        return True

    def calcola_maga(self,cr, uid, context=None):
        ids=self.search(cr,uid,[('name','!=',' ')])
        #print 'start controllo maga ora %s' % fields.datetime.now()
        for idt in ids:
            carico=0.0
            scarico=0.0
            giacenza=0.0
            ##print idt
            idarticolic=self.pool.get('maga.carico').search(cr,uid,[('cod_art','=',idt)])
            idarticolisc=self.pool.get('maga.scarico').search(cr,uid,[('cod_art','=',idt)])

            for idartc in idarticolic:
                carico+=self.pool.get('maga.carico').browse(cr,uid,idartc).qeff
            for idartsc in idarticolisc:
                if self.pool.get('maga.scarico').browse(cr,uid,idartsc).stat: scarico+=self.pool.get('maga.scarico').browse(cr,uid,idartsc).qeff
            giacenza=carico-scarico
            ##print 'carico %d scarico %d giacenza %d' % (carico,scarico,giacenza)
            self.write(cr,uid,idt,{'qcar':carico,'qscar':scarico,'qtatt':giacenza})
        #print 'fine controllo maga ora %s' % fields.datetime.now()
        return {}

    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        #print domain
        #print fields
        #print groupby
        #print context
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_articolo, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args =[]
        #print 'ovverride Search Articoli'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            else:
                new_args += [arg]
        #print 'articoli new ',new_args
        return super(maga_articolo, self).search(cr, uid, new_args, offset, limit, order, context, count)


class maga_carico(orm.Model):
    _name = "maga.carico"
    _description = "Carico articoli Magazzino"
    _columns = {
        'ric_id':fields.integer('indice Ricerca'),
        'cod_art': fields.many2one('maga.articolo', 'Articolo',required=True),
        'nome':fields.char('Nome', size=32,required=True,select=True),
        'num': fields.integer('Numero Pezzi'),
        'um': fields.char('Um', size=32),
        'qtb':fields.float('Quantità base',  digits=(14, 2)),
        'rqtb':fields.float('Rapporto',  digits=(14, 2)),
        'qeff':fields.integer('Pezzi Effettivi scaricati'),
        'anno':fields.integer('Anno',select=1),
        'mese':fields.integer('mese',select=1),
        'datab':fields.date("Data documento",readonly=False,select=True),
        'codb':fields.char('Codice DDT', size=32,required=True,select=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
    }
    _defaults = {
        'anno':lambda *a:int(datetime.date.today().strftime('%Y')),
        'mese':lambda *a:int(datetime.date.today().strftime('%m')),
        'datab':fields.date.context_today,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
    }
    def onchange_art(self,cr,uid,ids,cod_art,context=None):
        res={'value':{}}
        if cod_art:
            art=self.pool.get('maga.articolo').browse(cr,uid,cod_art)
            um=art.um
            qtb=art.qtb
            rqtb=art.rqtb
            nome=art.nome
            res= {'value':{'um':um.name, 'qtb':qtb,'rqtb':rqtb,'nome':nome}}
        return res

    def onchange_num(self,cr,uid,ids,num,context=None):
        cid=ids[0]
        l=self.browse(cr,uid,cid)
        qeff=(l.num*l.rqtb)*l.qtb
        return {'value':{'qeff':qeff,}}

    def Calc_tot(self, cr, uid, ids,context=None):
        cid=ids[0]
        if self.browse(cr,uid,cid).qeff<=0:
            maga=self.pool.get('maga.maga')
            l=self.browse(cr,uid,cid)
            #print l.comptenza.id
            self.competenza=l.comptenza
            art=self.pool.get('maga.articolo').browse(cr,uid,l.cod_art.id)
            self.giacagea=self.pool.get('maga.giacagea')
            rtagea=self.giacagea.search(cr,uid,[('codart','=',l.cod_art.name)])
            qeff=(l.num*l.rqtb)*l.qtb
            mese=int(datetime.date.today().strftime('%m'))
            anno=int(datetime.date.today().strftime('%Y'))
            artid=self.search(cr,uid,[('cod_art','=',l.cod_art.id),
                                      ('mese','=',mese),
                                      ('anno','=',anno)])
            tot=0

            giac=(art.qtatt+qeff)
            magaval={
                'codart':l.cod_art.id,
                'nomart':art.nome,
                'um': art.um.name,
                'for': art.tipof.name,
                'doccar': 'carico' ,
                'docscar':'no',
                'car':qeff,
                'scar':0,
                'giac': giac,
                'data':l.datab,
                'persone':0,
                'tipo':1,
                }
            if l.codb:
                magaval['doccar']=l.codb

            self.pool.get('maga.articolo').write(cr,uid,l.cod_art.id,{'qtatt':giac})
            maga.create(cr,uid,magaval)
            if rtagea==[]:
                giacagear=qeff
            else:
                agart=self.giacagea.browse(cr,uid,rtagea[0])
                giacagear=(agart.qtatt+qeff)
            if art.tipof.name=='AGEA':
                magavalag={'codart':l.cod_art.id,
                        'nomart':art.nome,
                        'um': art.um.name,
                        'doccar': l.codb ,
                        'docscar':'no',
                        'car':qeff,
                        'scar':0,
                        'giac': giacagear,
                        'data':l.datab,
                        'persone':0,
                        'tipo':1,
                        'idente':self.competenza.id,
                        'comptenza':self.competenza.id}
                self.pool.get('maga.agea').create(cr,uid,magavalag)
                if rtagea==[]:
                    self.giacagea.create(cr,uid,{'codart':art.name,'data':l.datab,'qtatt':giacagear,'idente':self.competenza.id,
                        'comptenza':self.competenza.id})
                else:
                    self.giacagea.write(cr,uid,rtagea[0],{'qtatt':giacagear})
            return self.write(cr,uid,cid,{'qeff':qeff,'um':l.um})
        else:
            return 1
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_carico, self).search(cr, uid, new_args, offset, limit, order, context, count)

    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_carico, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)



class maga_scarico(orm.Model):
    _name = "maga.scarico"
    _description = "Tabella di scarico Magazzino"
    _columns = {
        'id_borsa':fields.integer('Borsa'),
        'id_scarico':fields.integer('Scarico'),
        'cod_art':fields.many2one('maga.articolo', 'Articolo',required=True),
        'nome':fields.char('Nome', size=32,required=True,select=True),
        'num': fields.integer('Numero Pezzi'),
        'um': fields.char('Nome', size=32,readonly=True),
        'qtb':fields.float('Quantità base',  digits=(14, 2)),
        'rqtb':fields.float('Rapporto',  digits=(14, 2)),
        'qeff':fields.integer('Pezzi Effettivi scaricati'),
        'stat':fields.selection([(0,'In Scarico'),
                                (1,'Scaricato')],'Stato'),
        'data':fields.date('Data inserimento'),
        'giorno':fields.integer('Giorno'),
        'anno':fields.integer('Anno'),
        'mese':fields.integer('mese'),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
    }
    _defaults = {
        'num':1,
        'stat':0,
        'data':fields.date.context_today,
        'giorno':lambda *a:int(datetime.date.today().strftime('%d')),
        'anno':lambda *a:int(datetime.date.today().strftime('%Y')),
        'mese':lambda *a:int(datetime.date.today().strftime('%m')),
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        #  'Opinsf':_ut_ente
        }
    def onchange_art(self,cr,uid,ids,cod_art,context=None):
        res={'value':{}}
        #print cod_art
        if cod_art:
            art=self.pool.get('maga.articolo').browse(cr,uid,cod_art)
            um=art.um
            qtb=art.qtb
            rqtb=art.rqtb
            nome=art.nome
            res= {'value':{'um':um.name, 'qtb':qtb,'rqtb':rqtb,'nome':nome}}
        return res

    def onchange_num(self,cr,uid,ids,num,context=None):
        cid=ids[0]
        l=self.browse(cr,uid,cid)
        qeff=(l.num*l.rqtb)*l.qtb
        return {'value':{'qeff':qeff,}}

    def Calc_tot(self, cr, uid, ids,context=None):
        cid=ids[0]
        maga=self.pool.get('maga.maga')
        l=self.browse(cr,uid,cid)
        art=self.pool.get('maga.articolo').browse(cr,uid,l.cod_art.id)
        l.um=art.um.name
        l.qtb=art.qtb
        l.rqtb=art.rqtb
        l.nome=art.nome
        qeff=(l.num*l.rqtb)*l.qtb

        artid=self.search(cr,uid,[('cod_art','=',l.cod_art.id),
                                  ('stat','=',1),
                                  ('mese','=',int(datetime.date.today().strftime('%m'))),
                                  ('anno','=',int(datetime.date.today().strftime('%Y')))])

        self.write(cr,uid,cid,{'qeff':qeff,'um':l.um})
        giac=(art.qtatt-qeff)
        magaval={
        'codart':l.cod_art.id,
        'nomart':art.nome,
        'um': art.um.name,
        'for': art.tipof.name,
        'doccar': 'no' ,
        'docscar':'scarico',
        'car':0,
        'scar':qeff,
        'giac': giac,
        'data':l.data,
        'persone':0,
        'tipo':2,
        }
        if l.id_borsa:
            magaval['docscar']=self.pool.get('cborse.borsa').browse(cr,uid,l.id_borsa).cod_doc

        self.pool.get('maga.articolo').write(cr,uid,l.cod_art.id,{'qtatt':giac})
        maga.create(cr,uid,magaval)

    def aggiorna(self,cr, uid, context=None):
        #print 'start agg maga DA ELIMINARE'
        ids=self.search(cr,uid,[('nome','!=',' ')])
        func=self.pool.get('cborse.func')
        for idt in ids:
            s=self.browse(cr,uid,idt)
            dts='%d-%d-%d' % (s.anno,s.mese,s.giorno)
            #print dts
            dtr=func.gen_data(dts)
            self.write(cr,uid,idt,{'data':dtr.strftime('%d-%m-%Y')})
        return {}
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #print 'ovverride Search scarico'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            else:
                new_args += [arg]
        #print new_args
        return super(maga_scarico, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Scarico'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_scarico, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
maga_scarico()

class maga_maga(orm.Model):
    _name = "maga.maga"
    _description = "Tabella di scarico Magazzino"
    _columns = {
        'codart':fields.integer('Codice Articolo',readonly=True),
        'nomart':fields.char('Nome Articolo', size=32,required=True,select=True),
        'um': fields.char('UM', size=32,readonly=True),
        'for': fields.char('Fornitore', size=32,readonly=True,select=True),
        'doccar': fields.char('Documento di carico', size=32,readonly=True,select=True),
        'docscar': fields.char('Documento di scarico', size=32,readonly=True,select=True),
        'docriep': fields.char('Documento giornaliero', size=32,readonly=True,select=True),
        'car':fields.integer('qt caricate',readonly=True),
        'scar':fields.integer('qt scaricate',readonly=True),
        'giac':fields.integer('Giaceza',readonly=True),
        'data':fields.date('Data inserimento',readonly=True,select=True),
        'persone':fields.integer('Numero Persone',readonly=True),
        'tipo': fields.selection(
            [(1, 'Carico'),(2, 'Scarico')],
            'Timo movimento',readonly=True,select=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
    }
    _defaults = {
        'doccar': 'no',
        'docscar': 'no',
        'car':0,
        'scar':0,
        'persone':0,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def autoaggiorna(self,cr,uid,context=None):
        #print 'inizio generazione movimenti maga ora %s' % fields.datetime.now()
        ids=self.pool.get('maga.articolo').search(cr,uid,[('name','!=',' ')])
        qatt=0
        for idt in ids:
            self.carico=0
            self.scarico=0
            self.giacenza=0
            self.seachdat={}
            ##print idt
            idarticolic=self.pool.get('maga.carico').search(cr,uid,[('cod_art','=',idt)])

            art=self.pool.get('maga.articolo').browse(cr,uid,idt)
            numcar=idarticolic.__len__()
            contalist=1
            #controllo il carico per aticolo
            #print 'articolo %s numero carichi %d' % (idt,numcar)
            for idartc in idarticolic:
                l=self.pool.get('maga.carico').browse(cr,uid,idartc)
                #print 'conta lista % d' % contalist
                #print idarticolic
                if contalist<numcar:
                    l2=self.pool.get('maga.carico').browse(cr,uid,idarticolic[contalist])
                    idarticolisc=self.pool.get('maga.scarico').search(cr,uid,[('cod_art','=',idt),('data','>=',l.datab),('data','<',l2.datab)])
                else:
                    idarticolisc=self.pool.get('maga.scarico').search(cr,uid,[('cod_art','=',idt),('data','>=',l.datab)])
                #carico l'articolo
                self.giacenza+=l.qeff
                magaval={
                        'codart':l.cod_art.id,
                        'nomart':art.nome,
                        'um': art.um.name,
                        'for': art.tipof.name,
                        'doccar': 'carico' ,
                        'docscar':'no',
                        'car':l.qeff,
                        'scar':0,
                        'giac': self.giacenza,
                        'data':l.datab,
                        'persone':0,
                        'tipo':1}
                if l.codb:
                    magaval['doccar']=l.codb

                self.create(cr,uid,magaval)
                #carico gli scarichi seguenti la data di carico
                for idartsc in idarticolisc:
                    ls=self.pool.get('maga.scarico').browse(cr,uid,idartsc)
                    self.giacenza-=ls.qeff
                    magaval={'codart':ls.cod_art.id,
                            'nomart':art.nome,
                            'um': art.um.name,
                            'for': art.tipof.name,
                            'doccar': 'no' ,
                            'docscar':'scarico',
                            'car':0,
                            'scar':ls.qeff,
                            'giac': self.giacenza,
                            'data':ls.data,
                            'persone':0,
                            'tipo':2}
                    if ls.id_borsa:
                        magaval['docscar']=self.pool.get('cborse.borsa').browse(cr,uid,ls.id_borsa).cod_doc
                    self.create(cr,uid,magaval)

                contalist+=1

            ##print 'carico %d scarico %d giacenza %d' % (carico,scarico,giacenza)
            self.pool.get('maga.articolo').write(cr,uid,idt,{'qtatt':self.giacenza})
        #print 'fine generazione movimenti maga ora %s' % fields.datetime.now()
        return {}
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #~ #print new_args
        return super(maga_maga, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_maga, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
    _order = 'data asc,car asc,giac desc'

class maga_visual(orm.Model):
    _name = "maga.vis"
    _description = "tipo fornitore"
    _columns = {
        'tipof':fields.many2one('maga.tipofor', 'Tipo Fornitore',required=True,select=True),
        'art':fields.one2many('maga.articolo','tipof','Lista',required=True),
        'description': fields.text('Descrizione'),
    }


class maga_scarica(orm.Model):
    _name = "maga.scarica"
    _description = "Scarica articoli da magazzino con causali"
    _columns = {
        'causa': fields.selection(
            [(0, 'Scaduto'),(1, 'Rotto'),(2,'Altro')],
            'Causale',required=True),
        'riga_s':fields.one2many('maga.scarico','id_scarico','Lista',required=True),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'stat':fields.boolean('Stato'),
        'opinsft':fields.char('Operatore', size=32),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }
    _defaults = {
        'stat':True,
        'dtini':fields.date.context_today,
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }

    def valida(self,cr,uid,ids,context=None):
        cid=ids[0]
        scar=self.browse(cr, uid, cid)
        idlinee=self.pool.get('maga.scarico').search(cr, uid, [('id_scarico','=',cid)])
        linee=self.pool.get('maga.scarico').browse(cr, uid, idlinee)
        for linea in linee:
            if not linea.stat:
                self.pool.get('maga.scarico').write(cr,uid,linea.id,{'stat':1})
                idd=[]
                idd.append(linea.id)
                self.pool.get('maga.scarico').Calc_tot(cr,uid,idd)
        self.write(cr,uid,cid,{'stat':False})
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_scarica, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_scarica, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
maga_scarica()
#tabella ultimi aggiornamenti
class maga_riepgiorconf(orm.Model):
    _name = "maga.updtriepgior"
    _description = "last update Riepilogo Agea"
    _columns = {
        'numdoc':fields.integer('doc num'),
        'lastupd':fields.date('Ultimo Agg',readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
    }
    _defaults = {
        'numdoc':0,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_riepgiorconf, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_riepgiorconf, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)

#tabella di stampa riepiloghi giornalieri
class maga_lisstadtdoc(orm.Model):
    _name = "maga.listadoc"
    _description = "last update Riepilogo Agea"
    _columns = {
        'numdoc':fields.one2many('maga.riepgior','ric_id','doc num',readonly=True),
        'data':fields.date('data',readonly=True,select=True),
        'cod_doc':fields.char('indice Ricerca',size=64,readonly=True,select=True),
        'persgior':fields.integer('Persone giornaliere'),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
    }
    _defaults = {
        'numdoc':0,
        'persgior':0,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_lisstadtdoc, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_lisstadtdoc, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)

#tabella riepiloghi scarichi giornalieri agea
class maga_riepgior(orm.Model):
    _name = "maga.riepgior"
    _description = "Riepilogo Agea"
    _columns = {
        'ric_id':fields.integer('indice Ricerca'),
        'cod_doc':fields.char('indice Ricerca',size=64,readonly=True),
        'data':fields.date('Giorno consegna'),
        'id_art':fields.char('Articolo',size=128,readonly=True),
        'codart':fields.char('codice Articolo',size=128,readonly=True),
        'um':fields.char('unita di misura',size=128,readonly=True),
        'persone':fields.integer('numero persone'),
        'qt':fields.integer('Quantita'),
        'numborse':fields.integer('Numero Borse'),
        'lastupd':fields.date('Giorno consegna',readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }
    _default={
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def genera(self,cr,uid,context=None):
        #~ seleziono fornitore
        giacagea=self.pool.get('maga.giacagea')
        func=self.pool.get('cborse.func')
        indig=self.pool.get('cbase.indigente')
        borse=self.pool.get('cborse.borsa')
        self.listdoc=self.pool.get('maga.listadoc')
        self.scarico=self.pool.get('maga.scarico')
        self.updateg=self.pool.get('maga.updtriepgior')
        self.idcompetenze=self.pool.get('cbase.enti').search(cr,uid,[('esterno','=',False),('viveri','=',True)])
        self.competenza=0
        self.oggi=datetime.date.today()
        fornitore=self.pool.get('maga.tipofor').search(cr,uid,[('agea','=',True)])
        #cerco per ente di competenza
        #~ cerco gli articoli del fornitore
        #print 'generazione movimenti e magazzino Agea per:'
        for self.competenza in self.idcompetenze:
            #print 'Comptetenza: %d' % self.competenza
            self.idsArt=self.pool.get('maga.articolo').search(cr,uid,[('comptenza','=',self.competenza),('tipof','=',fornitore[0])])
            #~ per ogni articolo del fornitore
            self.artdic={}
            self.doc=self.search(cr, uid, [('comptenza','=',self.competenza),('cod_doc','<>',False)])
            self.idupdt=self.updateg.search(cr,uid,[('comptenza','=',self.competenza),('numdoc','>',0)])
            self.iddatsscar=[]
            if (self.idupdt<>[]):
                self.dtupd=self.updateg.browse(cr,uid,self.idupdt[0]).lastupd
                #da mettere controllo id scarico
                self.iddatsscar=self.scarico.search(cr,uid,[('comptenza','=',self.competenza),('data','>',self.dtupd)])

            if (self.iddatsscar<>[]) and (func.ctl_scad(self.dtupd)=='scad'):
                self.maxdoc=self.updateg.browse(cr,uid,self.idupdt[0]).numdoc
                ##print self.iddatsscar
                #print 'Last num doc: %d ' % self.maxdoc
                self.codedoc=''
                self.datadic={}
                #per ogni indice della data di scarico valutata
                for j in self.iddatsscar:
                    #prendo il campo
                    artes=self.scarico.browse(cr, uid, j)
                    #~ salvo la data
                    k=artes.data
                    #se non è una data che ho già valutato
                    docdict={'DOC':'','Elenco':{}}
                    if self.datadic.has_key(k)==False:
                        #faccio codice documento
                        self.codedoc='D%d' % self.maxdoc
                        docdict['DOC']=self.codedoc
                        #per ogni articolo nel sistema che appartiene al fornitore
                        articdic={}
                        codarticdic={}
                        persdic={}
                        artdic={}
                        umdic={}
                        listmaxp=[]
                        self.ric=self.listdoc.create(cr,uid,{'cod_doc':self.codedoc,'data':k})
                        for i in self.idsArt:
                            #preparo la data che uso per salvataggio
                            dt=k
                            #cerco le quantità per ogni articolo nel magazzino delle giacenze Agea
                            codiceaert=self.pool.get('maga.articolo').browse(cr,uid,i).name
                            rtagea=giacagea.search(cr,uid,[('comptenza','=',self.competenza),('codart','=',codiceaert)])
                            #carico la giacenza attuale
                            #~ cerco per articolo per data
                            idsscar=self.scarico.search(cr,uid,[('comptenza','=',self.competenza),('cod_art','=',i),('data','=',artes.data)])
                            #print idsscar
                            #per ogni riga trovata faccio la somma delle quantità
                            numpers=0
                            if idsscar:
                                self.giacenza=giacagea.browse(cr,uid,rtagea[0]).qtatt
                                for z in idsscar:
                                    artesdata=self.scarico.browse(cr, uid, z)
                                    idborsa=artesdata.id_borsa
                                    if borse.exists(cr, uid, idborsa):
                                        #seleziono l'indice dell' indigente dalle borse ma prima dovrei controllare
                                        #se esiste questo id
                                        indig_id= borse.browse(cr,uid,idborsa).ind_id.id
                                        ##print indig_id
                                        #carico il numero dei famigliari che prendono le borse  + l'intestatario
                                        idfam=self.pool.get('cbase.famiglia').search(cr,uid,[('ind_id','=',indig_id)])
                                        numpers=1
                                        for iidfam in idfam:
                                            if self.pool.get('cbase.famiglia').browse(cr,uid,iidfam).pborsa:
                                                numpers+=1
                                        #todo--> ora dovrei sommare per ogni documento il numero delle persone
                                        #totali per poi stamparle sul foglio agea

                                    #salvo le quantita per ogni articolo
                                    if articdic.has_key(i):
                                        articdic[i]+=artesdata.qeff
                                        persdic[i]+=numpers
                                    else:
                                        articdic[i]=artesdata.qeff
                                        persdic[i]=numpers
                                        artdic[i]=artesdata.nome
                                        codarticdic[i]=artesdata.cod_art.id
                                        umdic[i]=self.pool.get('maga.articolo').browse(cr,uid,i).um.name
                                listmaxp.append(persdic[i])
                                self.create(cr,uid,{'ric_id':0,
                                                    'cod_doc':self.codedoc,
                                                    'id_art':artdic[i],
                                                    'codart':codarticdic[i],
                                                    'um':umdic[i],
                                                    'data':dt,
                                                    'qt': articdic[i],
                                                    'persone':persdic[i],
                                                    'numborse':0,
                                                    'ric_id':self.ric,
                                                    'idente':self.competenza,
                                                    'comptenza':self.competenza  })
                                self.giacenza-=articdic[i]
                                magaval={'codart':codarticdic[i],
                                        'nomart':artdic[i],
                                        'um': umdic[i],
                                        'doccar': 'no' ,
                                        'docscar':self.codedoc,
                                        'car':0,
                                        'scar':articdic[i],
                                        'giac':self.giacenza,
                                        'data':dt,
                                        'persone':persdic[i],
                                        'tipo':2,
                                        'idente':self.competenza,
                                        'comptenza':self.competenza }
                                self.pool.get('maga.agea').create(cr,uid,magaval)
                                giacagea.write(cr,uid,rtagea[0],{'qtatt':self.giacenza,
                                                                 'idente':self.competenza,
                                                                 'comptenza':self.competenza})
                                docdict['Elenco']=articdic
                        self.datadic[k]=docdict
                        if articdic=={}:
                            self.listdoc.unlink(cr,uid,[self.ric])
                        else:
                            self.listdoc.write(cr,uid,[self.ric],{'persgior':max(listmaxp),
                                                                  'idente':self.competenza,
                                                                  'comptenza':self.competenza})
                            self.maxdoc+=1
                self.updateg.write(cr,uid,self.idupdt[0],{'lastupd':self.oggi.strftime('%d-%m-%Y'),
                                                       'numdoc':self.maxdoc,
                                                       'idente':self.competenza,
                                                       'comptenza':self.competenza})

        return {}
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_riepgior, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_riepgior, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
    _order = 'lastupd desc'



class maga_giacagea(orm.Model):
    _name = "maga.giacagea"
    _description = "tabella base delle giacenze Agea"
    _columns = {
        'codart': fields.char('Articolo', size=32,select=True),
        'data':fields.date('Data inserimento',readonly=True,select=True),
        'qtatt':fields.float( 'Giacenza',readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
    }
    _default={
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_giacagea, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_giacagea, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
maga_giacagea()

class maga_agea(orm.Model):
    _name = "maga.agea"
    _description = "Magazzino Agea"
    _rec_name="nomart"
    _columns = {
        'ric_id':fields.integer('indice Ricerca'),
        'codart':fields.integer('Codice Articolo',readonly=True),
        'nomart':fields.char('Nome Articolo', size=32,required=True,select=True),
        'um': fields.char('UM', size=32,readonly=True),
        'for': fields.char('Fornitore', size=32,readonly=True,select=True),
        'doccar': fields.char('Documento di carico', size=32,readonly=True,select=True),
        'docscar': fields.char('Documento di scarico', size=32,readonly=True,select=True),
        'car':fields.integer('qt caricate',readonly=True),
        'scar':fields.integer('qt scaricate',readonly=True),
        'giac':fields.integer('Giaceza',readonly=True),
        'data':fields.date('Data inserimento',readonly=True,select=True),
        'persone':fields.integer('Numero Persone',readonly=True),
        'tipo': fields.selection(
            [(1, 'Carico'),(2, 'Scarico')],
            'Timo movimento',readonly=True,select=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        }
    _defaults = {
        'doccar': 'no',
        'docscar': 'no',
        'for':'AGEA',
        'car':0,
        'scar':0,
        'persone':0,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        }
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_agea, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        #print domain
        #print fields
        #print groupby
        #print context
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_agea, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
    _order = 'data asc,car asc,giac desc'
maga_agea()

class maga_ricageareport(orm.Model):
    _name = "maga.ricageareport"
    _description = "Pagine report Agea"
    _columns = {
        'ric_id':fields.integer('indice Ricerca'),
        'pag_id':fields.many2one('maga.pagreport','pagina',select=True),
        'data':fields.date('data'),
        'doc': fields.char('Documento', size=32),
        'um1':fields.char('um', size=32),
        'car1':fields.integer('carico'),
        'scar1':fields.integer('scarico'),
        'giac1':fields.integer('Giaceza'),
        'um2':fields.char('um', size=32),
        'car2':fields.integer('carico',readonly=True),
        'scar2':fields.integer('scarico',readonly=True),
        'giac2':fields.integer('Giaceza',readonly=True),
        'um3':fields.char('um', size=32),
        'car3':fields.integer('carico',readonly=True),
        'scar3':fields.integer('scarico',readonly=True),
        'giac3':fields.integer('Giaceza',readonly=True),
        'um4':fields.char('um', size=32),
        'car4':fields.integer('carico',readonly=True),
        'scar4':fields.integer('scarico',readonly=True),
        'giac4':fields.integer('Giaceza',readonly=True),
        'tipo': fields.selection(
            [(1, 'Carico'),(2, 'Scarico')],
            'Timo movimento',readonly=True,select=True),
        'opinsft':fields.char('Operatore', size=32),
        'persone':fields.integer('Numero Persone',readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento"),
        }
    _defaults = {
            'dtin':fields.date.context_today,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_ricageareport, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        return super(maga_ricageareport, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
    _order = 'data,tipo desc'


class maga_pagreport(orm.Model):
    _name = "maga.pagreport"
    _description = "Pagine report Agea"
    _columns = {
        'report_id':fields.many2one('maga.ageareport','Report',select=True),
        'name':fields.integer('Pagina'),
        'mpp':fields.integer('mpp'),
        'r1':fields.integer('rart1'),
        'r2':fields.integer('rart2'),
        'r3':fields.integer('rart3'),
        'r4':fields.integer('rart4'),
        'l1':fields.integer('lart1'),
        'l2':fields.integer('lart2'),
        'l3':fields.integer('lart3'),
        'l4':fields.integer('lart4'),
        'lineart':fields.one2many('maga.ricageareport','pag_id', string='Lista'),
        'opinsft':fields.char('Operatore', size=32),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento"),
        }
    _defaults = {
            'r1':0,
            'r2':0,
            'r3':0,
            'r4':0,
            'l1':0,
            'l2':0,
            'l3':0,
            'l4':0,
            'mpp':20,
            'dtin':fields.date.context_today,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            }



    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False, xtra=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_pagreport, self).search(cr, uid, new_args, offset, limit, order, context, count)

    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        return super(maga_pagreport, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
    _order = 'name'


class maga_ageareport(orm.Model):
    _name = "maga.ageareport"
    _description = "Report Agea"
    _columns = {
        'name':fields.char('Cod Ricerca', size=32,select=True),
        'art1':fields.many2one('maga.articolo', 'Articolo',required=True),
        'nomart1':fields.char('Nome Articolo', size=32,readonly=True),
        'um1':fields.char('umis', size=32,readonly=True),
        'art2':fields.many2one('maga.articolo', 'Articolo'),
        'nomart2':fields.char('Nome Articolo', size=32,readonly=True),
        'um2':fields.char('umis', size=32,readonly=True),
        'art3':fields.many2one('maga.articolo', 'Articolo'),
        'nomart3':fields.char('Nome Articolo', size=32,readonly=True),
        'um3':fields.char('umis', size=32,readonly=True),
        'art4':fields.many2one('maga.articolo', 'Articolo'),
        'nomart4':fields.char('Nome Articolo', size=32,readonly=True),
        'um4':fields.char('umis', size=32,readonly=True),
        'lineart':fields.one2many('maga.ricageareport','ric_id', string='Lista'),
        'page_ids':fields.one2many('maga.pagreport','report_id', string='Psgine'),
        'ddata':fields.date("dal"),
        'adata':fields.date("al"),
        'numeroreg':fields.integer('Numero Registro'),
        'opinsft':fields.char('Operatore', size=32),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        'dtin': fields.datetime("Data inserimento",readonlycborse=True),
        }
    _defaults = {
            'numeroreg':0,
            'ddata':fields.date.context_today,
            'adata':fields.date.context_today,
            'dtin':fields.date.context_today,
            'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
            'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
            }

    def genera_dati(self,cr,uid,ids,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        ric=self.browse(cr, uid, ids[0])
        ddata=ric.ddata
        adata=ric.adata
        linesrepo=self.pool.get('maga.ricageareport')
        pageobj=self.pool.get('maga.pagreport')
        daelm=linesrepo.search(cr,uid,[('ric_id','=',ids[0])])
        linesrepo.unlink(cr,uid,daelm)
        pdel=pageobj.search(cr,uid,[('report_id','=',ids[0])])
        pageobj.unlink(cr,uid,pdel)
        res={'value':{}}
        if ddata<adata:
            listart=[]
            #print ric.art1
            if ric.art1:
                listart.append(ric.art1.id)
            #print ric.art2
            if ric.art2:
                listart.append(ric.art2.id)
            #print ric.art3
            if ric.art3:
                listart.append(ric.art3.id)
            #print ric.art4
            if ric.art4:
                listart.append(ric.art4.id)
            #print listart
            if listart <> []:
                localdomain=[('comptenza','=',user.id_ente.id),
                            ('data','>=',ddata),('data','<=',adata),
                            ('codart','in',listart)]
                dictc={}
                cont=1
                for a in listart:
                    dictc[a]=listart.index(a)+1
                #print dictc
                arts=self.pool.get('maga.agea').search(cr,uid,localdomain)
                val1=self.pool.get('maga.articolo').browse(cr, uid, ric.art1.id).nome
                um1=self.pool.get('maga.articolo').browse(cr, uid, ric.art1.id).um.name
                val2=self.pool.get('maga.articolo').browse(cr, uid, ric.art2.id).nome
                um2=self.pool.get('maga.articolo').browse(cr, uid, ric.art2.id).um.name
                val3=self.pool.get('maga.articolo').browse(cr, uid, ric.art3.id).nome
                um3=self.pool.get('maga.articolo').browse(cr, uid, ric.art3.id).um.name
                val4=self.pool.get('maga.articolo').browse(cr, uid, ric.art4.id).nome
                um4=self.pool.get('maga.articolo').browse(cr, uid, ric.art4.id).um.name
                self.write(cr,uid,ids[0],{'nomart1':val1,
                                          'um1':um1,
                                          'nomart2':val2,
                                          'um2':um2,
                                          'nomart3':val3,
                                          'um3':um3,
                                          'nomart4':val4,
                                          'um4':um4
                                          })
                objects=self.pool.get('maga.agea').browse(cr,uid,arts)
                for obj in objects:
                    rets=linesrepo.search(cr,uid,[('data','=',obj.data),('tipo','=',obj.tipo)])
                    if len(rets)>0:
                        line=linesrepo.read(cr,uid,rets)[0]
                        line['comptenza']=user.id_ente.id
                        line['territorio']=user.id_ente.territorio.id
                        line['idente']=user.id_ente.id
                    else:
                        line={
                              'um1':'','car1':0,'scar1':0,'giac1':0,
                              'um2':'','car2':0,'scar2':0,'giac2':0,
                              'um3':'','car3':0,'scar3':0,'giac3':0,
                              'um4':'','car4':0,'scar4':0,'giac4':0,
                              'pag_id':0
                              }
                    if dictc[obj.codart]==1:
                        line['um1']=obj.um
                        line['car1']=obj.car
                        line['scar1']=obj.scar
                        line['giac1']=obj.giac
                    if dictc[obj.codart]==2:
                        line['um2']=obj.um
                        line['car2']=obj.car
                        line['scar2']=obj.scar
                        line['giac2']=obj.giac
                    if dictc[obj.codart]==3:
                        line['um3']=obj.um
                        line['car3']=obj.car
                        line['scar3']=obj.scar
                        line['giac3']=obj.giac
                    if dictc[obj.codart]==4:
                        line['um4']=obj.um
                        line['car4']=obj.car
                        line['scar4']=obj.scar
                        line['giac4']=obj.giac
                    if len(rets)<=0:
                        line['data']=obj.data
                        line['ric_id']=ids[0]
                        line['persone']=obj.persone
                        line['tipo']=obj.tipo
                        if obj.doccar != 'no':
                            line['doc']=obj.doccar
                        elif obj.docscar != 'no':
                            line['doc']=obj.docscar
                        linesrepo.create(cr,uid,line)
                    else:
                        line['ric_id']=ids[0]
                        line['pag_id']=0
                        line['persone']=obj.persone
                        if obj.doccar!='no':
                            line['doc']=obj.doccar
                        elif obj.docscar!='no':
                            line['doc']=obj.docscar
                        linesrepo.write(cr,uid,rets,line)

            rows=linesrepo.search(cr,uid,[('ric_id','=',ids[0])])
            nrows=len(rets)
            pages=int(math.ceil(float(nrows)/21.0))
            rowcount=1
            pagecount=1
            r1=0
            r2=0
            r3=0
            r4=0
            for i in rows:
                if rowcount == 1:
                    if pagecount==1:
                        idpage=pageobj.create(cr,uid,{'name':pagecount,'report_id':ids[0]})
                    else:
                        idpage=pageobj.create(cr,uid,{
                                                    'report_id':ids[0],
                                                    'name':pagecount,
                                                    'r1':r1,
                                                    'r2':r2,
                                                    'r3':r3,
                                                    'r4':r4
                                                    })
                        r1=0
                        r2=0
                        r3=0
                        r4=0
                linesrepo.write(cr,uid,i,{'pag_id':idpage})
                blrepo=linesrepo.browse(cr,uid,i)
                if blrepo.giac1 != 0:
                    r1=blrepo.giac1
                elif (blrepo.scar1 > 0) and (blrepo.giac1 == 0):
                    r1=blrepo.giac1
                if blrepo.giac2 != 0:
                    r2=blrepo.giac2
                elif (blrepo.scar2 > 0) and (blrepo.giac2 == 0):
                    r2=blrepo.giac2
                if blrepo.giac3 != 0:
                    r3=blrepo.giac3
                elif (blrepo.scar3 > 0) and (blrepo.giac3 == 0):
                    r3=blrepo.giac3
                if blrepo.giac4 != 0:
                    r4=blrepo.giac4
                elif (blrepo.scar4 > 0) and (blrepo.giac4 == 0):
                    r4=blrepo.giac4
                rowcount+=1
                if rowcount > 21:
                    pageobj.write(cr,uid,idpage,{
                                        'name':pagecount,
                                        'l1':r1,
                                        'l2':r2,
                                        'l3':r3,
                                        'l4':r4
                                        })
                    rowcount=1
                    pagecount+=1
            if rowcount > 1 and rowcount < 21:
                    pageobj.write(cr,uid,idpage,{
                                        'name':pagecount,
                                        'l1':r1,
                                        'l2':r2,
                                        'l3':r3,
                                        'l4':r4
                                        })
        return True
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_args = []
        #~ #print 'ovverride Search'
        #print args
        for arg in args:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_args += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_args += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_args += [arg]
        #~ #print new_args
        return super(maga_ageareport, self).search(cr, uid, new_args, offset, limit, order, context, count)
    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        #print 'ovverride Group Articoli'
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        new_domain =[]
        for arg in domain:
            if (type(arg) is not tuple):
                if (type(arg) is not list):
                    new_args += arg
                    continue
            #~ #print arg[2]
            if arg[2] == 'USER_COMPETENZA':
                new_domain += [(arg[0], arg[1], user.id_ente.id)]
            elif arg[2] == 'USER_PAYMENT_MODE_IDS':
                new_domain += [(arg[0], arg[1], [m.id for m in user.payment_mode_ids])]
            else:
                new_domain += [arg]
        #print 'domain gruppo articoli %s' % new_domain
        return super(maga_ageareport, self).read_group(cr, uid, new_domain,fields,groupby,offset,limit, context,orderby)
    _order = 'numeroreg asc'
maga_ageareport()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


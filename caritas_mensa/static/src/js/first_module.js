
openerp.caritas_mensa = function (instance){

    //var module = instance.caritas_mensa;
    instance.caritas_mensa = {};
    var module = instance.caritas_mensa;
    var _t = instance.web._t;

    datas_Model(instance, module);
    my_basewidget(instance, module);
    persone_w(instance, module);
    dates_w(instance, module);
    screen_w(instance, module);

    instance.web.client_actions.add('mensa', 'instance.caritas_mensa.ScreenW');
};

function persone_w(instance, module){
    var QWeb = instance.web.qweb;
    module.PersonW = module.BBW.extend({
        template: 'mensa_prenot',
        init: function (parent, action) {
            this._super(parent,action);
            this.prenotwidgets = [];
            this.click_persona_action = action.click_persona_action;
            this.objs=action.objects;
        },
        start: function(){


        },
        renderElement:function(){
            this._super();
            var self = this;
            for(var i = 0, len = this.objs.length; i < len; i++){
                self.objs[i]['ora']=toTime(self.objs[i].dtcons.split(" ")[1],"h:m");
            }
            this.objs.sortBy('ora');
            for(var i = 0, len = this.objs.length; i < len; i++){
                var obj=self.objs[i];
                var ora=obj.dtcons.split(" ")[1];
                console.log('object ',obj);
                var buttone = QWeb.render('mensa_prenot',{id:obj.id,nome:obj.nome,ora:ora,ind:obj.indig_code});
                buttone = _.str.trim(buttone);
                var buttone= $(buttone);
                buttone.bind('click', function(event){

                        if(self.click_persona_action){
                            self.click_persona_action(event);
                        }

                });
                this.prenotwidgets.push(buttone);
            }

        },
    });
};
function dates_w(instance, module){
    var QWeb = instance.web.qweb;
   // var calend = new instance.web.Model('cborse.calend');
    module.DatesW = module.BBW.extend({
        template: 'mensa_calendar',
        init: function (parent, action) {
            this._super(parent,action);
            this.grouped=[];
            this.load=false;
            this.datewidgets = [];
            this.person_w =[];
            this.pool=new module.Pool({});
            this.click_date_action = action.click_date_action;
            this.click_persona_action = action.click_persona_action;

        },
        get_data: function(pnode){
            var self = this;
            self.grouped=[];
            var dates=new Date();
            var dates2=new Date();
            dates2.setDate(dates.getDate()+5);
            var datei=instance.web.datetime_to_str(dates);
            var datef=instance.web.datetime_to_str(dates2);
            var filtrod=[['dtconsv','>=',datei],
                     ['dtconsv', '<=', datef],['stato','in',['post','pian']]];
            this.pool.ready.done(function(){
                self.renderElement(pnode);
            });
            self.pool.get_data('cborse.calend',['nome','indig_code','dtconsv','dtcons'],
                                filtrod,[],'dtconsv','dtcons');


        },

        renderElement:function(pnode){
            this._super();
            var self = this;
            this.grouped=self.pool.loaded_data.groupBy('dtconsv');
            var val='';
            for(var i = 0, len = this.grouped.length; i < len; i++){
                var gruppo=this.grouped[i];
                var oggi=new Date();
                oggi.setHours(0,0,0,0);
                var cdate=instance.web.str_to_date(gruppo.key);
                if (oggi-cdate===0){
                    val='Oggi';
                }else{
                    val=cdate.format("d/m/Y");
                }
                var button = QWeb.render('mensa_calendar',{id:gruppo.key,val:val});
                button = _.str.trim(button);
                var button = $(button);
                button.click(function(event){
                        if(self.click_date_action){
                            self.click_date_action(event);
                        }
                });
                this.datewidgets.push(button);
                button.appendTo(pnode)
            }
        },

        getPrenot:function(action){
            for(var i = 0, len = this.person_w.length; i < len; i++){
                this.person_w[i].destroy();
            }
            for(var i = 0, len = this.grouped.length; i < len; i++){
                var gruppo=this.grouped[i];
                this.person_w[gruppo.key]  = new module.PersonW(self,{
                    objects:gruppo.values,
                    click_persona_action:action.click_persona_action,
                });
                this.person_w[gruppo.key].renderElement();
            }
        }
    });
};
function screen_w(instance, module){
    var QWeb = instance.web.qweb;
    module.ScreenW = module.BBW.extend({
        template: 'borsewidget',
        init: function (parent, action) {
            this._super(parent,action);
            this.state=1;

        },
        start: function(){
            // instance.web.blockUI();
            var self = this;
            this.dates_w  = new module.DatesW(this,{
                click_date_action: function(data){
                    if(data){
                        self.data_click(data);
                    }
                },
                click_persona_action: function(persona){
                    if(persona){
                        self.persona_click(persona);
                    }
                },
            });
            this.dates_w.get_data($('#widgetdate_prenot'));
        },
        data_click: function(id){
            var self = this;
            action={
                click_persona_action: function(persona){
                    if(persona){
                        self.persona_click(persona);
                    }
                },
            };
            this.dates_w.getPrenot(action);
            console.log('cliccato il ',this.dates_w.person_w[$(id.target).data('date')].prenotwidgets);
            $('#widgetdate_prenot > li').each(function () {
                $(this).css('background-color', "white");
            });
            $(id.target).closest("li").css('background-color', "yellow");
            $('#widgetprenot').empty();
            $('#widgetprenot').append(this.dates_w.person_w[$(id.target).data('date')].prenotwidgets );

        },
        persona_click: function(id){
            console.log('cliccato la persona ', $(id.target).closest("li").data('prenot'));
            $('#widgetprenot > li').each(function () {
                $(this).css('background-color', "white");
            });
            $(id.target).closest("li").css('background-color', "orange");
        },
        renderElement: function(){
            this._super();

        },
    });
}



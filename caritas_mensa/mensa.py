# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import orm, fields
from tools.translate import _
import datetime
from datetime import datetime,timedelta,time,date
import decimal_precision as dp
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from dateutil.rrule import rrule, DAILY

import resource
import tools
import addons
import calendar
import operator
import math
from calendar import monthrange

GIORNI=[('1', 'Lunedì'),
        ('2', 'Martedì'),
        ('3', 'Mercoledì'),
        ('4', 'Giovedì'),
        ('5', 'Venerdì'),
        ('6', 'Sabato'),
        ('7', 'Domenica')]

MESI=[('1', 'Gennaio'),
        ('2', 'Febbraio'),
        ('3', 'Marzo'),
        ('4', 'Aprile'),
        ('5', 'Maggio'),
        ('6', 'Giugno'),
        ('7', 'Luglio'),
        ('8', 'Agosto'),
        ('9', 'Settembre'),
        ('10', 'Ottobre'),
        ('11', 'Novembre'),
        ('12', 'Dicembre')]


class cmensa_func(orm.Model):
    _name = "cmensa.func"
    def add_months(self,sourcedate,months):
        month = sourcedate.month - 1 + months
        year = sourcedate.year + month / 12
        month = month % 12 + 1
        day = min(sourcedate.day,calendar.monthrange(year,month)[1])
        return datetime.date(year,month,day),month

    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))

    def gen_data_mese(self,data,mese):
        #print 'generazione per data dal %s ' % data
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]

        data=date(int(a),int(m),int(g))
        g=calendar.monthrange(int(a),int(mese))[1]
        data1=date(int(a),int(mese),g)
        continua=True
        #print 'mese data gen_data',int(mese),int(g)
        meser=0
        while continua:
            #print meser,data1,mese
            if (data1>=data) and (self.mese_del(data1)==int(mese)):
                continua=False
            else:
                data1,meser=self.add_months(data1,1)
        return data1

    def pianif_ora(self,list_id,d,t):
        idd=list_id
        if idd.__len__()==0:
            cmb=datetime.datetime.combine(d,t)
        else:
            cmb=datetime.datetime.combine(d,t)
            t=timedelta(minutes=idd.__len__()*7)
            cmb=cmb+t
        return cmb

    def monthdelta(self,d1, d2):
        delta = 0
        while True:
            mdays = monthrange(d1.year, d1.month)[1]
            d1 += timedelta(days=mdays)
            if d1 <= d2:
                delta += 1
            else:
                break
        return delta

    def set_stat(self,stato,data):
        stat={'new':'pian','pian':'post'}
        if stato=='new':
            stato='pian'
        else:
          stato=self.ctl_scad(data)
        return stato

    def data_cmp(self,dat1,data2):
        val=(data2-data1).days
        if val>0:
           return False
        if val<=0:
           return True

    def ctl_scad(self,data):
        dt=self.gen_data(data)
        oggi=date.today()
        val=(oggi-dt).days
        if val>= 1:
           return 'scad'
        if val==0:
           return 'pian'
        if val<=-1:
           return 'post'

    def get_autodtprimacons(self,data,d,t,delta):
        cmb=datetime.datetime.combine(d,t)
        #print "delta data prima:"
        ctrl=datetime.datetime.combine(self.gen_data(data),t)
        deltadt=(cmb-ctrl)
        #print deltadt.days
        if deltadt.days <= delta:
            td=timedelta(days=delta)
            cmb=cmb+td
            #print 'if delta data %s' % cmb
        #print 'norm delta data %s' % cmb
        return cmb

    def ctl_scadc(self,data,scad):
        dt=self.gen_data(data)
        td=in_the_future(dt,scad)
        oggi=date.today()
        val=(oggi-dt).days
        if val>=1:
           return True
        return False

    def in_the_future(self,data,months=1):
        year, month, day = data.timetuple()[:3]
        new_month = month + months
        return datetime.date(year + (new_month / 12), new_month % 12, day)


    def tra_n_giorni(self,data,giorni):
        td=timedelta(days=giorni)
        return data+td

    def anno_corr(self):
        ret=int(date.today().strftime('%Y'))
        return ret

    def mese_corr(self):
        return int(date.today().strftime('%m'))

    def mese_del(self,data):
        return int(data.strftime('%m'))

    def mese_data(self,data):
        return int(data.strftime('%m'))

    def oltre15(self,data):
        year, month, day = data.timetuple()[:3]
        if day>=15:
            return True
        return False

    def lista_date(self,cr,uid,numero,mesi,datapc):
        v={1:30,2:15}
        res=[]
        annoc=self.anno_corr()
        user = self.pool.get('res.users').browse(cr, uid, uid)
        tid=self.pool.get('cmensa.conf').search(
            cr, uid, [('comptenza','=',user.id_ente.id),('anno','=',annoc)])
        conf=self.pool.get('cmensa.conf').browse(cr, uid, tid[0])
        dateno=self.dict_datano(conf.date)
        mesic=self.genmese(conf.mesi)
        #print 'mesi'
        #print mesic
        volte=v.get(numero)
        continua=True
        borse=numero*mesi
        dataf=datapc
        datai=datapc
        res.append(datai)
        if len(res)==(borse):
            continua=False
        while continua:
            #print dataf
            dataf=self.tra_n_giorni(dataf, volte)
            m=self.mese_data(dataf)
            #print  mesic.__contains__(m)
            if mesic.__contains__(m):
                res.append(dataf)
            if res.__len__()==borse:
                    continua=False
        return res

    def dict_datano(self,dateno):
        res=[]
        v={'ddata':date.today(),'adata':date.today()}
        for r in dateno:
            v['ddata']=self.gen_data(r['ddata'])
            v['adata']=self.gen_data(r['adata'])
            res.append(v.copy())
        return res

    def inrange(self,dateno,data):
        r1=False
        r2=False
        for r in dateno:
             r1=False
             r2=False
             if data >= r['ddata']:
                   r1=True
             if  data <= r['adata']:
                   r2=True
             if r1 and r2:
                  return True
        return False

    def genmese(self,brmese):
        r=[]
        for i in range(0,brmese.__len__()):
            a=int(brmese[i].mese)
            r.append(a)
        return r

    def gengiorni(self,brgiorni):
        r=[]
        for i in range(0,brgiorni.__len__()):
            a=int(brgiorni[i].giorno)-1
            r.append(a)
        return r
    #math.fabs((a[0]['data']-a[1]['data']).days)
    def raggruppa_per_cons(self,consdict):
        return consdict.sort(key=operator.itemgetter('ncons'))

    def calc_datadelta(self,dataprev,listdate):
        ret=[]
        conta=0
        minimo=0
        elenco=[]
        for cons in listdate:
            elenco.append(math.fabs((dataprev-cons['data']).days))
            minimo=min(elenco)
        for cons in listdate:
            v=math.fabs((dataprev-cons['data']).days)
            if v<=minimo+3:
              rig={'id':conta,'delta':v,'ncons':cons['ncons']}
              ret.append(rig.copy())
            conta+=1
        return ret

    def Lista_date_cons_annoc(self,cr,uid,startmonth,durata,lastyear):
        r=[]
        user = self.pool.get('res.users').browse(cr, uid, uid)
        mensa_calend=self.pool.get('cmensa.prenotazione')
        annocorrente=self.anno_corr()
        tid=self.pool.get('cmensa.conf').search(
            cr, uid, [
                ('comptenza','=',user.id_ente.id),
                ('anno','>=',annocorrente)])
        mny=0
        da=startmonth
        mny=(da+durata+1)-12
        for idat in tid:
            conf=self.pool.get('cmensa.conf').browse(cr, uid, idat)
            annoc=conf.anno
            dateno=self.dict_datano(conf.date)
            giorni=self.gengiorni(conf.giorni)
            mesi=self.genmese(conf.mesi)
            print 'anno corrnte ',annoc
            if annoc==annocorrente:
                print 'mny ', mny
                if mny<=0:
                    a=startmonth+durata+1+1
                else:
                    a=12+1
                print a
            elif annoc>annocorrente and mny>0:
                da=1
                a=mny+1
            else:
                da=0
                a=0
            for m in range(da,a):
                if mesi.__contains__(m):
                    c=calendar.monthcalendar(annoc,m)
                    for x in c:
                      conta=0
                      for y in x:
                        conta+=1
                        if y!=0:
                            if giorni.__contains__(calendar.weekday(annoc,m,y)):
                                dt=date(annoc,m,y)
                                if not self.inrange(dateno, dt) :
                                    if dt>date.today():
                                        retid=mensa_calend.search(
                                            cr,uid,[
                                                ('comptenza','=',user.id_ente.id),
                                                ('dtconsv','=',dt.strftime('%Y-%m-%d')),
                                                ('stato','=','pian')
                                                ])
                                        pren=mensa_calend.browse(cr,uid,retid)
                                        totpers=0
                                        for p in pren:
                                            totpers+=p.npersone
                                        n={'data':dt,
                                            'ncons':totpers,'cons':True}
                                        r.append(n.copy())

        return r

    def data_in_lista(self,data,listadate):
        ret=False
        conta=0
        for g in listadate:
            v=math.fabs((data-g['data']).days)
            if v==0:ret=True
        return ret


class cmensa_cgiorni(orm.Model):
    _name = "cmensa.cgiorni"
    _description = "conf Lista giorni consegna"
    _columns = {
        'confb_id': fields.many2one('cmensa.conf','id configurazione'),
        'giorno':fields.selection(GIORNI, 'Giorno'),
    }


class cmensa_cmesi(orm.Model):
    _name = "cmensa.cmesi"
    _description = "conf Lista mesi consegna"
    _columns = {
        'confb_id': fields.many2one('cmensa.conf','id configurazione'),
        'mese':fields.selection(MESI,'Mese'),
    }


class cmensa_cdate(orm.Model):
    _name = "cmensa.cdate"
    _description = "conf Lista date escluse"
    _columns = {
        'confb_id': fields.many2one('cmensa.conf','id configurazione'),
        'ddata': fields.date("dal"),
        'adata': fields.date("al"),
    }


class cmensa_conf(orm.Model):
    _name = "cmensa.conf"
    _description = "configurazione Pianificazione mensa"
    _columns = {
        'idente':fields.many2one('cbase.enti','Inserito Da', size=128,select=True,readonly=True),
        'comptenza':fields.many2one('cbase.enti','Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one', relation='res.city', string='Territorio', readonly=True),
        'anno':fields.integer('Configurazione valida per l anno',required=True,select=True),
        'nbmax':fields.integer('Numero Max di Persone al giorno'),
        'nbext':fields.integer('Numero Max di Persone posticipate'),
        'arai':fields.integer('Orario Inizio checkin'),
        'araf':fields.integer('Orario Fine checkin'),
        'giorni':fields.one2many('cmensa.cgiorni','confb_id','Giorni Consegna' ,required=True),
        'mesi':fields.one2many('cmensa.cmesi','confb_id','Mesi Consegna',required=True),
        'date':fields.one2many('cmensa.cdate','confb_id','Date e Festività',required=True),
        'opinsft':fields.char('Operatore', size=32),
        'dtin': fields.datetime("Data inserimento",readonly=True),
    }
    _defaults = {
        'nbmax':25,
        'nbext':5,
        'dtin':fields.date.context_today,
        'anno':int(date.today().strftime('%Y')),
        'opinsft':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).name,
        'idente':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).id_ente.id,
    }

class cmensa_checkin(orm.Model):
    _name = "cmensa.checkin"
    _description = "Check in Mensa "
    _columns = {
        'data':fields.date("Data Checkin",required=True),
        'pianif':fields.one2many('cmensa.prenotazione',
                                 'chk_id'),
        'nome':fields.char('Nome', size=256),
        'cod_tesseral':fields.char('Codice Checkin', size=32),
        'inizia':fields.boolean('init'),
        'firstchek':fields.boolean('primocheck'),
        'chekok':fields.boolean('finecheck'),
        'noprox':fields.boolean('noprox'),
        'cod_tesseralc':fields.char('Codice Conferma', size=32),
        'dataprox':fields.date("Data Prossima Consegna", readonly=True),
        'npersone':fields.integer('Numero Persone',readonly=True),
        'totpersone':fields.integer('Tatale Persone Attese',readonly=True),
        'totpersentr':fields.integer('Tatale Persone Entrate'),
        'persconf':fields.integer('persone'),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'stato':fields.selection([('conf', 'Confermo'),
                         ('ass', 'Non Confermo')], 'Stato'),
        'opinsft':fields.char('Operatore', size=32),
        'idente':fields.many2one('cbase.enti',
            'Inserito Da', size=128,select=True,readonly=True),
        'comptenza':fields.many2one('cbase.enti',
            'Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one',
            relation='res.city', string='Territorio', readonly=True),
        }
    _defaults = {
                'data':fields.date.context_today,
                'firstchek':False,
                'inizia':False,
                'chekok':False,
                'noprox':False,
                'dtin':fields.date.context_today,
                'opinsft':lambda self, cr, uid, c: self.pool.get(
                    'res.users').browse(cr, uid, uid, c).name,
                'idente':lambda self, cr, uid, c: self.pool.get(
                    'res.users').browse(cr, uid, uid, c).id_ente.id,
                'comptenza':lambda self, cr, uid, c: self.pool.get(
                    'res.users').browse(cr, uid, uid, c).id_ente.id,
                }

    def change_ind(self,cr,uid,ids,ind_id,context=None):
        if ind_id:
            ct=self.pool.get('cbase.indigente').browse(cr,uid,ind_id).indig_id
            res=self.codice_borsa(cr,uid,ids,ct,False,context)
            return res
        else:
            return {}

    def iniziac(self,  cr,  uid, ids, context=None):
        prenot=self.pool.get('cmensa.prenotazione')
        checkin=self.browse(cr,uid,ids[0])
        idret=prenot.search(cr,uid,[('dtconsv','=',checkin.data)])
        if idret.__len__()>0:
            persons=prenot.browse(cr,uid,idret)
            tot=0
            for p in persons:
                if p.stato in ['pian','conf']:
                    tot+=p.npersone
                    prenot.write(cr,1,p.id,{'chk_id':ids[0]})
            self.write(cr, uid, ids[0],{'inizia':True,'totpersone':tot})
            return True
        else:
            return False

    def nuovo(self,  cr,  uid, ids, context=None):
        self.write(cr, uid, ids[0],{'firstchek':False,
                                     'dataprox':False,
                                     'noprox':False,
                                     'nome':False,
                                     'chekok':False,
                                     'persconf':0,
                                     'cod_tesseralc':'',
                                     'cod_tesseral':'',})
        return True

    def conferma(self,cr,uid,ids,cod_tessera,stato,persone,context=None):
        print stato
        if cod_tessera:
            if not stato:
                str='Selezionare se conferma o se non conferma...'
                raise orm.except_orm(_('Attenzione'), _(str))
                return {'value':{'cod_tesseralc':''}}
            prenot=self.pool.get('cmensa.prenotazione')
            checkin=self.browse(cr,uid,ids[0])
            data=checkin.data
            iprox=prenot.search(cr,uid,[('cod_tessera','=',cod_tessera),
                                        ('dtconsv','>',data)])
            if iprox:
                prenot.write(cr,1,iprox[0],{'stato':stato,'npersone':persone})
                self.nuovo(cr,uid,ids)
                return {'value':{'chekok':True}}
            else:
                str='codice non valido... riprovare'
                raise orm.except_orm(_('Attenzione'), _(str))
                return {'value':{'cod_tesseralc':''}}



    def checkin(self,cr,uid,ids,cod_tessera,ch=True,context=None):
        if cod_tessera:
            res={'value':{}}
            #print cod_tessera
            prenot=self.pool.get('cmensa.prenotazione')
            checkin=self.browse(cr,uid,ids[0])
            entrate=checkin.totpersentr
            data=checkin.data
            idret=prenot.search(cr,uid,[('cod_tessera','=',cod_tessera),
                                        ('dtconsv','=',data)])
            if idret.__len__()>0:
                idp=idret[0]
                prn=prenot.browse(cr,uid,idp)
                #da gestire eventuale mancanza di conferma... vediamo quando
                #hanno idee chiare
                if prn.stato=='pian' or prn.stato=='conf':
                    prenot.write(cr,1,idp,{'stato':'evas'})
                    iprox=prenot.search(cr,uid,[('cod_tessera','=',cod_tessera),
                                                ('dtconsv','>',data)])
                    if iprox:
                        prnprox=prenot.browse(cr,uid,iprox[0])
                        res['value']['dataprox']=prnprox.dtconsv
                        res['value']['firstchek']=True
                        res['value']['persconf']=prnprox.npersone
                    else:
                        res['value']['noprox']=True
                        res['value']['chekok']=True
                    entrate+=prn.npersone
                    res['value']['npersone']=prn.npersone
                    res['value']['totpersentr']=entrate
                    res['value']['nome']=prn.ind_id.name
                    return res
                elif prn.stato=='new':
                    str=u"Contattare Reposabile Mensa,\n \
                          utente inserito ma non pianificato\n \
                          Accesso alla Mensa non previsto"
                    raise orm.except_orm(_('Attenzione'), _(str))
                    return res
                elif prn.stato=='ass':
                    str=u"Contattare Reposabile Mensa,\n \
                          L'utente non può accedere in quanto\n \
                          aveva comunicato assenza\n \
                          Accesso alla Mensa non previsto"
                    raise orm.except_orm(_('Attenzione'), _(str))
                    return res
                elif prn.stato=='evas':
                    str=u"L'utente è già Entrato!!\n"
                    raise orm.except_orm(_('Attenzione'), _(str))
                    return res
            else:
              str='Per oggi non è previsto l\'accesso alla Mensa'
              raise orm.except_orm(_('Attenzione'), _(str))
            return res
        else:
            return {}

    _sql_constraints = [(
        'data_uniq',  'unique(data)',
        u'Attenzione!! Questo data esiste già, \
        continuare su quella esistente.'
        )]

class cmensa_prenotazione(orm.Model):
    _name = "cmensa.prenotazione"
    _description = "Prenotazione Mensa "

    def _get_def_npers(self, cr, uid, context=None):
        npers=1
        if 'npersone' in context and context['npersone']:
            npers=context['npersone']
        return npers

    _columns = {
        'chk_id':fields.many2one('cmensa.checkin',
                                 'id checkin'),
        'pnl_id':fields.many2one('cmensa.prenotazioni',
                                 'id pianif'),
        'ind_id':fields.related('pnl_id','name',type='many2one',
            relation='cbase.indigente', string='Richiedente'),
        'cod_tesseral':fields.char('Codice', size=32),
        'cod_tesseralc':fields.char('Codice', size=32),
        'cod_tessera':fields.related('ind_id','indig_id',
            type='char', relation='cbase.indigente', string='Codice'),
        'npersone':fields.integer('Numero Persone'),
        'dtconsv':fields.date("Data prenotazione",required=True),
        'stato':fields.selection([('new', 'Nuova'),
                                 ('pian', 'Pianificato'),
                                 ('conf', 'Confermato'),
                                 ('ass', 'Assente'),
                                 ('evas', 'Erogato'),
                                 ('scad', 'Scaduta')], 'Stato'),
        'dtin': fields.datetime("Data inserimento",readonly=True),
        'lung_day':fields.integer('Lunghezza Giorno'),
        'opinsft':fields.char('Operatore', size=32),
        'idente':fields.many2one('cbase.enti',
            'Inserito Da', size=128,select=True,readonly=True),
        'comptenza':fields.many2one('cbase.enti',
            'Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',type='many2one',
            relation='res.city', string='Territorio', readonly=True),
        }
    _defaults = {
                'dtin':fields.date.context_today,
                'opinsft':lambda self, cr, uid, c: self.pool.get(
                    'res.users').browse(cr, uid, uid, c).name,
                'lung_day':4,
                'npersone':lambda self,cr,uid,c:self._get_def_npers(cr,uid,c),
                'stato':'new',
                'idente':lambda self, cr, uid, c: self.pool.get(
                    'res.users').browse(cr, uid, uid, c).id_ente.id,
                'comptenza':lambda self, cr, uid, c: self.pool.get(
                    'res.users').browse(cr, uid, uid, c).id_ente.id,
                }

    def aggiornastati(self,cr,uid,context=None):
        print 'aggiornamento mensa'
        self.idcompetenze=self.pool.get(
            'cbase.enti').search(cr,uid,[('esterno','=',False)])
        self.competenza=0
        self.func=self.pool.get('cmensa.func')
        self.oggi=date.today()
        self.dtsearch=date.today().strftime(DEFAULT_SERVER_DATE_FORMAT)
        for self.competenza in self.idcompetenze:
            self.idprenot=self.search(cr,uid,[
                ('comptenza','=',self.competenza),
                ('dtconsv','<',self.dtsearch),
                ('stato','in',['pian','conf'])])
            #print self.idprenot
            if len(self.idprenot)>0:
                for idt in self.idprenot:
                    self.write(cr,uid,idt,{'stato':'scad'})



    def _count_at_data(self,cr,uid,ids,data,context=None):
        cid=ids[0]
        nprenot_ids=self.search(cr,uid,[('dtconsv','=',data)])
        #print 'chiamato conteggio e vale ',num
        return num

    def cambio_id(self,cr,uid,ids,context=None):
        cid=ids[0]
        br=self.browse(cr, uid, cid)
        if br.ind_id.active:
            self.write(cr, uid, cid, {'nome': br.ind_id.name,
                                      'indig_code': br.ind_id.indig_id})

    def gen_data(self,data):
        dt=data.split(" ")
        dt=dt[0].split("-")
        a,m,g=dt[:3]
        return date(int(a),int(m),int(g))

    def change_data(self,cr,uid,ids,data,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        func=self.pool.get('cmensa.func')
        annoc=func.anno_corr()
        tid=self.pool.get('cmensa.conf').search(cr, uid, [
            ('comptenza','=',user.id_ente.id),('anno','=',annoc)])
        conf=self.pool.get('cmensa.conf').browse(cr, uid, tid[0])
        d=func.gen_data(data)
        idret=self.search(cr,uid,[
            ('comptenza','=',user.id_ente.id),('dtconsv','=',d.strftime('%Y-%m-%d'))])
        maxc=conf.nbmax
        lst=func.Lista_date_cons_annoc(cr,uid,2)
        if func.data_in_lista(d,lst):
            if idret.__len__()>=25:
                str='In questa data sono pianificate %d Consegne' % idret.__len__()
                raise orm.except_orm(_('Info'), _(str))
        else:
          str='Questa non è una data di consegna'
          raise orm.except_orm(_('Attenzione'), _(str))

    def read_conf(self, cr, uid, ids,context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        anno=self.pool.get('cmensa.func').anno_corr()
        tid=self.pool.get('cmensa.conf').search(cr, uid, [
            ('comptenza','=',user.id_ente.id),('anno','=',anno)])
        func=self.pool.get('cmensa.conf').browse(cr, uid, tid[0])
        return func

    def accetta_domanda(self,  cr,  uid, ids, context=None):
        self.write(cr, uid, ids, {'stato': 'pian'})
        return True

    def imp_assente(self,  cr,  uid, ids, context=None):
        self.write(cr, uid, ids, {'stato': 'ass'})
        return True

    def reimposta_domanda(self,  cr,  uid, ids, context=None):
        self.write(cr, uid, ids, {'stato': 'new'})
        return True

    def unlink(self, cr, uid, ids, context=None):
        lineas=self.browse(cr,uid,ids)
        for linea in lineas:
            if (linea.stato=='scad' or linea.stato=='cons'):
                raise orm.except_orm(
                    'Attenzione','Impossibile eleiminare borse consegnate o scadute' )
                return True
        return super(cmensa_prenotazione,self).unlink(cr, uid,ids, context)


    _order = 'dtconsv asc'


class cmensa_prenotazioni(orm.Model):
    _name = "cmensa.prenotazioni"
    _description = "pianificazione Mensa"
    _columns = {
        'name':fields.many2one('cbase.indigente', 'Richiedente',required=True),
        'fam_id':fields.related('name','famiglia',type='one2many',
            relation='cbase.famiglia', string='Famiglia',readonly=True),
        'cod_tessera':fields.related('name','indig_id',
            type='char', relation='cbase.indigente',
            string='Codice',readonly=True),
        'dtfrom':fields.date("Dal"),
        'dtto':fields.date("Al"),
        'stato':fields.selection([('new','Nuova'),
                                 ('gen', 'Generata'),
                                 ('done','Finita')], 'Stato'),
        'npersone':fields.integer('Numero Persone'),
        'pianif':fields.one2many('cmensa.prenotazione',
            'pnl_id','Lista date Pianificate',required=True),
        'dtini': fields.datetime("Data inserimento",readonly=True),
        'opinsft':fields.char('Operatore', size=32),
        'comptenza':fields.many2one('cbase.enti',
            'Competenza di ', size=128,select=True),
        'territorio':fields.related('comptenza','territorio',
            type='many2one', relation='res.city',
            string='Territorio', readonly=True),
        'idente':fields.many2one('cbase.enti','Inserito Da',
            size=128,select=True,readonly=True),
        }

    _defaults = {
        'stato':'new',
        'npersone':1,
        'dtini':fields.date.context_today,
        'opinsft':lambda self, cr, uid, c: self.pool.get(
            'res.users').browse(cr, uid, uid, c).name,
        'idente':lambda self, cr, uid, c: self.pool.get(
            'res.users').browse(cr, uid, uid, c).id_ente.id,
        'comptenza':lambda self, cr, uid, c: self.pool.get(
            'res.users').browse(cr, uid, uid, c).id_ente.id,
        }

    def generadate(self,  cr,  uid, ids, context=None):
        objs = self.browse(cr, uid, ids)
        objprenot=self.pool.get('cmensa.prenotazione')
        func=self.pool.get('cmensa.func')
        for prenot in objs:
            if prenot.dtfrom == False or prenot.dtto==False:
                raise orm.except_orm(
                    _(u'Attenzione!!'),
                    _(u"Inseire prima la data inizio e la data di fine.."))
                return False
            start_date=datetime.strptime(
                prenot.dtfrom, DEFAULT_SERVER_DATE_FORMAT).date()
            end_date=datetime.strptime(
                prenot.dtto, DEFAULT_SERVER_DATE_FORMAT).date()
            lastyear=int(end_date.strftime('%Y'))
            m=int(start_date.strftime('%m'))
            delta=func.monthdelta(start_date,end_date)
            print lastyear
            lista_date=func.Lista_date_cons_annoc(cr,uid,m,delta,lastyear)
            for d in lista_date:
                if start_date<=d['data']<=end_date:
                    cmpd=d['data'].strftime(DEFAULT_SERVER_DATE_FORMAT)
                    idr=objprenot.search(cr,uid,[
                        ('ind_id','=',prenot.name.id),
                        ('dtconsv','=',cmpd)])
                    if idr==[]:
                        objprenot.create(cr,uid,{'pnl_id':prenot.id,
                                              'dtconsv':cmpd,
                                              'npersone':prenot.npersone,
                                              'stato':'pian'})

            self.write(cr,uid,prenot.id,{'stato':'gen'})
        return True

    def unlink(self, cr, uid, ids, context=None):
        return True

    _order = 'name'


class cbase_famiglia(orm.Model):
    _inherit = 'cbase.famiglia'
    _description = 'famiglia'
    _columns = {
        'id_prenotaz':fields.many2one('cmensa.prenotazioni', 'Mensa')
    }

class cbase_indigente(orm.Model):
    _inherit = 'cbase.indigente'
    _description = 'famiglia'

    _columns = {
        'id_prenotaz':fields.many2one('cmensa.prenotazioni', 'Mensa')
    }





# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


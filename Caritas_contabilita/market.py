# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------------
#   Modulo Gestione alimenti raccolti dai superarket
#   Autore: Alessio Gerace
#   data  : Maggio 2011
#   update : Giugno 2011
#   
#
#
#
#
##############################################################################

from osv import osv,fields
from tools.translate import _
from datetime import datetime,timedelta,time,date
import decimal_precision as dp

import resource 
import tools
import addons
import datetime
import calendar
import operator
import math

class market_func(osv.osv):
          _name = "market.func"
          
          def gen_data(self,data):
             dt=data.split(" ")
             dt=dt[0].split("-")
             a,m,g=dt[:3]
             return date(int(a),int(m),int(g))
         
          def dict_datano(self,dateno):
            res=[]
            v={'ddata':datetime.date.today(),'adata':datetime.date.today()}
            for r in dateno:
                v['ddata']=self.gen_data(r['ddata'])
                v['adata']=self.gen_data(r['adata'])
                res.append(v.copy())
            return res 
        
          def inrange(self,dateno,data):
            r1=False
            r2=False
            for r in dateno:
                 r1=False
                 r2=False
                 if data >= r['ddata']:
                       r1=True
                 if  data <= r['adata']:
                       r2=True
                 if r1 and r2:
                      return True
            return False
market_func()


class market_superm(osv.osv):
    def _ut_ente(self, cr, uid, ids,name, args, context=None):
        res = {}
        for indi in self.browse(cr, uid, ids):
            if indi.opinsft==False:
                 res[indi.id]=""
                 #con questa funzione cerco il create_id e prendo il valore
                 #e poi cerco il l'ente a cui apprtiene e lo stampo
                 idc=self.perm_read(cr,uid,ids)[0]['create_uid'][0]
                 ide=self.pool.get('res.users').browse(cr,uid,idc).name
                 self.write(cr, uid, indi.id, {'opinsft': ide})
                 res[indi.id]=ide
        return res 
    def _amount_all(self, cr, uid, ids, name, args, context=None):
        res = {}
        total=0.0
        for list in self.browse(cr, uid, ids, context=context):
            total+=list.imp
        for list in self.browse(cr, uid, ids, context=context):
            res[list.id].tot=total
        return res
    _name = "market.superm"
    _description = "Lista righe ddt e valori"
    _columns = {
        'ric_id':fields.integer('indice Ricerca'),
        'market_id':fields.many2one('rubrica.rubrica', 'Supermercato',required=True),
        'data':fields.date("del",required=True),
        'num':fields.char('DDT Numero', size=32,required=True),
        'imp': fields.float('Importo €',  digits=(14, 2)),
        'tot':fields.float('Tot €',  digits=(14, 2)),
        'totale':fields.function(_amount_all,method=True,string='Tot.Richieste €'),
        'opinsft':fields.char('Operatore', size=32),
        'Opinsf':fields.function(_ut_ente, method=True, type="char", string='Operatore',readonly=True),
        'dtin': fields.datetime("Data inserimento",readonly=True),
            }
    _defaults = {
            'dtin':fields.date.context_today,
            }
    _order = 'data asc'
        
market_superm()

class market_loadsuperms(osv.osv):
    def _ut_ente(self, cr, uid, ids,name, args, context=None):
        res = {}
        for indi in self.browse(cr, uid, ids):
            if indi.opinsft==False:
                 res[indi.id]=""
                 #con questa funzione cerco il create_id e prendo il valore
                 #e poi cerco il l'ente a cui apprtiene e lo stampo
                 idc=self.perm_read(cr,uid,ids)[0]['create_uid'][0]
                 ide=self.pool.get('res.users').browse(cr,uid,idc).name
                 self.write(cr, uid, indi.id, {'opinsft': ide})
                 res[indi.id]=ide
        return res   
    
    _name = "market.loadsuperms"
    _description = "Report supermercati"
    _columns = {
        'market_id':fields.many2one('rubrica.rubrica', 'Supermercato',required=True),
        'sped':fields.many2one('rubrica.rubrica', 'Da inviare A'),
        'list_id':fields.one2many('market.superm','ric_id','Lista DDT',readonly=True),
    #fields.function(_ddt_lista, method=True, type="one2many", string='Lista DDT',readonly=True),
        #fields.many2many('market.superm','rubrica.rubrica','market_id','market_id','Lista DDT'),
        'ddata':fields.date("dal"),
        'adata':fields.date("al"),
        'tot': fields.float('Totale Importo €',  digits=(14, 2)),
        'opinsft':fields.char('Operatore', size=32),
        'opinsf':fields.function(_ut_ente, method=True, type="char", string='Operatore',readonly=True),
        'dtin': fields.datetime("Data inserimento",readonlycborse=True),
            }
    _defaults = {
            'ddata':fields.date.context_today,
            'adata':fields.date.context_today,
            'dtin':fields.date.context_today,
            }

    _order = 'dtin desc'
    
    def genera_dati(self,cr,uid,ids,context=None):
        func=self.pool.get('market.func')
        list=self.browse(cr, uid, ids[0])
        idsS=self.pool.get('market.superm').search(cr,uid,[('market_id','=',list.market_id.id)])
        val=0.0
        idno=0
        idsi=ids[0]
        if idsS.__len__() > 0:
            list_id=self.pool.get('market.superm').browse(cr,uid,idsS)
            for d in list_id:
                if (bool(list.ddata) & bool(list.adata)):
                    if ((d.data >= list.ddata) & (d.data <= list.adata)):
                        self.pool.get('market.superm').write(cr,uid,d.id,{'ric_id':idsi})
                        val+=d.imp
                    else:
                        self.pool.get('market.superm').write(cr,uid,d.id,{'ric_id':idno})
                else:
                    self.pool.get('market.superm').write(cr,uid,d.id,{'ric_id':idsi})
                    val+=d.imp
            a=d.imp
            self.write(cr,uid,ids[0],{'tot': val})
        return True
    
 
market_loadsuperms()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

